# pitchme.app Web application

- Client side: create-react-app ejected, React, Mobx, Redux (only for simple-webrtc package),
  Material UI, simple-webrtc

- Server side: Express, rest api, mongoose, socket-io

## Development process

1. Server app `cd ./server && npm run dev` (https://localhost probably you have to get browser
   access to work with unsafe https connection)

2. Client app `cd ./client && npm run dev:local` to work with local backend or `npm run dev` to work
   with deployed backend (https://localhost:3000 - a working host for client app)

## Production process

1. build apps (in both dirs: `npm run build`)

2. start express server `cd ./server && npm start`

HOSTING:
https://www.mvps.net/
L: ivan.slepchenko@zettech.com
P: ZetTech1221

VPS: 178.157.82.240
L: root
P: Coca_Cola1981


gitlab image registry deploy token:
L: gitlab-deploy-token-pitchmeapp
P: 663rMYXRHnUxcSAvFVUw
