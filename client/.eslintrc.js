module.exports = {
    extends: [
        'airbnb-typescript',
        'airbnb/hooks',
        'plugin:@typescript-eslint/recommended',
        'plugin:jest/recommended',
        'prettier',
    ],
    plugins: ['react', '@typescript-eslint', 'jest'],
    env: {
        browser: true,
        es6: true,
        jest: true,
    },
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
        project: './tsconfig.eslint.json',
    },
    rules: {
        'linebreak-style': 'off',
        'react/jsx-uses-react': 'off',
        'react/react-in-jsx-scope': 'off',
        'react/state-in-constructor': 'off',
        '@typescript-eslint/ban-ts-comment': 'off',
        '@typescript-eslint/return-await': 'warn',
        'react/prop-types': 'off',
        'react/jsx-props-no-spreading': 'off',
        'react/no-array-index-key': 'warn',
        '@typescript-eslint/no-var-requires': 'warn',
        'react/prefer-stateless-function': 'warn',
        'react-hooks/exhaustive-deps': 'off',
        'consistent-return': 'off',
        'no-plusplus': 'warn',
        'jsx-a11y/media-has-caption': 'warn',
        'prefer-destructuring': 'warn',
        'no-restricted-globals': 'warn',
        'no-useless-concat': 'warn',
        'react-hooks/rules-of-hooks': 'warn',
        '@typescript-eslint/naming-convention': 'warn',
        '@typescript-eslint/explicit-module-boundary-types': 'off',
        'no-param-reassign': 'warn',
        'react/no-unescaped-entities': 'warn',
        'class-methods-use-this': 'off',
        'react/require-default-props': 'warn',
        'import/prefer-default-export': 'off',
        'import/no-cycle': 'warn',
        'no-underscore-dangle': 'off',
    },
    settings: {
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
        'import/resolver': {
            typescript: {
                project: './tsconfig.eslint.json',
            },
        },
    },
};
