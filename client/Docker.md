# Docker

Linux system!

1.  Need to install packages:

    -   docker
    -   docker-compose

2.  Start docker `systemctl start docker`

3.  Docker production build

-   with docker-compose:

    -   `sudo docker-compose build`
    -   `sudo docker-compose up`

-   with docker:

    -   build container `sudo docker build .`
    -   run container `sudo docker run <container_id>`
