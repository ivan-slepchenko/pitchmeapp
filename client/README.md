# pitchme.app Backend Side

## NPM Tasks

To use deploy task you should install aws cli and configure it for you.
https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html  
https://docs.aws.amazon.com/cli/latest/reference/configure/

-   `set NODE_ENV=development&&npm run dev` - to start frontend with local backend.
-   `npm run dev` to start frontend with production backend.
-   `npm run dev:local` to start frontend with local(dev) backend.
-   `npm run build` to build a production ready version.
-   `npm run deploy` to push new bundle into s3 static hosting.
-   `npm run parse:trans` to update keys in base.json file

## HOW TO TEST IN NETWORK

1. Check your local server internal network ip.
   check it with ipconfig.  
   For my wireless LAN adapter IPv4 Address is 192.168.178.32.

2. Install OpenSSL if you don't have it.

3. Open your your openssl.cnf file, for Windows 10 its probably located somewhere like C:\Program Files\OpenSSL-Win64\bin\cnf\openssl.cnf

4. Edit this configuration file to include the server's IP address under the [ v3_ca ] section: [Edit or insert]
   subjectAltName = IP:192.168.178.32

5. Generate self-signed certificate for your ip adress.

    ```shell
    openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./security/192.168.178.32.key -out ./security/192.168.178.32.crt -config "C:\Program Files\OpenSSL-Win64\bin\cnf\openssl.cnf"
    ```

6. When you will run your app, add the certificate into trusted root certification authorities store.

## HELPFUL RESOURCES:

-   [SimpleWebRTC api](https://docs.simplewebrtc.com)
-   [Material UI](https://material-ui.com) - Components/UI based on material lib
-   [Material Icons](https://material.io/tools/icons/) - Icons list
-   [React Icons](https://react-icons.netlify.com/#/) - Preffered icons pack

### NOTES:

-   `openssl req -x509 -newkey rsa:4096 -keyout security/some.key -out security/some.crt -days 365 -nodes` - it create .key and .crt
-   start chrome with ignoring ssl certificate for localhost:3000 `/opt/google/chrome/chrome --ignore-certificate-errors --unsafely-treat-insecure-origin-as-secure=https://localhost:3000`

## USED ENVIRONMENT VARIABLES:

-   PUBLIC_URL

When running docker container in the interactive mode (param: -it) you can get IP of the container directly in the console - ipconfig. This IP is the container IP address. Since you are running the container in Win10 and I suppose you are using default NAT adapter you cant access the container using port mapping (eg. localhost:port). The reason is WinNAT that simply doesnt work like this now. In WinServer2016 you can use this method (port mapping).

So you have to (on Win10):

Obtain IP of the container (ipconfig inside the container or docker inspect [containername] and search for the IP (almost in the bottom of the result).
Expose the port you need to access to (docker run –expose [port number]).
Access from you host like this: http://ip-of-your-container 469:port
That`s all.
