import { css } from '@emotion/core';
import styled from '@emotion/styled';
import { Button } from 'primereact/button';

export const colorOrange = '#B70000';

export const globalCss = css`
    @import url('https://fonts.googleapis.com/css?family=Annie+Use+Your+Telescope');
    @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500');
    @import url('https://fonts.googleapis.com/css?family=Saira+Condensed');

    input:-webkit-autofill,
    input:-webkit-autofill:hover,
    input:-webkit-autofill:focus,
    input:-webkit-autofill:active {
        transition: background-color 5000s ease-in-out 0s, color 5000s ease-in-out 0s, font-size 5000s ease-in-out 0s;
    }

    ::-webkit-scrollbar {
        width: 10px;
    }
    ::-webkit-scrollbar-track {
        border-radius: 10px;
    }
    ::-webkit-scrollbar-thumb {
        background-color: rgba(255, 255, 255, 0.3);
        border-radius: 10px;
    }

    button {
        cursor: pointer;
    }

    .p-width-full {
        width: 100%;
    }
    
    .p-toast-message {
        border-radius: 0px !important;
    }
    
    .p-of-contain {
        object-fit: contain;
    }

    .p-top-left {
        position: relative;
        top: 0px;
        left: 0px;
    }

    .p-ellipsis {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .p-toast-message-info {
        background: #007ab9 !important;
        border: solid #b70000 !important;
        border-width: 0 6px 0 6px !important;
        color: white !important;
    }

    .p-toast-message-error {
        background: #b70000 !important;
        border: solid #007ab9 !important;
        border-width: 0 6px 0 6px !important;
        color: white !important;
    }

    .p-toast-icon-close {
        color: white !important;
    }

    .p-toast-message-warn {
        background: #ffc10e !important;
        border: solid #b70000 !important;
        border-width: 0 6px 0 6px !important;
        color: white !important;
    }

    .svg-white {
        svg {
            fill: white;
        }
    }

    .p-toast-message-success {
        background: #219653 !important;
        border: solid #b70000 !important;
        border-width: 0 6px 0 6px !important;
        color: white !important;
    }

    .p-toast-message-content {
        padding: unset !important;
    }

    .invisiblePage {
        visibility: hidden;
    }
    video {
        transform: rotateY(180deg);
    }
    .p-white {
        color: white;
    }

    .p-s-box {
        width: 28px !important;
        height: 28px !important;
        padding: 0px;
    }

    .p-xs-box {
        width: 23px !important;
        height: 23px !important;
        padding: 0px;
    }

    .p-xxs-icon {
        width: 10px !important;
        height: 10px !important;
        left: 4px;
        font-size: 13px;
        top: 4px;
        position: absolute;
    }

    .p-button-success {
        color: #ffffff !important;
        background: #219653 !important;
        border: 1px solid #219653 !important;
        box-shadow: none !important;
    }

    .p-button-info {
        color: #ffffff !important;
        background: #007ab9 !important;
        border: 1px solid #007ab9 !important;
        box-shadow: none !important;
    }

    .p-button-danger {
        color: #ffffff !important;
        background: #b70000 !important;
        border: 1px solid #b70000 !important;
        box-shadow: none !important;
    }

    .p-button-help {
        color: #ffffff !important;
        background: #9b51e0 !important;
        border: 1px solid #9b51e0 !important;
        box-shadow: none !important;
    }

    .p-button-warning {
        font-weight: bold;
        color: #b70000 !important;
        background: #ffc10e !important;
        border: 2px solid #b70000 !important;
        box-shadow: none !important;
    }

    .p-button-secondary {
        color: #ffffff !important;
        background-color: #7f91a8 !important;
        border: 1px solid #7f91a8 !important;
        box-shadow: none !important;
    }

    .p-highlight {
        border-color: #219653 !important;
        background: #219653 !important;
        color: #ffffff;
        box-shadow: none !important;
    }

    .p-button-lg {
        font-size: 1.25rem !important;
        padding: 1rem 0.8rem !important;
    }

    .p-button-xsm {
        font-weight: 400 !important;
        padding: 0.1rem 0.5rem !important;
    }

    .p-rounded-4 {
        border-radius: 4px;
    }

    .p-button-rounded-lg {
        width: 50px !important;
        height: 50px !important;
        span {
            font-size: 1.4rem !important;
        }
    }

    .p-index-0 {
        z-index: 0 !important;
    }

    code {
        font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
    }

    body {
        margin: 0;
        padding: 0;
        background-color: white;
        color: #4f4f4f;
        letter-spacing: -0.16px;
        font-family: 'Roboto', 'Arial', sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        overflow: hidden;
    }

    main {
        .large-text {
            font-size: large;
        }
        .middle-text {
            font-size: 1.2rem;
        }
        .small-text {
            font-size: small;
        }
        .xx-large-text {
            font-size: 5.5vw;
        }

        .x-large-text {
            font-size: 3.5vw;
        }

        .more-space {
            margin-bottom: 15px;
        }
    }

    .no-focus-border:focus {
        outline-style: none;
        box-shadow: none;
        border-color: transparent;
    }

    main form {
        flex-shrink: 0;
    }

    audio,
    canvas,
    video {
        display: inline-block;
    }

    /**
     * Prevent modern browsers from displaying \`audio\` without controls.
     * Remove excess height in iOS 5 devices.
     */
    audio:not([controls]) {
        display: none;
        height: 0;
    }

    /**
     * Address \`[hidden]\` styling not present in IE 8/9.
     * Hide the \`template\` element in IE, Safari, and Firefox < 22.
     */
    [hidden],
    template {
        display: none;
    }
`;

export const CenteredContainer = styled.div`
    width: 90%;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
`;

export const AbsoluteCenteredContainer = styled(CenteredContainer)`
    position: absolute;
`;

export const TopThirdContainer = styled.div`
    display: flex;
    justify-content: start;
    flex-direction: column;
    align-items: center;
    position: absolute;
    width: 100%;
    height: 75%;
    top: 25%;
`;
export const FullSizeAbsolute = styled.div`
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100%;
`;

export const FullSizeAbsoluteFlexColumnContainer = styled(FullSizeAbsolute)`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

export const FullSizeContainer = styled.div`
    width: 100%;
    height: 100%;
    overflow: hidden;
`;

export const FullSizeForm = styled.form`
    width: 100%;
    height: 100%;
`;

export const NoWrapRow = styled.div`
    width: 100%;
    display: block;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    input {
        width: 100%;
        position: relative;
    }
`;

export const Row = styled.div`
    width: 100%;
`;

export const FlexNoWrapRow = styled(NoWrapRow)`
    display: flex;
    align-items: center;
`;

export const CenteredFlexNoWrapRow = styled(FlexNoWrapRow)`
    justify-content: center;
`;

export const MultilineFormRow = styled(NoWrapRow)`
    white-space: normal;
`;

export const WhiteText = styled.div`
    color: white;
`;

export const FormLabel = styled.span`
    font-size: 14px;
`;

export const FormBiggerLabel = styled.span`
    font-size: 18px;
`;

export const RequiredAsterix = styled.span`
    position: absolute;
    font-size: 38px;
    font-weight: 500;
    margin-left: 5px;
    color: #b70000;
`;

export const CapitalizedRobotoButton = styled(Button)`
    font-family: Roboto;
    font-weight: normal;
    text-transform: capitalize;
    height: fit-content;
`;

export const NextInFormButton = styled(CapitalizedRobotoButton)`
    width: fit-content;
    transform: translateX(-50%);
    left: 50%;
`;

export const Title = styled.h2`
    font-weight: normal;
    font-size: 18px;
    text-align: center;
`;

export const AnnieTitle = styled.h2`
    font-weight: normal;
    font-size: 33px;
    text-align: center;
    font-family: 'Annie Use Your Telescope';
`;

export const AnnieTitleNoPadding = styled.span`
    font-weight: normal;
    font-size: 33px;
    text-align: center;
    font-family: 'Annie Use Your Telescope';
    padding-bottom: 0px;
    padding-top: 0px;
`;

export const Form = styled.form`
    max-width: 380px;
    width: 85%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 0 10px;
    padding: 15px;
`;

export const BorderedDiv = styled.div`
    background-color: #f3f3f3;
    border-radius: 20px;
    max-width: 380px;
    width: 85%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 10px;
`;

export const BlackBorderedDiv = styled(BorderedDiv)`
    background-color: rgba(0, 0, 0, 0.3);
    color: white;
    z-index: 1;
`;

export const RoundedImage = styled.img`
    border-radius: 100%;
`;

export const GreyPanel = styled.div`
    background-color: #f3f3f3;
    border-radius: 20px;
`;
