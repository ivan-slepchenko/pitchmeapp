import { Component } from 'react';
import { FaRegSurprise } from 'react-icons/fa';
import styled from '@emotion/styled';
import { gaBoundaryError } from '../utils/ga';

const Wrapper = styled.div`
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    background: rgba(0, 0, 0, 0.33);

    svg {
        width: 1.5em;
        height: 1.5em;
    }
`;

class ErrorBoundary extends Component {
    state = { hasError: false };

    static getDerivedStateFromError() {
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        console.log(error);
        console.log(errorInfo);
        gaBoundaryError(error, errorInfo);
    }

    render() {
        const { hasError } = this.state;
        const { children } = this.props;

        if (hasError) {
            return (
                <Wrapper>
                    <h1>
                        Something went wrong
                        <FaRegSurprise />
                    </h1>
                    <h2>We work with this problem</h2>
                </Wrapper>
            );
        }

        return children;
    }
}

export default ErrorBoundary;
