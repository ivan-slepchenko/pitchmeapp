import styled from '@emotion/styled';

const BluredWrapper = styled.div`
    width: 100%;
    height: 100%;
    background: rgb(0 55 138 / 66%);
    filter: blur(10px);
`;

export default BluredWrapper;
