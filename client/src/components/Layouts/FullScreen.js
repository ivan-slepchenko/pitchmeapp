import styled from '@emotion/styled';

const FullScreen = styled.div`
    height: 100%;
    width: 100%;
    overflow: hidden;
`;

export default FullScreen;
