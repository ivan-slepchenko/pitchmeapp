import { BrowserView, MobileView } from 'react-device-detect';
import DeviceOrientation, { Orientation } from 'react-screen-orientation';

const OrientationPattern = ({ landscape, portrait }) => (
    <>
        <MobileView>
            <DeviceOrientation lockOrientation="portrait">
                <Orientation orientation="landscape" alwaysRender={false}>
                    {landscape}
                </Orientation>

                <Orientation orientation="portrait" alwaysRender={false}>
                    {portrait}
                </Orientation>
            </DeviceOrientation>
        </MobileView>

        <BrowserView>{landscape}</BrowserView>
    </>
);

export default OrientationPattern;
