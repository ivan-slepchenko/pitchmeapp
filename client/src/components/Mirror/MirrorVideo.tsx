// @ts-nocheck
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { isIOSSafari } from 'utils/detectBrowser';
import { observe } from 'mobx';
import { bind } from 'decko';
import { withRouter } from 'react-router-dom';
import { MirrorWrapper } from './Styled';
import { gaMirrorCantJoinRoomTimeLimitExceed, gaUserMediaStreamStartFailed } from '../../utils/ga';
import { ROUTES } from '../../routes';
import { handleNoCameraAccess } from '../../pages/Mirror/handleNoCameraAccess';

@withRouter
@inject(({ notifyStore, userStore }) => ({ notifyStore, userStore }))
@observer
class MirrorVideo extends Component {
    disposer;

    constructor(props) {
        super(props);
        this.videoRef = props.videoRef || React.createRef();
        this.stream = null;
    }

    componentDidMount() {
        console.log('MIRROR VIDEO DID MOUNT');
        const { userStore } = this.props;

        this.streamUserMedia().then();
        this.disposer = observe(userStore, 'userCamAccess', () => this.streamUserMedia().then());
    }

    componentWillUnmount() {
        if (this.stream) {
            this.stream.getTracks().forEach((track) => track.stop());
            this.stream = null;
        }
        this.videoRef = null;
        this.disposer();
        console.log('MIRROR VIDEO DID UMOUNT');
    }

    @bind
    handleClick(e) {
        const {
            notifyStore,
            history,
            userStore: { mediaStatus },
            roomStore: { isRoomLimitExceed, notifyAboutRoomTimeExceed },
        } = this.props;

        if (!mediaStatus) {
            e.preventDefault();
            handleNoCameraAccess(notifyStore.show, history.push);
        } else if (isRoomLimitExceed) {
            e.preventDefault();
            notifyAboutRoomTimeExceed();
            gaMirrorCantJoinRoomTimeLimitExceed();
        } else {
            history.push({ pathname: ROUTES.ROOM.path });
        }
    }

    async streamUserMedia() {
        const {
            notifyStore,
            userStore: { setUserCamAccess, setUserMicAccess, setMediaStatus },
            history,
        } = this.props;
        const video = this.videoRef.current;
        try {
            const stream = await navigator.mediaDevices.getUserMedia({
                audio: true,
                video: true,
            });
            if (isIOSSafari) {
                video.setAttribute('controls', true); // Hacks for Mobile Safari
            }

            video.srcObject = stream;
            video.onloadedmetadata = () => {
                video.play();
            };

            setMediaStatus(true);
            this.stream = stream;
        } catch (error) {
            gaUserMediaStreamStartFailed(error);
            setUserCamAccess(false);
            setUserMicAccess(false);
            handleNoCameraAccess(notifyStore.show, history.push);
        }
    }

    render() {
        const {
            userStore: { userCamAccess, mediaStatus },
        } = this.props;
        return (
            <MirrorWrapper className="p-index-0" style={{ opacity: userCamAccess && mediaStatus ? 1 : 0 }}>
                <video id="video" controls={false} playsInline autoPlay volume="0.01" muted ref={this.videoRef} />
            </MirrorWrapper>
        );
    }
}

MirrorVideo.propTypes = {
    videoRef: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({ current: PropTypes.instanceOf(Element) })]),
};

export default MirrorVideo;
