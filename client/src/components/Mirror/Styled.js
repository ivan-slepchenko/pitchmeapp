import styled from '@emotion/styled';

export const MirrorWrapper = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(29, 29, 29, 1);
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;

    #video {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        z-index: 1;
        object-fit: cover;
        object-position: 50% 50%;
        pointer-events: none;
    }
`;
