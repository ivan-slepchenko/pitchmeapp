// @ts-nocheck
import { inject, observer } from 'mobx-react';
import { autorun } from 'mobx';
import { Toast } from 'primereact/toast';
import { Component } from 'react';

@inject(({ notifyStore }) => ({ notifyStore }))
@observer
class Notifier extends Component {
    toast = null;

    constructor(props) {
        super(props);
        autorun(() => {
            const {
                notifyStore: { eventsQueue, utilizeQueue, setShown },
            } = this.props;
            eventsQueue.forEach(({ type, key, payload, shown }) => {
                switch (type) {
                    case 'show': {
                        if (shown) {
                            break;
                        }

                        const { message, options } = payload;
                        this.toast.show({
                            severity: options.variant,
                            life: 10000,
                            content: (
                                <div className="p-flex p-flex-column" style={{ flex: '1' }}>
                                    <div className="p-text-center p-m-2">{message}</div>
                                </div>
                            ),
                        });
                        setShown(key, message);
                        break;
                    }

                    case 'close_all': {
                        this.toast.clear();
                        utilizeQueue();
                        break;
                    }

                    default:
                }
            });
        });
    }

    render() {
        return (
            <Toast
                position="none"
                style={{ width: '100%' }}
                ref={(el) => {
                    this.toast = el;
                }}
            />
        );
    }
}

export default inject(({ notifyStore }) => ({
    notifyStore,
}))(observer(Notifier));
