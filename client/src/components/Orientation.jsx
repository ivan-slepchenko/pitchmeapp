import { BrowserView, MobileView } from 'react-device-detect';
import DeviceOrientation, { Orientation } from 'react-screen-orientation';

const AppOrientation = ({ children }) => (
    <>
        <MobileView>
            <DeviceOrientation>
                <Orientation orientation="landscape" alwaysRender={false}>
                    {children}
                </Orientation>

                <Orientation orientation="portrait" alwaysRender={false}>
                    {children}
                </Orientation>
            </DeviceOrientation>
        </MobileView>

        <BrowserView>{children}</BrowserView>
    </>
);

export default AppOrientation;
