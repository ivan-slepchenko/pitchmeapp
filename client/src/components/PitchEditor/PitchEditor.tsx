// @ts-nocheck
import React, { Component } from 'react';
import { Button } from 'primereact/button';
import { RadioButton } from 'primereact/radiobutton';
import { InputText } from 'primereact/inputtext';
import { inject, observer } from 'mobx-react';
import { Title, RequiredAsterix, CapitalizedRobotoButton, FormLabel, FullSizeForm, NoWrapRow } from '../../Styled';
import { PITCH_TIMES } from '../../store/UserStore';
import { ROUTES } from '../../routes';
import { gaPitchUploadClicked } from '../../utils/ga';

const PPTX_TYPE = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
const PPT_TYPE = 'application/vnd.ms-powerpoint';
const PDF_TYPE = 'application/pdf';

@inject(({ userStore, notifyStore, eventsStore }) => ({ userStore, notifyStore, eventsStore }))
@observer
class PitchEditor extends Component {
    constructor(props) {
        super(props);
        this.uploadRef = React.createRef(null);
    }

    render() {
        const {
            notifyStore,
            userStore: {
                pitchToEdit,
                pitchToEdit: { name, id: pitchId },
                user: { pitches },
            },
            exit,
            pushPitch,
            prevPath,
            eventsStore: { featureFlags },
        } = this.props;

        const handleSubmit = (e) => {
            e.preventDefault();
            pushPitch();
        };

        return (
            <FullSizeForm onSubmit={handleSubmit}>
                <Title>{pitchId === -1 ? 'Add Your Pitch:' : 'Edit Your Pitch'}</Title>

                <NoWrapRow className="p-pb-3 p-pl-3 p-pr-3">
                    <InputText
                        placeholder="Pitch Name:"
                        name="name"
                        value={name}
                        keyfilter={null}
                        autoComplete="off"
                        onChange={(e) => {
                            pitchToEdit.name = e.target.value;
                        }}
                    />
                    <RequiredAsterix>*</RequiredAsterix>
                </NoWrapRow>

                {featureFlags.PITCH_DECKS.enabled && (
                    <NoWrapRow className="p-pl-3 p-pr-3">
                        <Button
                            type="button"
                            className="p-button-rounded p-button-success p-xs-box p-mr-1"
                            icon="pi pi-plus p-xxs-icon"
                            onClick={() => {
                                this.uploadRef.current.click();
                                gaPitchUploadClicked();
                            }}
                        />
                        <FormLabel>
                            {
                                // eslint-disable-next-line no-nested-ternary
                                pitchToEdit.file
                                    ? pitchToEdit.file.name
                                    : pitchToEdit.originalName
                                        ? pitchToEdit.originalName
                                        : 'upload slides in pdf or power point format (optional)'
                            }
                        </FormLabel>
                    </NoWrapRow>
                )}

                <NoWrapRow className="p-pl-3 p-pr-3 p-pt-3">
                    <FormLabel>pitch time:</FormLabel>
                </NoWrapRow>

                <NoWrapRow className="p-pl-3 p-pr-3 p-pt-3 p-d-flex">
                    {PITCH_TIMES.map(({ value, title }) => (
                        <div key={title}>
                            <RadioButton
                                inputId={`id_${value}`}
                                className="p-mr-1"
                                name="city"
                                value={value}
                                onChange={(e) => {
                                    pitchToEdit.time = parseInt(e.target.value, 10);
                                }}
                                checked={value === pitchToEdit.time}
                            />
                            <FormLabel htmlFor={`id_${value}`} className="p-mr-2">{`${title}`}</FormLabel>
                        </div>
                    ))}
                </NoWrapRow>

                <input
                    id="upload"
                    ref={this.uploadRef}
                    type="file"
                    name="presentation"
                    style={{ display: 'none' }}
                    onChange={(e) => {
                        const file = e.target.files[0];

                        if (!file || [PPTX_TYPE, PPT_TYPE, PDF_TYPE].indexOf(file.type) === -1) {
                            notifyStore.show({
                                message: 'At the moment, the only PDF or power point pitch decks are supported',
                                options: {
                                    variant: 'warn',
                                },
                            });
                            return;
                        }
                        const maxSize30MB = 31457280;
                        if (!file || file.size >= maxSize30MB) {
                            notifyStore.show({
                                message: (
                                    <div>
                                        File is too big, maximum allowed size is 30mb
                                        <br />
                                        <button
                                            className="p-rounded-4 p-button-info p-button-sm p-ml-2 p-mr-1"
                                            type="button"
                                            onClick={() => {
                                                // eslint-disable-next-line max-len
                                                window.open(
                                                    'https://www.adobe.com/acrobat/online/compress-pdf.html',
                                                    '_blank'
                                                );
                                            }}
                                        >
                                            PRESS TO OPTIMIZE YOUR PDF
                                        </button>
                                    </div>
                                ),
                                options: {
                                    variant: 'warn',
                                },
                            });
                            return;
                        }

                        pitchToEdit.file = file;
                    }}
                />
                <hr className="p-mt-3" />
                <NoWrapRow className="p-pl-3 p-pr-3 p-mt-2 p-d-flex p-jc-center">
                    <CapitalizedRobotoButton
                        label={
                            // eslint-disable-next-line no-nested-ternary
                            pitches.length === 0 ? (prevPath === ROUTES.MIRROR.path ? 'Back' : 'Skip') : 'Cancel'
                        }
                        className="p-button-rounded p-button-sm p-button-info p-ml-2 p-mr-1"
                        onClick={() => {
                            exit();
                        }}
                    />
                    <CapitalizedRobotoButton
                        disabled={pitchToEdit.name === ''}
                        type="submit"
                        label="Save"
                        className="p-button-rounded p-button-sm p-button-help"
                    />
                </NoWrapRow>
            </FullSizeForm>
        );
    }
}

export default PitchEditor;
