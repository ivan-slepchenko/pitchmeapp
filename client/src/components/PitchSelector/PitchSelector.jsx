import { useState } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { AddPitchArrow, LightRadioButton, PitchSelectorFramePitchesBlock } from '../../pages/Mirror/Styled';
import { ROUTES } from '../../routes';
import { AnnieTitle, BlackBorderedDiv, FormLabel, NoWrapRow } from '../../Styled';

function PitchSelector({ userStore, history, eventsStore }) {
    const visible = useState(true);
    const {
        user: { chosenPitchIndex, pitches },
        changeUserField,
    } = userStore;

    const {
        featureFlags
    } = eventsStore;

    const handlePitchChange = (event) => {
        changeUserField('chosenPitchIndex', parseInt(event.target.value, 10));
    };
    const formatPitchTime = (time) => `${time / 1000 / 60}min.`;

    return (
        <>
            {visible && (
                <BlackBorderedDiv>
                    {pitches.length === 0 ? (
                        <>
                            <NoWrapRow className="p-pl-3 p-pr-3 p-d-flex p-jc-center">
                                <FormLabel>Add your pitch to perform</FormLabel>
                                <AddPitchArrow className="p-ml-1 p-mr-1" />
                                <Button
                                    className="p-button-rounded p-button-secondary p-s-box"
                                    icon="pi pi-cog p-xs-icon"
                                    onClick={() => {
                                        history.push(ROUTES.PITCH_SETTINGS.path,{ prevPath: ROUTES.MIRROR.path });
                                    }}
                                />
                            </NoWrapRow>
                            <NoWrapRow className="p-d-flex p-jc-center p-mt-3 p-text-center">
                                <FormLabel>
                                    or give your live feedbacks without <br /> pitching
                                </FormLabel>
                            </NoWrapRow>
                        </>
                    ) : (
                        <>
                            {!featureFlags.NO_DECKS_DEFAULT_TIME.enabled && (<AnnieTitle className="p-mt-1 p-mb-2">I want to...</AnnieTitle>)}
                            <PitchSelectorFramePitchesBlock>
                                {!featureFlags.NO_DECKS_DEFAULT_TIME.enabled && (
                                    <FormLabel className="p-d-flex p-jc-center p-mt-1 p-mb-2">train my pitch:</FormLabel>
                                )}
                                {pitches.map((pitch, index) => (
                                    <NoWrapRow className="p-d-flex p-jc-left p-mt-2 p-mb-2" key={index}>
                                        <LightRadioButton
                                            inputId={`id_${index}`}
                                            className="p-mr-1 p-button-success-light"
                                            name={`id_${index}`}
                                            value={index}
                                            onChange={handlePitchChange}
                                            checked={index === chosenPitchIndex}
                                        />
                                        {!featureFlags.NO_DECKS_DEFAULT_TIME.enabled ? (
                                            <FormLabel htmlFor={`id_${index}`} className="p-mr-2">
                                                {`${pitch.name}: ${formatPitchTime(pitch.time)}`}
                                            </FormLabel>
                                        ) : (
                                            <FormLabel htmlFor={`id_${index}`} className="p-mr-2">I want to pitch</FormLabel>
                                        )}
                                    </NoWrapRow>
                                ))}
                            </PitchSelectorFramePitchesBlock>
                            <NoWrapRow className="p-d-flex p-jc-left p-mt-3 p-mb-1 p-ml-3">
                                <LightRadioButton
                                    inputId="id_-1"
                                    key={-1}
                                    className="p-mr-1"
                                    name="id_-1"
                                    value={-1}
                                    onChange={handlePitchChange}
                                    checked={chosenPitchIndex === -1}
                                />
                                <FormLabel htmlFor="id_-1" className="p-mr-2">
                                    {featureFlags.NO_DECKS_DEFAULT_TIME.enabled ? featureFlags.NO_DECKS_DEFAULT_TIME.values.imexpert : "I'm expert and just want to give feedbacks to others"}
                                </FormLabel>
                            </NoWrapRow>
                        </>
                    )}
                </BlackBorderedDiv>
            )}
        </>
    );
}

export default inject(({ userStore, eventsStore }) => ({ userStore, eventsStore }))(observer(PitchSelector));
