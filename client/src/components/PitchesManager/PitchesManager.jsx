import { Button } from 'primereact/button';
import { MAX_PITCHES_COUNT } from '../../config';
import { FormLabel, FullSizeContainer, NoWrapRow, CapitalizedRobotoButton, Title } from '../../Styled';
import { ROUTES } from '../../routes';
import Loader from '../UI/Loader';

export default function PitchesManager({
    pitches,
    createNewPitch,
    editPitch,
    deletePitch,
    goNextStep,
    prevPath,
    inUploadProcess,
}) {
    return (
        <FullSizeContainer>
            <Title>My Pitch Decks:</Title>

            {pitches.map((it) => (
                <NoWrapRow className="p-pl-1 p-pr-1 p-pb-3" key={it._id}>
                    <Button
                        className="p-button-rounded p-button-danger p-xs-box p-mr-1"
                        icon="pi pi-trash p-xxs-icon"
                        onClick={() => {
                            deletePitch(it);
                        }}
                    />
                    <Button
                        className="p-button-rounded p-button-info p-xs-box p-mr-1"
                        icon="pi pi-pencil p-xxs-icon"
                        onClick={() => {
                            editPitch(it);
                        }}
                    />
                    <FormLabel>{`${it.name} (${it.time / (1000 * 60)} min)`}</FormLabel>
                </NoWrapRow>
            ))}

            {inUploadProcess && (
                <NoWrapRow className="p-d-flex p-ai-center p-pl-1 p-pr-1 p-pb-3" key={-1}>
                    <Loader width="23px" height="23px" inner={<></>} />
                    <FormLabel className="p-ml-1 p-pl-1">processing...</FormLabel>
                </NoWrapRow>
            )}

            {pitches.length < MAX_PITCHES_COUNT && (
                <NoWrapRow className="p-pl-1 p-pr-0 p-d-flex">
                    <Button
                        className="p-button-rounded p-button-success p-xs-box p-mr-1"
                        icon="pi pi-plus p-xxs-icon"
                        onClick={() => {
                            createNewPitch();
                        }}
                    />
                </NoWrapRow>
            )}
            <hr className="p-mt-3" />
            <NoWrapRow className="p-pl-3 p-pr-3 p-mt-2 p-d-flex p-jc-center">
                <CapitalizedRobotoButton
                    label={prevPath === ROUTES.MIRROR.path ? 'Back' : 'Next'}
                    className="p-button-rounded p-button-sm p-button-help"
                    onClick={() => {
                        goNextStep();
                    }}
                />
            </NoWrapRow>
        </FullSizeContainer>
    );
}
