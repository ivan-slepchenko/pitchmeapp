// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { bind } from 'decko';
import { isMobile } from 'react-device-detect';
import { API_URL } from '../../utils/constants';
import { FullSizeContainer, Row, CapitalizedRobotoButton } from '../../Styled';
import { PresentationContainer } from './Styled';
import OrientationPattern from '../Layouts/OrientationPattern';
import { gaKeyUsedToControlPresentation } from '../../utils/ga';
import { BottomBar } from '../UI/Bars';

@inject(({ roomStore, notifyStore, userStore }) => ({ roomStore, notifyStore, userStore }))
@observer
class Presentation extends Component {
    state = {
        width: 0,
        height: 0,
    };

    componentDidMount() {
        const {
            roomStore: {
                pitch: { isMyTurn },
                activePitch,
            },
            userStore: { getIsOnboarded },
            notifyStore,
        } = this.props;
        this.updateDimensions();
        window.addEventListener('resize', this.updateDimensions);
        if (isMyTurn && activePitch.pages > 0) {
            if (!isMobile && !getIsOnboarded('P_YCSS')) {
                notifyStore.show({
                    message: 'You can switch slides with backspace and arrow keys',
                    options: {
                        variant: 'info',
                    },
                });
            }
            document.addEventListener('keydown', this.handleKeyDown, false);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimensions);
        document.removeEventListener('keydown', this.handleKeyDown, false);
    }

    @bind
    handleKeyDown(event) {
        const {
            roomStore: { prevPage, nextPage },
        } = this.props;
        if (event.keyCode === 37) {
            gaKeyUsedToControlPresentation();
            prevPage();
        }
        if (event.keyCode === 39 || event.keyCode === 32) {
            gaKeyUsedToControlPresentation();
            nextPage();
        }
    }

    @bind
    updateDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        const {
            roomStore: {
                setIsFeedbackOnDeck,
                setPDFPageLoaded,
                pitch: { isMyTurn, page, isFeedbackOnDeck, isPDFPageLoaded },
                room: { isVideoConnectionEstablished },
                activePitch,
                isPitchDeckVisible,
                prevPage,
                nextPage,
            },
        } = this.props;

        if (!activePitch && !isVideoConnectionEstablished) {
            return null;
        }
        const presentationFilePath = `${API_URL}/userstore/decks/${activePitch.fileUUID}`;

        const PdfViewer = (props) => {
            const { layout } = props;
            const isLandscape = layout === 'landscape';
            const { width, height } = this.state;
            const leftVH = 30;
            const leftAbsolute = height * (leftVH / 100);
            const bottomAbsolute = 45;

            const presentationWidth = `${isLandscape ? width - leftAbsolute : width}px`;
            const presentationHeight = isLandscape ? `${height - bottomAbsolute}px` : 'auto';
            const navigationButtons = isPDFPageLoaded ? (
                <>
                    <Button
                        className="p-button-rounded p-button-secondary p-button-xsm"
                        icon="pi pi-chevron-left"
                        onClick={prevPage}
                    />
                    {!isLandscape && (
                        <span>
                            {page} of {activePitch.pages} slides
                        </span>
                    )}
                    <Button
                        className="p-button-rounded p-button-secondary p-button-xsm"
                        icon="pi pi-chevron-right"
                        onClick={nextPage}
                    />
                </>
            ) : null;

            const areButtonsVisible = (isMyTurn || (!isMyTurn && isFeedbackOnDeck)) && activePitch.pages > 0;

            return (
                <FullSizeContainer
                    style={
                        isLandscape ? { marginLeft: `${leftVH}vh`, width: presentationWidth } : { paddingTop: '60%' }
                    }
                >
                    <img
                        width={presentationWidth}
                        height={presentationHeight}
                        src={presentationFilePath + (page === 1 ? '.jpg' : `-${page}.jpg`)}
                        onLoad={setPDFPageLoaded}
                        alt="presentation"
                        style={{
                            objectFit: 'contain',
                            objectPosition: 'top',
                        }}
                    />
                    {areButtonsVisible &&
                        (isLandscape ? (
                            <Row
                                className="p-d-flex p-jc-between p-ai-center p-p-2 p-white"
                                style={{
                                    position: 'absolute',
                                    top: '0px',
                                    width: presentationWidth,
                                }}
                            >
                                {navigationButtons}
                            </Row>
                        ) : (
                            <Row className="p-d-flex p-jc-between p-ai-center p-p-2 p-white">{navigationButtons}</Row>
                        ))}
                    {isFeedbackOnDeck && !isMyTurn && !isLandscape && (
                        <BottomBar>
                            <CapitalizedRobotoButton
                                label="Back To Ranking"
                                className="p-button-rounded p-button-info p-button-lg"
                                onClick={() => {
                                    setIsFeedbackOnDeck(false, true);
                                }}
                            />
                        </BottomBar>
                    )}
                </FullSizeContainer>
            );
        };

        return (
            <PresentationContainer className="p-white">
                {isPitchDeckVisible && (
                    <OrientationPattern
                        landscape={<PdfViewer layout="landscape" />}
                        portrait={<PdfViewer layout="portrait" />}
                    />
                )}
            </PresentationContainer>
        );
    }
}

export default Presentation;
