import styled from '@emotion/styled';
import { FullSizeContainer } from '../../Styled';

export const PresentationContainer = styled(FullSizeContainer)`
    background-color: #000;
    position: absolute;
`;
