// @ts-nocheck
import { Component } from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import { getURLParam } from 'utils/helpers';
import ls from 'utils/localStorage';
import { GET_PARAMS } from 'utils/constants/getParams';
import { LS_KEYS } from 'utils/constants/lsKeys';
import routes, { ROUTES } from 'routes';
import { isNotSupportedBrowser } from 'utils/unsupportedBrowser';

import Notifier from './Notifier/Notifier';
import ErrorBoundary from './ErrorBoundary';

const unsupportedBrowserWindowStyle = {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%)',
    textAlign: 'center',
    fontSize: 20,
};

function SmartRoute({ isAuthenticated, ...props }) {
    const { path } = props;

    const isRoot = [ROUTES.ROOT.path].includes(path);
    const inLogIn = [ROUTES.LOGIN.path].includes(path);

    // redirect to profile
    if (isAuthenticated && (inLogIn || isRoot)) {
        return <Route render={() => <Redirect to={`${ROUTES.PROFILE.path}`} />} />;
    }

    return <Route {...props} />;
}
@inject(({ userStore, routingStore }) => ({ userStore, routingStore }))
@observer
class Root extends Component {
    componentDidMount() {
        const meta = document.createElement('meta');
        meta.setAttribute('name', 'viewport');
        meta.setAttribute('content', 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no');
        document.head.appendChild(meta);
    }

    get UnsupportedBrowserWindow() {
        return (
            <div style={unsupportedBrowserWindowStyle}>
                <div>Your Browser doesn&apos;t support video connection</div>
                <div>Use modern Google Chrome Browser</div>
            </div>
        );
    }

    render() {
        const {
            userStore: { isAuth },
            routingStore,
        } = this.props;
        const isTester = getURLParam(GET_PARAMS.TESTER) === 'true';
        const needClearLS = getURLParam(GET_PARAMS.CLEAR_LS);

        if (isNotSupportedBrowser()) {
            return this.UnsupportedBrowserWindow;
        }

        if (isTester) {
            ls.set(LS_KEYS.TESTER, true);
        }

        if (needClearLS) {
            ls.clear();
        }

        return (
            <main>
                <Notifier />

                <ErrorBoundary>
                    <Router history={routingStore.history}>
                        <Switch>
                            {routes.map((route) => {
                                const { isPrivate, path } = route;
                                const needRedirectToLogin = isPrivate && !isAuth;

                                if (needRedirectToLogin) {
                                    return (
                                        <Route
                                            key="protected"
                                            render={() => <Redirect to={`${ROUTES.NO_AUTH_REDIRECT.path}`} push/>}
                                        />
                                    );
                                }

                                return <SmartRoute key={route.path} isAuthenticated={isAuth} {...route} />;
                            })}
                        </Switch>
                    </Router>
                </ErrorBoundary>
            </main>
        );
    }
}

export default Root;
