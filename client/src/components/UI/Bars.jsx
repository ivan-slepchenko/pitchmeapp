import styled from '@emotion/styled';

export const Bar = styled.div`
    position: absolute;
    z-index: 8;
    display: flex;
    justify-content: center;
    width: 100%;
`;

export const BottomBar = styled(Bar)`
    bottom: 20px;
    align-items: center;
`;

export const RoomControlBar = styled(Bar)`
    top: 50px;
`;
