import React from 'react';
import styled from '@emotion/styled';
import { FaCheck } from 'react-icons/fa';
import { MdClose } from 'react-icons/md';
import { inject, observer } from 'mobx-react';
import ReactGA from 'react-ga';

import Fab from 'components/UI/Fab';
import { CenteredContainer } from '../../Styled';

const Wrapper = styled.div`
    position: relative;
    cursor: pointer;
    box-shadow: 0 0 1px 1px #f24f8a;
    border-radius: 12px;

    .fake-it__label {
        position: absolute;
        right: -14px;
        top: -8px;
        background: #f50057;
        padding: 10px 14px;
        font-weight: bold;
        color: #fff;
        border-top-right-radius: 6px;
        border-top-left-radius: 12px;
        border-bottom-left-radius: 12px;
        font-size: 12px;
        z-index: 9;

        &::after {
            position: absolute;
            content: '';
            top: 100%;
            border-color: transparent;
            border-right-color: inherit;
            width: 0;
            height: 0;
            left: auto;
            right: 0;
            border-style: solid;
            border-width: 1.2em 1.2em 0 0;
            border-color: transparent;
            border-top-color: #7c1238;
        }
    }

    .fake-it__content {
        opacity: 0.5;
        padding: 0 10px;
        pointer-events: none;
    }
`;

const padding = '20px';
const ModalContainer = styled.div`
    max-width: 400px;
    min-width: 300px;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    background: #fff;
    color: #000;
    border-radius: 8px;
    outline: 0;
    overflow: hidden;

    .modal {
        &__close {
            position: absolute;
            top: 0;
            right: 0;
            width: 36px;
            height: 36px;
            fill: #fff;
            cursor: pointer;
        }

        &__header {
            padding: ${padding};
            background: #f50057;
            color: #fff;
            text-align: center;
            font-size: 24px;
        }

        &__body {
            padding: ${padding} 30px;
            line-height: 25px;
            text-align: center;

            &-benefits {
                margin-bottom: 10px;

                svg {
                    fill: #23cc40;
                }
            }
            b {
                color: darkorchid;
            }

            hr {
                margin: 20px;
            }
        }

        &__buy {
            min-width: 120px;
            padding: 0 8px;
        }

        &__price {
            font-size: 12px;
            color: #fff;
            padding-left: 6px;
            text-transform: lowercase;
        }

        &__footer {
            padding: 0 30px;
            display: flex;
            align-items: center;
            justify-content: space-between;

            button {
                min-width: 110px;
                height: 28px;
                opacity: 0.9;
            }
        }

        &__bottom-text {
            font-size: 12px;
            padding: 5px 20px;
            max-width: 280px;
            margin: auto;
        }
    }
`;

const benefits = [
    'Find friends by location and gender',
    'Build your friendlist',
    'Use masks and effects',
    'Play VIP games',
];

const fakeItTinyStyle = {
    fakeItTiny: {
        '&:before': {
            position: 'absolute',
            borderRadius: '10px',
            content: "'VIP'",
            background: '#d30000',
            left: '-10px',
            top: '-7px',
            fontSize: '10px',
            color: 'white',
            fontWeight: 'bold',
            padding: '0px 3px;',
        },
    },
};

function FakeIt({ children, gaLabel, tiny, classes, disabled }) {
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
        ReactGA.event({
            category: 'VIP',
            action: 'Blocked Click',
            label: gaLabel,
        });
    };
    const handleClose = () => {
        setOpen(false);
        ReactGA.event({
            category: 'VIP',
            action: 'Close Proposition',
            label: gaLabel,
        });
    };
    const handleBuy = () => {
        setOpen(false);
        ReactGA.event({
            category: 'VIP',
            action: 'Buy',
            label: gaLabel,
        });
    };

    if (disabled) {
        return children;
    }

    return (
        <>
            {tiny ? (
                React.Children.map(children, (child) =>
                    React.cloneElement(child, { className: classes.fakeItTiny, onClick: handleOpen })
                )
            ) : (
                <Wrapper onClick={handleOpen}>
                    <div className="fake-it__label">VIP</div>
                    <div className="fake-it__content">{children}</div>
                </Wrapper>
            )}
            <CenteredContainer open={open} onClose={handleClose}>
                <ModalContainer>
                    <MdClose className="modal__close" onClick={handleClose} />
                    <div className="modal__header">Feature Is Locked</div>
                    <div className="modal__body">
                        Feature Isn't Ready Yet
                        <br />
                        We are small but work really hard 💪
                        <br />
                        These features are the next to implement:
                        <hr />
                        {benefits.map((text) => (
                            <div key={text} className="modal__body-benefits">
                                <FaCheck /> text
                            </div>
                        ))}
                        <strong>Help us to deliver them faster!</strong>
                    </div>
                    <div className="modal__footer">
                        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick" />
                            <input type="hidden" name="hosted_button_id" value="W9EKDYS38ZHE6" />
                            <input
                                type="image"
                                src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif"
                                border="0"
                                name="submit"
                                title="PayPal - The safer, easier way to pay online!"
                                alt="Donate with PayPal button"
                            />

                            {/* eslint-disable-next-line */}
                            <img
                                alt=""
                                border="0"
                                src="https://www.paypal.com/en_LU/i/scr/pixel.gif"
                                width="1"
                                height="1"
                                onClick={handleBuy}
                            />
                        </form>

                        <Fab variant="extended" ghost="true" type="button" size="small" onClick={handleClose}>
                            Play
                        </Fab>
                    </div>

                    <div className="modal__bottom-text">
                        * every donation will be paid back with lifetime VIP access!
                    </div>
                </ModalContainer>
            </CenteredContainer>
        </>
    );
}

export default inject(({ notifyStore }) => ({ notifyStore }))(observer(FakeIt));
