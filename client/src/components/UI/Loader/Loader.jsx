import Wrapper from './Styled';

const Loader = ({ inner, width, height }) => (
    <div style={{ width, height }}>
        <Wrapper width={width} height={height} className="ui-loader">
            {inner}
            <i className="loading" />
        </Wrapper>
    </div>
);

export default Loader;
