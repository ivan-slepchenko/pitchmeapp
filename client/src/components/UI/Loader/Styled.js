import styled from '@emotion/styled';

// Loader...
const Loader = styled.div`
    z-index: 9;
    position: absolute;
    width: ${(props) => props.width};
    height: ${(props) => props.height};
    color: white;
    display: flex;
    align-items: center;
    justify-content: center;

    .loading {
        animation: rotation 1.8s infinite linear;
        border: 3px solid #fff;
        border-radius: 50%;
        border-top-color: rgb(43, 3, 81);
        height: 100%;
        width: 100%;
        position: absolute;
    }

    img {
        padding: 20px;
        background: rgb(43, 3, 81);
        border-radius: 50%;
        width: ${(props) => props.width};
        height: ${(props) => props.height};
    }

    @keyframes rotation {
        to {
            transform: rotate(360deg);
        }
    }
`;

export default Loader;
