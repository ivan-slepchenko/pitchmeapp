/* eslint-disable react/no-danger */
import { useState } from 'react';
import PropTypes from 'prop-types';
import styled from '@emotion/styled';
import { MdMoreHoriz as NextTextIcon, MdClear as CloseIcon } from 'react-icons/md';
import { css } from '@emotion/core';
import classNames from 'classnames';

const Bubble = styled.div`
    ${(props) =>
        !props.static &&
        css`
            position: absolute;
            transform: translate(-50%, -50%);
        `}

    text-align: center;
    text-transform: none;
    font-family: Roboto, Annie;
    background: rgba(0, 0, 0, 0.66);
    border-radius: 10px;
    padding: 10px 8px;
    user-select: none;
    min-width: 200px;
    max-width: 400px;
    z-index: 1;

    .hint {
        &__btn-wrapper {
            margin: 0;
            outline: none;
            border: none;
            background: transparent;
            color: #fff;
            cursor: pointer;
        }

        &__close-btn {
            position: absolute;
            right: 5px;
            top: 5px;
            width: 15px;
            height: 15px;
            padding: 0px;

            svg {
                width: 15px;
                height: 15px;
            }
        }
    }
    .text_block {
        margin-right: 10px;
        margin-left: 10px;

        &--closeable {
            padding-top: 15px;
        }
    }
`;

function TextHint(props) {
    const { closable, withHtml, hidden } = props;
    let { children } = props;
    const [closed, closeHint] = useState(false);
    // multiple mode features
    const multipleMode = children instanceof Array;
    const count = children.length;
    const randomIndex = Math.round((children.length - 1) * Math.random());
    const [activeIndex, changeActiveIndex] = useState(randomIndex);
    const hintClass = classNames({
        text_block: true,
        'text_block--closeable': closable,
    });

    if (hidden || closed) {
        return null;
    }

    if (multipleMode) {
        children = children[activeIndex];
    }

    return (
        <Bubble {...props} closed={closed}>
            {withHtml ? <div className={hintClass} dangerouslySetInnerHTML={{ __html: children }} /> : children}

            {closable && (
                <button type="button" className="hint__btn-wrapper hint__close-btn" onClick={() => closeHint(true)}>
                    <CloseIcon />
                </button>
            )}

            {multipleMode && (
                <button
                    type="button"
                    className="hint__btn-wrapper"
                    onClick={() => {
                        const nextIndex = activeIndex + 1;
                        const newIndex = nextIndex >= count ? 0 : nextIndex;

                        changeActiveIndex(newIndex);
                    }}
                >
                    <NextTextIcon />
                </button>
            )}
        </Bubble>
    );
}

TextHint.propTypes = {
    /**
     * You can use React element for single mode
     * or Array with elements for multiple mode
     */
    children: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.arrayOf(PropTypes.node)]).isRequired,
    closeabe: PropTypes.bool,
    withHtml: PropTypes.bool,
    static: PropTypes.bool,
};

export default TextHint;
