import styled from '@emotion/styled';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/components/inputtextarea/InputTextarea';

export const TextInputField = styled(InputText)`
    background-color: #262626;
    font-size: 18px;
    color: white;
    font-family: Roboto;
    border-radius: 5px;
`;
export const TextInputArea = styled(InputTextarea)`
    background-color: #262626;
    font-size: 18px;
    color: white;
    font-family: Roboto;
    border-radius: 5px;
`;
