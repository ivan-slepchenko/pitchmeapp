export const MAX_PITCHES_COUNT = 3;
export const FEEDBACK = {
    a: {
        label: 'storytelling',
        description: 'Rank with 5 starts if you enjoyed the story of the pitch',
    },
    b: {
        label: 'structure',
        description: 'Rank with 5 stars, if you feel the pitch is well balanced',
    },
    c: {
        label: 'conciseness',
        description: 'Rank with 5 stars, if you can explain the startup idea by your own words',
    },
    d: {
        label: 'speech clarity and grammar',
        description: "Rank with 5 stars, if you don't see any pitch grammar issues",
    },
    e: {
        label: 'call to action',
        description: 'Rank with 5 stars, if you feel strong cal to action in the pitch',
    },
    f: {
        label: 'call to emotion',
        description: 'Rank with 5 stars if you got emotionally connected to the story',
    },
    text: {
        label: 'write down your feedback key points',
    },
};
export const TWILIO_ACCOUNT_SID = 'ACafb4624f7a59ec72f69991271986812b';
export const TWILIO_API_KEY = 'SK05b723147d3f453b5f9f9adeb52a8f45';
export const TWILIO_API_SECRET = 'hAtsAlppScBZnHJJthW1dfCMgr3vgBJH';
