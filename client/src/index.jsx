import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Provider as MobxProvider } from 'mobx-react';
import LogRocket from 'logrocket';
import { Global } from '@emotion/core';
import { IS_DEV } from 'utils/constants';
import reduxStore from 'store/reduxStore';
import Store from 'store';
import Root from 'components/Root';
import { globalCss } from './Styled';

import 'primereact/resources/themes/vela-purple/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.scss';
import FullScreen from './components/Layouts/FullScreen';

const mixpanel = require('mixpanel-browser');

mixpanel.init('6e4b354f39d1097deca434ceb998a124', { api_host: 'https://api-eu.mixpanel.com' }, '');

if (!IS_DEV) {
    LogRocket.init('o8saq1/sparkmeclub');
    LogRocket.identify('applet');
}

const mobxStore = new Store();

const App = () => (
    <Provider store={reduxStore}>
        <MobxProvider {...mobxStore}>
            <FullScreen>
                <Global styles={globalCss} />
                <Root />
            </FullScreen>
        </MobxProvider>
    </Provider>
);

const rootElement = document.getElementById('root');
render(<App />, rootElement);
