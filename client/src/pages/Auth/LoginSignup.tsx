// @ts-nocheck
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';

import { Link } from 'react-router-dom';
import ls from 'utils/localStorage';
import { HoneYourPitchTitle, LoginSignupTitle } from './Styled';
import SocialAuth from './components/SocialAuth';
import { CenteredContainer, FullSizeAbsoluteFlexColumnContainer } from '../../Styled';
import OrientationPattern from '../../components/Layouts/OrientationPattern';
import OnBoardingCarousel from './components/OnBoarding/OnBoardingCarousel';

@inject(({ eventsStore, userStore }) => ({ eventsStore, userStore }))
@observer
class LoginSignup extends Component {
    render() {
        const {
            eventsStore: { eventLogo, featureFlags, featureFlagsFetched },
            userStore: { inviteCode },
        } = this.props;
        const isOnBoarded = ls.get('isOnBoarded');
        const portraitLayout = (
            <FullSizeAbsoluteFlexColumnContainer>
                <CenteredContainer>
                    <img src={eventLogo} alt="event logo" />
                    <LoginSignupTitle className="p-mt-4">Sign Up / Login</LoginSignupTitle>
                    <div className="p-pt-3 p-pb-3">with</div>
                    <SocialAuth className="p-mt-3" />
                    <div className="p-text-center p-mt-6">by using this app, you agree with</div>
                    <Link className="p-mt-3 p-mb-6" to="/agreement" style={{ color: '#8b6efa' }} target="_blank">
                        Terms And Conditions
                    </Link>
                </CenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );
        const landscapeLayout = (
            <FullSizeAbsoluteFlexColumnContainer>
                <CenteredContainer>
                    <img src={eventLogo} alt="event logo" />
                    <LoginSignupTitle className="p-mt-2">Sign Up / Log In</LoginSignupTitle>
                    <div className="p-pt-3 p-pb-3">with</div>
                    <SocialAuth />
                    <div className="p-text-center p-mt-0">by using this app, you agree with</div>
                    <Link className="p-mt-3 p-mb-0" to="/agreement" style={{ color: '#8b6efa' }} target="_blank">
                        Terms And Conditions
                    </Link>
                </CenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );
        return (
            <>
                {featureFlagsFetched && (
                    <>
                        {isOnBoarded || !featureFlags.BOARDING_SLIDES.enabled ? (
                            <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />
                        ) : (
                            <OnBoardingCarousel style={{ width: '100%', height: '100%' }} />
                        )}
                        {inviteCode && (
                            <div
                                style={{ position: 'absolute', right: '0px', top: '0px' }}
                                className="p-d-flex p-dir-row p-align-center p-p-1"
                            >
                                <div>driven by pitchme.app</div>
                            </div>
                        )}
                    </>
                )}
            </>
        );
    }
}

export default LoginSignup;
