import styled from '@emotion/styled';
import { FaFacebook, FaGoogle, FaLinkedin } from 'react-icons/all';

export const FaFacebookSocial = styled(FaFacebook)`
    width: 60px !important;
    height: 60px !important;
    fill: #3a5ba0 !important;
`;
export const FaGoogleSocial = styled(FaGoogle)`
    width: 60px !important;
    height: 60px !important;
    fill: #1369ea !important;
`;

export const FaLinkedinSocial = styled(FaLinkedin)`
    width: 60px !important;
    height: 60px !important;
    fill: #0e76a8 !important;
`;
export const HoneYourPitchTitle = styled.div`
    color: #2a0452;
    font-size: 18px;
`;

export const LoginSignupTitle = styled.div`
    color: #2a0452;
    font-size: 24px;
`;

export const AgreementBlock = styled.div`
    border: 1px solid rgba(155, 81, 224, 0.95);
    background: rgba(255, 255, 255, 0.92);
    border-radius: 8px;
    box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.2);
    padding: 20px;
    margin: 10px;
    position: absolute;
    height: 100%;
    overflow: auto;
    color: #444;
`;
