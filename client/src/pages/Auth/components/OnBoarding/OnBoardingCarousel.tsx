// @ts-nocheck
import { Component } from 'react';
import { Carousel } from 'primereact/carousel';
import { bind } from 'decko';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import ls from 'utils/localStorage';
import { withRouter } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import { OnBoardingCarouselWrapper } from './Styled';
import { FullSizeContainer } from '../../../../Styled';
import { ROUTES } from '../../../../routes';

@inject(({ eventsStore }) => ({ eventsStore }))
@withRouter
@observer
class OnBoardingCarousel extends Component {
    state = {
        page: 0,
    };

    slides = [
        {
            title: 'Welcome to',
            // eslint-disable-next-line max-len
            subtitle:
                'With Pitch Me, you can attend online startup pitch events and exchange feedback with other founders.',
            index: '0',
        },
        {
            title: 'Setup your pitch',
            // eslint-disable-next-line max-len
            subtitle: 'Start by adjusting your pitch time and uploading a pitch deck that you can present to others.',
            index: '1',
        },
        {
            title: 'Join a counterpart',
            // eslint-disable-next-line max-len
            subtitle:
                "Next, connect with other founders to pitch your startup or simply join to listen to others' pitches.",
            index: '2',
        },
        {
            title: 'Pitch / Get Feedback',
            // eslint-disable-next-line max-len
            subtitle:
                'Pitch your startup, collect feedback from your counterpart, then switch roles or connect with someone new!',
            index: '3',
        },
        {
            title: 'Switch Roles',
            // eslint-disable-next-line max-len
            subtitle:
                "After pitching and exchanging feedback with other founders, you'll be able to exit the event with valuable feedback.",
            index: '4',
        },
        {
            title: 'Check Leaderboard & Stats',
            // eslint-disable-next-line max-len
            subtitle:
                'Every event has a leaderboard, where you can see how your Pitch stacks up to others, as well as a personal progress page.',
            index: '5',
        },
    ];

    carouselElement;

    componentDidMount() {
        if (!isMobile) {
            document.addEventListener('keydown', this.handleKeyDown, false);
        }
    }

    componentWillUnmount() {
        if (!isMobile) {
            document.removeEventListener('keydown', this.handleKeyDown, false);
        }
    }

    @bind
    handleKeyDown(event) {
        if (event.keyCode === 37) {
            this.prevPage();
        }
        if (event.keyCode === 39 || event.keyCode === 32) {
            this.nextPage();
        }
        event.preventDefault();
    }

    @bind
    restartFade() {
        const carousel = document.getElementById('carousel');
        carousel.style.animation = 'none';
        carousel.style.opacity = 0;
        setTimeout(() => {
            carousel.style.animation = null;
            carousel.style.opacity = null;
        }, 30);
    }

    @bind
    nextPage() {
        const { page } = this.state;
        this.restartFade();
        if (page === this.slides.length - 1) {
            this.finishBoardingSlides();
        } else {
            this.setState({ page: Math.min(page + 1, this.slides.length - 1) });
            this.forceUpdate();
        }
    }

    @bind
    prevPage() {
        const { page } = this.state;
        this.restartFade();
        this.setState({ page: Math.max(page - 1, 0) });
        this.forceUpdate();
    }

    @bind
    finishBoardingSlides() {
        const { history } = this.props;
        ls.set('isOnBoarded', true);
        history.push({ pathname: ROUTES.LOGIN.path });
    }

    @bind
    itemTemplate(originalSlide) {
        const { page } = this.state;
        const {
            eventsStore: { eventLogo },
        } = this.props;

        const slide = isMobile ? originalSlide : this.slides[page];
        const isFinalPage = slide === this.slides[this.slides.length - 1];
        const isFirstPage = slide === this.slides[0];

        const imgURL = `/media/images/boarding/slide_${slide.index}.png`;
        return (
            <FullSizeContainer
                className="p-d-flex p-dir-col p-jc-center p-ai-center"
                onClick={(e) => {
                    if (!isMobile) {
                        this.nextPage();
                        e.preventDefault();
                    }
                }}
            >
                <div className="p-text-center">
                    <h2>{slide.title}</h2>
                </div>
                {isFirstPage && eventLogo && (
                    <>
                        <img src={eventLogo} alt="event logo" />
                        <div className="p-text-center">
                            <h2>cross-pitching event!</h2>
                        </div>
                    </>
                )}
                <div
                    style={{
                        backgroundImage: `url(${imgURL})`,
                        width: '220px',
                        height: '130px',
                        backgroundPosition: 'center',
                        backgroundRepeat: 'no-repeat',
                        backgroundSize: 'contain',
                    }}
                    alt="app.pitchme.app"
                />
                <div className="p-text-center p-mt-3" style={{ maxWidth: '400px' }}>
                    {slide.subtitle}
                </div>
                {isFinalPage && (
                    <Button
                        className="p-button-rounded p-button-help p-mt-4 p-button-success"
                        label="Let's do it!"
                        onClick={this.finishBoardingSlides}
                    />
                )}
            </FullSizeContainer>
        );
    }

    @bind
    carousel(page) {
        if (!isMobile) {
            return (
                <Carousel
                    id="carousel"
                    className="fade-in"
                    tabindex="-1"
                    numVisible={1}
                    value={this.slides}
                    itemTemplate={this.itemTemplate}
                    page={page}
                    onPageChange={(e) => this.setState({ page: e.page })}
                />
            );
        }
        return <Carousel tabindex="-1" numVisible={1} value={this.slides} itemTemplate={this.itemTemplate} />;
    }

    render() {
        const { page } = this.state;
        return <OnBoardingCarouselWrapper>{this.carousel(page)}</OnBoardingCarouselWrapper>;
    }
}
export default OnBoardingCarousel;
