import styled from '@emotion/styled';
import { FullSizeAbsolute } from '../../../../Styled';

export const OnBoardingCarouselWrapper = styled(FullSizeAbsolute)`
    .p-carousel {
        width: 100%;
        height: 100%;
    }
    .p-carousel-content {
        width: 100%;
        height: 100%;
    }
    .p-carousel-container {
        width: 100%;
        height: 100%;
    }
    .p-carousel-items-container {
        height: 100%;
    }
    .fade-in {
        animation: fadeIn ease 1s;
    }
    @keyframes fadeIn {
        0% {
            opacity: 0;
        }
        100% {
            opacity: 1;
        }
    }
`;
