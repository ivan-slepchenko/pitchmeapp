import { API_URL } from 'utils/constants';
import { AuthBtn, Wrapper } from './Styled';
import { FaFacebookSocial, FaGoogleSocial, FaLinkedinSocial } from '../../Styled';

function SocialAuth() {
    const baseHref = `${API_URL}/api/auth`;

    return (
        <>
            <Wrapper>
                <AuthBtn href={`${baseHref}/facebook`}>
                    <FaFacebookSocial />
                </AuthBtn>

                <AuthBtn href={`${baseHref}/google`}>
                    <FaGoogleSocial />
                </AuthBtn>

                <AuthBtn href={`${baseHref}/linkedin`}>
                    <FaLinkedinSocial />
                </AuthBtn>
            </Wrapper>
        </>
    );
}

export default SocialAuth;
