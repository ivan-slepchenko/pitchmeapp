import styled from '@emotion/styled';

const Wrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`;

const AuthBtn = styled.a`
    margin: 0 10px !important;
    padding: 6px;
    display: flex;
    align-items: center;
    justify-content: center;
    background: none;
    cursor: pointer;

    svg {
        width: 32px;
        height: 32px;
        fill: #3c3c3c;
    }
`;

const Divider = styled.div`
    position: relative;
    text-align: center;
    color: #444;
    text-transform: uppercase;
    font-size: small;

    &::after {
        content: '';
        position: absolute;
        top: 7px;
        left: 0;
        width: 100%;
        height: 1px;
        background: #c2c1c2;
    }

    span {
        position: relative;
        z-index: 1;
        padding: 0 5px;
        background: #efebef;
    }
`;

export { AuthBtn, Wrapper, Divider };
