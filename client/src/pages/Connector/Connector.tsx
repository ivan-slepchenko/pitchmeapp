// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';

import FullScreen from 'components/Layouts/FullScreen';

import SearchGame from './Search';
import Room from './Room';
import OpponentPreview from './OpponentPreview/OpponentPreview';

@inject(({ roomStore }) => ({ roomStore }))
@observer
class Connector extends Component {
    displayConnectingModeTimeout = null;

    async componentDidMount() {
        const {
            roomStore: { waitForRoomAndInitSocket },
        } = this.props;

        waitForRoomAndInitSocket();
    }

    componentWillUnmount() {
        const {
            roomStore: { destroySocket },
        } = this.props;

        destroySocket();
    }

    get Stage() {
        const {
            roomStore: {
                room: { id: roomId },
            },
        } = this.props;

        const inRoom = Boolean(roomId);

        if (!inRoom) {
            return <SearchGame />;
        }

        return <Room />;
    }

    render() {
        return <FullScreen>{this.Stage}</FullScreen>;
    }
}

export default Connector;
