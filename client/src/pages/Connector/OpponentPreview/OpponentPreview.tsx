// @ts-nocheck
import { Component } from 'react';
import 'react-resizable/css/styles.css';
import { ROUTES } from 'routes';
import Loader from 'components/UI/Loader';
import { Button } from 'primereact/button';
import { connect } from 'react-redux';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { BottomBar } from '../../../components/UI/Bars';
import {
    AbsoluteCenteredContainer,
    FullSizeAbsoluteFlexColumnContainer,
    NoWrapRow,
    RoundedImage,
    TopThirdContainer,
} from '../../../Styled';
import { ColorTintWrapper } from '../Room/Media/Styled';
import { InfoRow, Title } from './Styled';
import OrientationPattern from '../../../components/Layouts/OrientationPattern';
import { getUserThumbnail } from '../../../utils/helpers';

@connect()
@inject(({ roomStore, userStore }) => ({ roomStore, userStore }))
@observer
class OpponentPreview extends Component {
    render() {
        const {
            roomStore: {
                leaveRoom,
                room: {
                    companion: { id, name, startupName, info },
                },
            },
        } = this.props;
        const photo = getUserThumbnail(id);
        const { history } = this.props;
        const mirrorButton = (
            <Button
                className="p-button-rounded p-button-secondary p-button-rounded-lg p-mt-2"
                icon="pi pi-times"
                onClick={() => {
                    leaveRoom();
                    history.push({ pathname: ROUTES.MIRROR.path });
                }}
            />
        );
        const startupNameBlock = startupName?.length > 0 ? <InfoRow className="p-mt-2 p-mb-2 p-pl-2 p-pr-2">PROJECT NAME: {startupName}</InfoRow> : <></>;
        const infoBlock = info?.length > 0 ? <InfoRow className="p-pl-2 p-pr-2">INFO: {info}</InfoRow> : <></>;
        const nameBlock = (
            <NoWrapRow className="p-mt-2 p-text-center p-pr-2 p-pl-2">
                <Title>{name}</Title>
            </NoWrapRow>
        );
        const connectingBlock = <NoWrapRow className="p-mt-2  p-text-center">connecting...</NoWrapRow>;
        const landscapeLayout = (
            <FullSizeAbsoluteFlexColumnContainer>
                <AbsoluteCenteredContainer className="p-white">
                    <RoundedImage width="80px" height="80px" src={photo} alt={photo} />
                    {connectingBlock}
                    {nameBlock}
                    {startupNameBlock}
                    {infoBlock}
                    {mirrorButton}
                </AbsoluteCenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );
        const portraitLayout = (
            <>
                <TopThirdContainer className="p-white">
                    {nameBlock}
                    {startupNameBlock}
                    <Loader
                        className="p-mt-4"
                        width="130px"
                        height="130px"
                        inner={<img width="130px" height="130px" src={photo} alt={photo} />}
                    />
                    <div className="p-mt-2">{infoBlock}</div>
                    {connectingBlock}
                </TopThirdContainer>
                <BottomBar>{mirrorButton}</BottomBar>
            </>
        );

        return (
            <>
                {/* <MirrorVideo /> */}
                <ColorTintWrapper />
                <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />
            </>
        );
    }
}

export default withRouter(OpponentPreview);
