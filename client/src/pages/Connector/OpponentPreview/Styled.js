import styled from '@emotion/styled';

export const Title = styled.span`
    font-size: 30px;
`;

export const InfoRow = styled.div`
    display: block;
    word-break: break-word;
    width: 100%;
    text-align: center;
`;
