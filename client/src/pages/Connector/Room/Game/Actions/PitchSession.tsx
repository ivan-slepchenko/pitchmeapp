// @ts-nocheck
import React  from 'react';
import { inject, observer } from 'mobx-react';
import { CRAFTING_FEEDBACK, INTRO, PITCH, REVIEWING_FEEDBACK } from '../../../../../utils/constants/pitchFlow';
import IntroduceYourself from './states/IntroduceYourself';
import WaitForOpponentToStart from './states/WaitForOpponentToStart';
import ListenForThePitch from './states/ListenForThePitch';
import PerformThePitch from './states/PerformThePitch';
import WaitForFeedback from './states/WaitForFeedback';
import WaitForFeedbackToBeReviewed from './states/WaitForFeedbackToBeReviewed';
import ReviewFeedback from './states/ReviewFeedback';
import { FullSizeContainer } from '../../../../../Styled';
import CraftFeedback from './states/CraftFeedback';

const PitchSession = inject('roomStore', 'eventsStore')(
    observer(
        ({
            roomStore: {
                pitch: { state: pitchState, isMyTurn },
                room: { isVideoConnectionEstablished },
            }
        }) => {
            if (!isVideoConnectionEstablished) {
                return <></>;
            }

            let element = {
                [INTRO]: isMyTurn ? <IntroduceYourself /> : <WaitForOpponentToStart />,
                [PITCH]: isMyTurn ? <PerformThePitch /> : <ListenForThePitch />,
                [CRAFTING_FEEDBACK]: isMyTurn ? <WaitForFeedback /> : <CraftFeedback />,
                [REVIEWING_FEEDBACK]: isMyTurn ? <ReviewFeedback /> : <WaitForFeedbackToBeReviewed />,
            }[pitchState];
            element = element == null ? pitchState : element;
            return <FullSizeContainer>{element}</FullSizeContainer>;
        }
    )
)

export default PitchSession;
