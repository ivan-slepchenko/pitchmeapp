import styled from '@emotion/styled';
import { css } from '@emotion/core';
import { Rating } from 'primereact/rating';
import { InputTextarea } from 'primereact/components/inputtextarea/InputTextarea';
import { BlackBorderedDiv, FullSizeAbsoluteFlexColumnContainer } from '../../../../../Styled';

export const LandscapeRatingBlock = styled.div`
    width: 700px !important;
    flex-wrap: wrap;
    justify-content: flex-start;
    display: flex;
    flex-direction: column;
    height: ${(props)=>(`${props.displayInOneLine ? '70px' : '120px'  } !important;`)}
    align-items: center;
`;

export const RateCategoryBlock = styled.div`
    text-align: center;
    opacity: ${(props) => (props.shown ? 1 : 0)};
    transition: ${(props) => (props.shown ? '1s' : '0s')};
`;

export const RateForSlotBlock = styled.div`
    width: 100%;
    text-align: center;
    opacity: ${(props) => (props.inAnimationPhase ? '0' : '1')};
    transition: 0.5s ease-in;
`;

export const YellowRating = styled(Rating)`
    text-align: center;
    .p-rating-icon {
        color: #ffc10e;
        font-size: ${(props) => (props.big ? '2rem' : '1.343rem')};
        .pi-star {
            color: #ffc10e;
            font-size: ${(props) => (props.big ? '2rem' : '1.343rem')};
        }
    }
    .p-rating-icon:hover {
        color: #ffc10e !important;
    }
    .p-rating-icon:focus {
        outline-style: none;
        box-shadow: none;
        border-color: transparent;
        color: #ffc10e;
    }
    .pi-star {
        color: #ffc10e !important;
        font-size: ${(props) => (props.big ? '2rem' : '1.343rem')};
    }
`;

export const VerticalFullSizeRoomDialogContainer = styled(FullSizeAbsoluteFlexColumnContainer)`
    padding-top: 90px !important;
    height: 100%;
`;

export const HorizontalFullSizeRoomDialogContainer = styled(FullSizeAbsoluteFlexColumnContainer)`
    padding-bottom: 40px !important;
    height: 100%;
`;

export const RatingDiv = styled(BlackBorderedDiv)`
    border-radius: 50px;
    max-width: 700px;
    width: 100%;
    zoom: 0.9;
`;

export const OvertimeLabel = styled.div`
    font-size: 18px;
    font-weight: bold;
    color: #ff0000;
`;

export const RankPositionLabel = styled.div`
    font-size: 18px;
    font-weight: bold;
    color: #007ab9;
`;

export const RankIsLocked = styled.div`
    font-size: 18px;
    font-weight: bold;
    color: #ffc10e;
`;

export const PillowTitle = styled.div`
    background-color: rgba(0, 0, 0, 0.3);
    border-radius: 20px;
    padding: 3px;
    min-width: 300px;
    max-width: 500px;
    text-align: center;
`;

export const PillowLabel = styled.div`
    background-color: ${(props) => (props.shadowWholeBlock ? 'rgba(0, 0, 0, 0)' : 'rgba(0, 0, 0, 0.3)')};
    border-radius: 20px;
    padding: 3px;
    width: 230px;
    text-align: center;
`;

export const FeedbackTextArea = styled(InputTextarea)`
    width: 330px;
    background-color: #262626;
    font-size: 18px;
    color: white;
    font-family: Roboto;
    border-radius: 10px;
    max-height: 160px;
`;

export const OfferPopup = styled.div`
    width: 250px;
    position: absolute;
    background: #202124;
    padding: 12px;
    bottom: 66px;
    right: 0;
    border-radius: 10px;
    box-shadow: 0 0 4px 2px rgba(0, 0, 0, 0.2);
    visibility: hidden;
    opacity: 0;
    transition: 0.4s;

    ${({ active }) =>
        active &&
        css`
            visibility: visible;
            opacity: 1;
        `}

    .popup-title {
        color: white;
        margin-bottom: 10px;
    }

    .game-list {
        display: flex;
        flex-direction: column;

        button {
            border-radius: 30px;
            border-width: 2px;
            margin-bottom: 8px;
            border-color: #fff;
            background: rgba(255, 255, 255, 0.1);
            color: #fff;
        }
    }
`;
