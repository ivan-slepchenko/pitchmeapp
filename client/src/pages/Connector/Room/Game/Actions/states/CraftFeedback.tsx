// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { bind } from 'decko';
import { isMobile } from 'react-device-detect';
import { AnnieTitle, FlexNoWrapRow, FormLabel } from '../../../../../../Styled';
import {
    FeedbackTextArea,
    VerticalFullSizeRoomDialogContainer,
    PillowTitle,
    RateCategoryBlock,
    RateForSlotBlock,
    RatingDiv,
    YellowRating,
    HorizontalFullSizeRoomDialogContainer,
} from '../Styled';
import { REVIEWING_FEEDBACK } from '../../../../../../utils/constants/pitchFlow';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

@inject(({ roomStore, notifyStore, userStore, eventsStore }) => ({ roomStore, notifyStore, userStore, eventsStore }))
@observer
class CraftFeedback extends Component {
    state = {
        pageIndex: 0,
        hidden: false,
        inAnimationPhase: false,
    };

    @bind
    getPageToFeedbackSlot() {
        const {eventsStore:{eventQuestions}} = this.props;
        const result = [];
        if(eventQuestions.a) {
            result.push('a');
        }
        if(eventQuestions.b) {
            result.push('b');
        }
        if(eventQuestions.c) {
            result.push('c');
        }
        if(eventQuestions.d) {
            result.push('d');
        }
        if(eventQuestions.e) {
            result.push('e');
        }
        if(eventQuestions.f) {
            result.push('f');
        }
        result.push('text');
        return result;
    };

    @bind
    computePitchOvertime() {
        const {
            roomStore: {
                timing: { startPitch, endPitch },
                room: {
                    companion: { chosenPitch },
                },
            },
        } = this.props;

        const pitchLength = endPitch - startPitch;
        return Math.round((pitchLength - chosenPitch.time) / 1000);
    }

    @bind
    submitCategoryScore(e, index) {
        const {
            roomStore: { feedback },
        } = this.props;
        const currentFeedbackSlotLabel = this.getPageToFeedbackSlot()[index];
        feedback[currentFeedbackSlotLabel] = currentFeedbackSlotLabel === 'text' ? e.target.value : e.value;
    }

    @bind
    submitCategoryScoreAndGoNextStep(e) {
        const { pageIndex, inAnimationPhase } = this.state;
        if (inAnimationPhase) {
            return;
        }
        this.submitCategoryScore(e, pageIndex);
        this.setState({ inAnimationPhase: true });
        setTimeout(() => {
            this.setState({ pageIndex: pageIndex + 1, hidden: true });
            setTimeout(() => {
                this.setState({ hidden: false });
                this.setState({ inAnimationPhase: false });
            }, 10);
        }, 500);
    }

    render() {
        const {
            roomStore: {
                setPitchState,
                setIsFeedbackOnDeck,
                isPitchDeckExist,
                pitch: { isFeedbackOnDeck },
                feedback,
            },
            eventsStore: {
                eventQuestions
            }
        } = this.props;

        const { pageIndex, hidden, inAnimationPhase } = this.state;

        if (isFeedbackOnDeck) {
            return null;
        }

        const currentFeedbackSlotLabel = this.getPageToFeedbackSlot()[pageIndex];

        const alreadyRatedRows = [];
        for (let i = 0; i < Math.min(pageIndex, this.getPageToFeedbackSlot().length - 1); i++) {
            const rankedSlotLabel = this.getPageToFeedbackSlot()[i];
            alreadyRatedRows.unshift(
                <RateCategoryBlock shown={!hidden || i !== pageIndex - 1}>
                    <FormLabel>{eventQuestions[rankedSlotLabel].label}</FormLabel>
                    <YellowRating
                        value={feedback[rankedSlotLabel]}
                        cancel={false}
                        onChange={(e) => {
                            this.submitCategoryScore(e, i);
                        }}
                    />
                </RateCategoryBlock>
            );
        }

        const alreadyRatedRowsBlock = (
            <>
                {(alreadyRatedRows.length && pageIndex < (this.getPageToFeedbackSlot().length - 1)) > 0 && (
                    <hr className="p-mb-3 p-mt-3" style={{ width: '100%' }} />
                )}
                {alreadyRatedRows}
            </>
        );

        const ratingControls = (
            <>
                {pageIndex < (this.getPageToFeedbackSlot().length - 1) ? (
                    <RateForSlotBlock
                        inAnimationPhase={isMobile && inAnimationPhase}
                        className="p-d-flex p-dir-col p-ai-center"
                    >
                        <PillowTitle className="x-large-text p-mt-3 p-mb-3 p-pl-3 p-pr-3">
                            {eventQuestions[currentFeedbackSlotLabel].label}
                        </PillowTitle>
                        <YellowRating
                            big
                            value={feedback[currentFeedbackSlotLabel]}
                            cancel={false}
                            onChange={this.submitCategoryScoreAndGoNextStep}
                            className="p-mb-3"
                        />
                        <FormLabel className="large-text">* {eventQuestions[currentFeedbackSlotLabel].description}</FormLabel>
                    </RateForSlotBlock>
                ) : (
                    <FeedbackTextArea
                        size="small"
                        className="p-mt-2 p-mb-2"
                        name="info"
                        placeholder="Write your feedback key points:"
                        autoResize
                        value={feedback[currentFeedbackSlotLabel]}
                        rows={5}
                        onChange={(e) => {
                            this.submitCategoryScore(e, this.getPageToFeedbackSlot().length - 1);
                        }}
                    />
                )}
                <OrientationPattern
                    portrait={alreadyRatedRowsBlock}
                    landscape={isMobile ? null : alreadyRatedRowsBlock}
                />
            </>
        );

        const slidesButton = isPitchDeckExist ? (
            <Button
                className="p-button-rounded p-mr-2 p-button-info"
                label="View The Deck"
                onClick={() => {
                    setIsFeedbackOnDeck(true, true);
                }}
            />
        ) : null;

        const submitButton = (
            <Button
                className="p-button-rounded p-button-help"
                label="Submit"
                onClick={(e) => {
                    setPitchState(REVIEWING_FEEDBACK);
                }}
            />
        );

        const orientationIndependentBlock = (
            <RatingDiv className="p-pl-4 p-pr-4" style={{ transition: '1s' }}>
                <AnnieTitle className="p-mt-3 p-mb-2">Craft Your Feedback</AnnieTitle>
                <FormLabel className="large-text">STEP {pageIndex + 1} OF {this.getPageToFeedbackSlot().length}</FormLabel>
                {ratingControls}
                <FlexNoWrapRow className="p-jc-center p-mt-3">
                    {slidesButton}
                    {pageIndex === (this.getPageToFeedbackSlot().length - 1) ? submitButton : null}
                </FlexNoWrapRow>
            </RatingDiv>
        );
        return (
            <OrientationPattern
                landscape={
                    <HorizontalFullSizeRoomDialogContainer className="p-pl-2 p-pt-2 p-pr-2">
                        {orientationIndependentBlock}
                    </HorizontalFullSizeRoomDialogContainer>
                }
                portrait={
                    <VerticalFullSizeRoomDialogContainer className="p-pl-2 p-pb-2 p-pr-2">
                        {orientationIndependentBlock}
                    </VerticalFullSizeRoomDialogContainer>
                }
            />
        );
    }
}

export default CraftFeedback;
