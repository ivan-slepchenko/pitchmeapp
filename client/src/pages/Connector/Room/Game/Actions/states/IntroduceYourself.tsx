// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import {
    AnnieTitle,
    AnnieTitleNoPadding,
    BlackBorderedDiv,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
} from '../../../../../../Styled';
import { PITCH } from '../../../../../../utils/constants/pitchFlow';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';
import { BottomBar } from '../../../../../../components/UI/Bars';
import { gaStartPitch } from '../../../../../../utils/ga';

@inject(({ roomStore }) => ({ roomStore }))
class IntroduceYourself extends Component {
    render() {
        const {
            roomStore: { setPitchState },
        } = this.props;

        const content = (
            <BlackBorderedDiv>
                <AnnieTitleNoPadding>inroduce yourself and press</AnnieTitleNoPadding>
                <Button
                    className="p-button-rounded p-button-help p-mt-2"
                    label="Start Your Pitch"
                    onClick={() => {
                        gaStartPitch();
                        setPitchState(PITCH);
                    }}
                />
            </BlackBorderedDiv>
        );
        const landscapeLayout = <FullSizeAbsoluteFlexColumnContainer>{content}</FullSizeAbsoluteFlexColumnContainer>;
        const portraitLayout = (
            <FullSizeContainer>
                <BottomBar>{content}</BottomBar>
            </FullSizeContainer>
        );
        return <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />;
    }
}

export default IntroduceYourself;
