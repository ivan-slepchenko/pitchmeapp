// @ts-nocheck
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import {
    AnnieTitle,
    BlackBorderedDiv,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
} from '../../../../../../Styled';
import { BottomBar } from '../../../../../../components/UI/Bars';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

@inject(({ roomStore, userStore, eventsStore }) => ({ roomStore, userStore, eventsStore }))
@observer
class ListenForThePitch extends Component {
    render() {
        const {
            roomStore: {
                hasPitch,
                isPitchDeckVisible,
                room: {
                    companion: { chosenPitch },
                },
            },
            eventsStore: { featureFlags },
        } = this.props;
        const listenThePitchText = 'listen the pitch, and get ready to give your feedback';

        const noPitchDeckContent = (
            <BlackBorderedDiv>
                <AnnieTitle className="p-mb-0 p-mt-0 p-white">{listenThePitchText}</AnnieTitle>
            </BlackBorderedDiv>
        );
        const forPitchDeckContent = <AnnieTitle className="p-mb-3 p-p-1 p-white">{listenThePitchText}</AnnieTitle>;
        const landscapeLayout = (
            <FullSizeAbsoluteFlexColumnContainer>{noPitchDeckContent}</FullSizeAbsoluteFlexColumnContainer>
        );
        const pitchDeckPortraitLayout = (content) => (
            <FullSizeContainer>
                <BottomBar>{content}</BottomBar>
            </FullSizeContainer>
        );
        return (
            <>
                {isPitchDeckVisible ? (
                    <OrientationPattern portrait={pitchDeckPortraitLayout(forPitchDeckContent)} landscape={<></>} />
                ) : (
                    <OrientationPattern
                        landscape={landscapeLayout}
                        portrait={pitchDeckPortraitLayout(noPitchDeckContent)}
                    />
                )}
            </>
        );
    }
}

export default ListenForThePitch;
