// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { BottomBar } from '../../../../../../components/UI/Bars';
import { CRAFTING_FEEDBACK } from '../../../../../../utils/constants/pitchFlow';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

@inject(({ roomStore }) => ({ roomStore }))
@observer
class PerformThePitch extends Component {
    render() {
        const {
            roomStore: {
                setPitchState,
                pitch: { timeToRequestTheFeedback },
            },
        } = this.props;

        const button = (isLandscape) => (
            <>
                {timeToRequestTheFeedback && (
                    <Button
                        style={
                            isLandscape
                                ? {
                                      display: 'none',
                                  }
                                : {}
                        }
                        className="p-button-rounded p-button-help p-button-lg"
                        label="Request The Feedback"
                        onClick={() => {
                            setPitchState(CRAFTING_FEEDBACK);
                        }}
                    />
                )}
                ;
            </>
        );

        const orientationDependentButton = () => <OrientationPattern landscape={<></>} portrait={button(false)} />;

        return <BottomBar>{orientationDependentButton()}</BottomBar>;
    }
}

export default PerformThePitch;
