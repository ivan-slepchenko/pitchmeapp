// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { withRouter } from 'react-router-dom';
import { isMobile } from 'react-device-detect';
import { AnnieTitle, FormBiggerLabel, FormLabel, FullSizeAbsoluteFlexColumnContainer } from '../../../../../../Styled';
import {
    OvertimeLabel,
    RankPositionLabel,
    LandscapeRatingBlock,
    RatingDiv,
    VerticalFullSizeRoomDialogContainer,
    HorizontalFullSizeRoomDialogContainer, RankIsLocked,
} from '../Styled';
import { LEAVE_ROOM } from '../../../../../../utils/constants/events/common';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';
import FeedbackView from '../../../../../Stats/FeedbackView';
import { ROUTES } from '../../../../../../routes';
import { REVIEWING_FEEDBACK } from '../../../../../../utils/constants/pitchFlow';

@withRouter
@inject(({ roomStore, notifyStore, userStore, statsStore }) => ({ roomStore, notifyStore, userStore, statsStore }))
@observer
class ReviewFeedback extends Component {
    state = {
        showButton: false,
        rank: -1,
        place: 0,
        feedbacks: 0,
        isFeedbackFetched: false
    };

    timeout = null;

    componentDidMount() {
        const {
            statsStore: { fetchLeaderboard },
            userStore: {
                user: { id: userId },
            },
        } = this.props;

        this.timeout = setTimeout(() => {
            this.setState({ showButton: true });
        }, 5000);
        fetchLeaderboard().then(() => {
            const {
                statsStore: {
                    eventLeaderboardData
                }
            } = this.props;
            for (let i = 0; i < eventLeaderboardData.length; i++) {
                const entry = eventLeaderboardData[i];
                if (entry.userId === userId) {
                    this.setState({ rank: entry.rank, place: i + 1, feedbacks: entry.feedbacks, isFeedbackFetched: true });
                }
            }
        });
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    countActualFeedbacks(feedback) {
        let amount = 0;
        if(feedback.a){amount++};
        if(feedback.b){amount++};
        if(feedback.c){amount++};
        if(feedback.d){amount++};
        if(feedback.e){amount++};
        if(feedback.f){amount++};
        return amount;
    }

    render() {
        const {
            roomStore: {
                leaveRoom,
                switchRoles,
                feedback,
                pitchOvertime,
                replayIsNeeded
            }
        } = this.props;

        const { showButton, rank, place, feedbacks, isFeedbackFetched } = this.state;

        const overtimeBlock = (
            <div className="p-d-flex p-mt-0 p-mb-1">
                <FormBiggerLabel>overtime:</FormBiggerLabel>
                <OvertimeLabel className="p-pl-2">{`${pitchOvertime} seconds`}</OvertimeLabel>
            </div>
        );

        const rankBlock = (
            <div className="p-d-flex p-mt-0 p-mb-1">
                <FormBiggerLabel className="p-pr-2">score:</FormBiggerLabel>
                <RankPositionLabel className="p-pr-3">{rank}</RankPositionLabel>
                <FormBiggerLabel className="p-pr-2">rank:</FormBiggerLabel>
                <RankPositionLabel>#{place}</RankPositionLabel>
            </div>
        );

        const checkYourRankBlock = (
            <RankIsLocked className="p-ai-center p-d-flex">you can see leaderboard in the new tab 👉
                <Button
                    className="p-button-rounded p-button-info p-button-xsm p-ml-2"
                    label="Leaderboard"
                    onClick={() => {
                        const win = window.open(ROUTES.LEADERBOARD_STANDALONE.path, "_blank");
                        win.focus();
                    }}
                />
            </RankIsLocked>
        );

        const rankIsLocked = (
            <div className="p-d-flex p-mt-0 p-mb-1">
                <RankIsLocked className="p-pr-2">pitch {3-feedbacks} times more to unlock your score and rank</RankIsLocked>
            </div>
        );

        const computingRankBlock = (
            <div className="p-d-flex p-mt-0 p-mb-1">
                <FormBiggerLabel className="p-pr-2">...updating the score...</FormBiggerLabel>
            </div>
        );

        const leaveButton = (
            <Button
                className="p-button-rounded p-button-help p-mt-2 p-button-success"
                label="Exit Room"
                onClick={() => {
                    leaveRoom(LEAVE_ROOM);
                }}
            />
        );

        const switchRolesButton = (
            <Button
                className="p-button-rounded p-button-help p-mt-2 p-button-success"
                label="Switch Roles"
                onClick={switchRoles}
            />
        );

        const portraitLayout = (
            <VerticalFullSizeRoomDialogContainer className="p-pl-2 p-pb-2 p-pr-2">
                <RatingDiv className="p-pl-4 p-pr-4">
                    <AnnieTitle className="p-mt-2 p-mb-0">The Feedback</AnnieTitle>
                    {pitchOvertime > 0 && overtimeBlock}
                    {(feedbacks > 0 && feedbacks < 3) && rankIsLocked}
                    {feedbacks >= 3 && rankBlock}
                    {feedbacks >= 3 && checkYourRankBlock}
                    {!isFeedbackFetched && computingRankBlock}
                    <div
                        className="p-d-flex p-dir-col p-p-2 p-ai-center"
                        style={{
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                            width: '100%',
                            borderRadius: '10px',
                            maxHeight: '500px',
                            scrollBehavior: 'auto',
                            overflowY: 'auto',
                            overflowX: 'hidden',
                        }}
                    >
                        <FeedbackView feedback={feedback} />
                        {feedback.text.length > 0 && (
                            <>
                                <hr className="p-mt-2 p-mb-2" style={{ width: '100%' }} />
                                <FormLabel className="p-mb-2 large-text">{feedback.text}</FormLabel>
                            </>
                        )}
                    </div>
                    {showButton && !replayIsNeeded && leaveButton}
                    {showButton && replayIsNeeded && switchRolesButton}
                    {replayIsNeeded && <span className="p-mt-2">* companion wants to pitch back</span>}
                </RatingDiv>
            </VerticalFullSizeRoomDialogContainer>
        );

        const landscapeLayout = (
            <HorizontalFullSizeRoomDialogContainer className="p-pl-2 p-pt-2 p-pr-2">
                <RatingDiv className="p-pl-4 p-pr-4">
                    <AnnieTitle className="p-mt-0 p-mb-0">The Feedback:</AnnieTitle>
                    <div className="p-d-flex p-dir-row">
                        {rank > 0 && pitchOvertime > 0 && overtimeBlock}
                        <div className="p-ml-3" />
                        {( feedbacks > 0 && feedbacks < 3) && rankIsLocked}
                        {feedbacks >= 3 && rankBlock}
                        {feedbacks === 0 && computingRankBlock}
                    </div>
                    {feedbacks >= 3 && checkYourRankBlock}
                    <div
                        className="p-d-flex p-dir-col p-p-2 p-ai-center"
                        style={{
                            backgroundColor: 'rgba(0, 0, 0, 0.3)',
                            width: '100%',
                            borderRadius: '10px',
                            maxHeight: isMobile ? '210px' : '500px',
                            scrollBehavior: 'auto',
                            overflowY: 'auto',
                            overflowX: 'hidden',
                        }}
                    >
                        <LandscapeRatingBlock displayInOneLine={this.countActualFeedbacks(feedback) <= 3}>
                            <FeedbackView feedback={feedback} shadowWholeBlock />
                        </LandscapeRatingBlock>
                        <hr className="p-mt-2 p-mb-2" style={{ width: '100%' }} />
                        <FormLabel className="large-text">{feedback.text}</FormLabel>
                    </div>
                    {showButton && !replayIsNeeded && leaveButton}
                    {showButton && replayIsNeeded && switchRolesButton}
                    {replayIsNeeded && <span className="p-mt-1">* companion wants to pitch back</span>}
                </RatingDiv>
            </HorizontalFullSizeRoomDialogContainer>
        );

        return <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />;
    }
}

export default ReviewFeedback;
