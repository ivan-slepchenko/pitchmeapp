import { Component } from 'react';
import {
    AnnieTitle,
    BlackBorderedDiv,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
} from '../../../../../../Styled';
import { BottomBar } from '../../../../../../components/UI/Bars';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

class WaitForFeedback extends Component {
    render() {
        const content = (
            <BlackBorderedDiv>
                <AnnieTitle className="p-mb-0 p-mt-0">Wait for feedback</AnnieTitle>
            </BlackBorderedDiv>
        );
        const landscapeLayout = <FullSizeAbsoluteFlexColumnContainer>{content}</FullSizeAbsoluteFlexColumnContainer>;
        const portraitLayout = (
            <FullSizeContainer>
                <BottomBar>{content}</BottomBar>
            </FullSizeContainer>
        );
        return <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />;
    }
}

export default WaitForFeedback;
