/* eslint-disable react/jsx-closing-tag-location */
// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Button } from 'primereact/button';
import { withRouter } from 'react-router-dom';
import {
    AnnieTitle,
    BlackBorderedDiv,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
    Title,
} from '../../../../../../Styled';
import { BottomBar } from '../../../../../../components/UI/Bars';
import { LEAVE_ROOM } from '../../../../../../utils/constants/events/common';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

@withRouter
@inject(({ roomStore, userStore }) => ({ roomStore, userStore }))
@observer
class WaitForFeedbackToBeReviewed extends Component {
    state = { showButton: false };

    timeout = null;

    componentDidMount() {
        this.timeout = setTimeout(() => {
            this.setState({ showButton: true });
        }, 5000);
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    render() {
        const {
            roomStore: {
                leaveRoom,
                pitch: { isInReplayMode },
                room: {
                    companion: { contactEmail, name },
                },
            },
            userStore: {
                user: { chosenPitchIndex },
            },
        } = this.props;
        const { showButton } = this.state;
        const content = (
            <BlackBorderedDiv>
                <AnnieTitle>Wrap up the conversation</AnnieTitle>
                {chosenPitchIndex !== -1 && !isInReplayMode && <AnnieTitle>And get ready to pitch</AnnieTitle>}
                {showButton && (chosenPitchIndex === -1 || isInReplayMode) && (
                    <Button
                        className="p-button-rounded p-button-help"
                        label="Exit Room"
                        onClick={() => {
                            leaveRoom(LEAVE_ROOM);
                        }}
                    />
                )}
                {contactEmail != null && contactEmail.length > 0 && (
                    <>
                        <div style={{textAlign: "center"}}>{name} shared the contact email</div>
                        <a
                            className="p-mb-2"
                            href={`mailto:${contactEmail}`}
                            rel="noreferrer"
                            target="_blank"
                            style={{ color: '#00a4f9' }}
                        >
                            {contactEmail}
                        </a>
                    </>
                )}
            </BlackBorderedDiv>
        );
        const landscapeLayout = <FullSizeAbsoluteFlexColumnContainer>{content}</FullSizeAbsoluteFlexColumnContainer>;
        const portraitLayout = (
            <FullSizeContainer>
                <BottomBar>{content}</BottomBar>
            </FullSizeContainer>
        );
        return <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />;
    }
}

export default WaitForFeedbackToBeReviewed;
