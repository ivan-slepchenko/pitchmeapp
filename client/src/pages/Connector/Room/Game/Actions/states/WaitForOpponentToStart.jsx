import { Component } from 'react';
import {
    AnnieTitleNoPadding,
    BlackBorderedDiv,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
} from '../../../../../../Styled';
import { BottomBar } from '../../../../../../components/UI/Bars';
import OrientationPattern from '../../../../../../components/Layouts/OrientationPattern';

class WaitForOpponentToStart extends Component {
    render() {
        const content = (
            <BlackBorderedDiv>
                <AnnieTitleNoPadding>wait for the opponent to start</AnnieTitleNoPadding>
            </BlackBorderedDiv>
        );
        const landscapeLayout = <FullSizeAbsoluteFlexColumnContainer>{content}</FullSizeAbsoluteFlexColumnContainer>;
        const portraitLayout = (
            <FullSizeContainer>
                <BottomBar>{content}</BottomBar>
            </FullSizeContainer>
        );
        return <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />;
    }
}

export default WaitForOpponentToStart;
