import styled from '@emotion/styled';
import { FullSizeContainer } from '../../../../../../../Styled';

function calculateCircumference(radius) {
    return 2 * Math.PI * radius;
}

const SIZE = 82;
const radius = SIZE / 2;
const circleSize = calculateCircumference(radius);

export const TimerContainer = styled.div`
    width: 100%;
    height: 100%;
    z-index: 1;
    text-align: center;
    font-size: ${(props) => (props.allowedPitchingTimeLeftInSeconds > 0 ? '7vh' : '18px')};
    font-family: 'Saira Condensed';
    color: ${(props) => (props.allowedPitchingTimeLeftInSeconds < 11 ? '#f50057' : '#fff')};
`;

export const TimerBackground = styled(FullSizeContainer)`
    background: ${(props) => (props.allowedPitchingTimeLeftInSeconds < 11 ? '#000' : 'rgb(70, 48, 70)')};
    border-radius: 100%;
    position: absolute;
`;

export const TimerOutline = styled.div`
    transition: 0.3s;
    svg {
        position: absolute;
        top: 0;
        right: 0;
        transform: rotateY(-180deg) rotateZ(-90deg);
        overflow: visible;

        circle {
            stroke-dasharray: ${circleSize}px;
            stroke-dashoffset: 0px;
            stroke-linecap: round;
            stroke-width: 3px;
            stroke: ${(props) => (props.allowedPitchingTimeLeftInSeconds < 11 ? '#f50057' : '#fff')};
            fill: none;
            animation: countdown ${(props) => props.pitchMaximumTimeInSeconds}s linear forwards;
        }
    }

    @keyframes countdown {
        from {
            stroke-dashoffset: 0px;
        }
        to {
            stroke-dashoffset: ${circleSize}px;
        }
    }
`;
