// @ts-nocheck
import { useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';
import { FullSizeAbsoluteFlexColumnContainer } from '../../../../../../../Styled';
import { TimerBackground, TimerContainer, TimerOutline } from './Styled';
import { PITCH } from '../../../../../../../utils/constants/pitchFlow';

const Timer = inject('roomStore', 'eventsStore')(
    observer(
        ({
             roomStore: {
                 activePitch,
                 pitch: { state: pitchState },
                 timing: { startPitch:pitchStatTime, endPitch:feedbackStartTime },
             },
             eventsStore: { featureFlags:{ FEEDBACK_TIMER } },
             onTimeOver
        }) => {
            const [timerId, setTimerId] = useState(null);
            const [pitchMaximumTimeInSeconds, setPitchMaximumTimeInSeconds] = useState(0);
            const [allowedPitchingTimeLeftInSeconds, setAllowedPitchingTimeLeftInSeconds] = useState(0);

            const stopTimer = () => {
                clearInterval(timerId);
                if (onTimeOver) {
                    onTimeOver();
                }
            };

            const timerTick = () => {
                const alreadyPitchingTimeInSeconds = (Date.now() - (pitchState === PITCH ? pitchStatTime : feedbackStartTime)) / 1000;
                setAllowedPitchingTimeLeftInSeconds(Math.round(pitchMaximumTimeInSeconds - alreadyPitchingTimeInSeconds));

                if (allowedPitchingTimeLeftInSeconds === 0) {
                    stopTimer();
                }
            };

            useEffect(() => {
                setPitchMaximumTimeInSeconds((pitchState === PITCH ? activePitch.time : FEEDBACK_TIMER.values.time) / 1000);
                timerTick();
            }, [
                pitchState,
                activePitch,
                FEEDBACK_TIMER,
            ]);

            useEffect(() => {
                if (timerId != null) {
                    clearInterval(timerId);
                }
                const newTimerId = setInterval(timerTick, 1);
                setTimerId(newTimerId);
                setAllowedPitchingTimeLeftInSeconds(pitchMaximumTimeInSeconds)
                return () => {
                    clearInterval(newTimerId)
                }
            }, [pitchMaximumTimeInSeconds]);

            if (allowedPitchingTimeLeftInSeconds === pitchMaximumTimeInSeconds) {
                return null;
            }

            return (
                <TimerContainer
                    allowedPitchingTimeLeftInSeconds={allowedPitchingTimeLeftInSeconds}
                    pitchMaximumTimeInSeconds={pitchMaximumTimeInSeconds}
                >
                    <TimerBackground
                        allowedPitchingTimeLeftInSeconds={allowedPitchingTimeLeftInSeconds}
                        pitchMaximumTimeInSeconds={pitchMaximumTimeInSeconds}
                    />
                    <TimerOutline
                        allowedPitchingTimeLeftInSeconds={allowedPitchingTimeLeftInSeconds}
                        pitchMaximumTimeInSeconds={pitchMaximumTimeInSeconds}
                    >
                        <svg viewBox="0 0 82 82" preserveAspectRatio="none">
                            <circle r="41" cx="41" cy="41"/>
                        </svg>
                    </TimerOutline>
                    <FullSizeAbsoluteFlexColumnContainer>
                        {allowedPitchingTimeLeftInSeconds > 0 ? allowedPitchingTimeLeftInSeconds : 'TIME IS OVER'}
                    </FullSizeAbsoluteFlexColumnContainer>
                </TimerContainer>
            );
        },
    ),
);

export default Timer;
