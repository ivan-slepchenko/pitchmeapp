// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';

import { AudioIcon, MiniMirrorToggleStyledDiv, NoAudioIcon } from '../../../Mirror/Styled';

@inject(({ notifyStore, userStore }) => ({ notifyStore, userStore }))
@observer
class AudioToggleButton extends Component {
    render() {
        const {
            top,
            bottom,
            playAudio,
            media,
            userStore: { setUserMicAccess, userMicAccess },
        } = this.props;
        return (
            <MiniMirrorToggleStyledDiv
                top={top}
                bottom={bottom}
                onClick={() => {
                    playAudio(!userMicAccess, media);
                    setUserMicAccess(!userMicAccess, media);
                }}
            >
                {userMicAccess ? <AudioIcon /> : <NoAudioIcon />}
            </MiniMirrorToggleStyledDiv>
        );
    }
}

export default AudioToggleButton;
