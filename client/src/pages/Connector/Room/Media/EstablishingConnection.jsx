import Loader from '../../../../components/UI/Loader';
import { FlexNoWrapRow, WhiteText } from '../../../../Styled';

const EstablishingConnection = ({ status }) => (
    <FlexNoWrapRow
        style={{
            position: 'absolute',
            zIndex: 100,
            top: '50%',
            justifyContent: 'center',
        }}
    >
        <Loader width="23px" height="23px" inner={<></>} />
        <WhiteText className="p-ml-2">{status}</WhiteText>
    </FlexNoWrapRow>
);

export default EstablishingConnection;
