import { FullSizeContainer } from '../../../../Styled';
import MediaPlayer from './MediaPlayer';

const FullSizeMediaPlayer = ({ media }) => (
    <FullSizeContainer style={{ background: '#000' }}>
        <MediaPlayer media={media} />
    </FullSizeContainer>
);

export default FullSizeMediaPlayer;
