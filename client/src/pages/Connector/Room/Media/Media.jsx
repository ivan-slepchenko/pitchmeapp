import { useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';
import EstablishingConnection from './EstablishingConnection';
import OrientationPattern from '../../../../components/Layouts/OrientationPattern';
import LandscapeMedia from './landscape/LandscapeMedia';
import PortraitMedia from './portrait/PortraitMedia';
import {
    disableAudio,
    disableVideo,
    enableAudio,
    enableVideo,
    generateTokenAndConnectToRoom, trackPublicationsToTracks,
} from './controllers/helpers';
import { FullSizeMediaContainer } from './Styled';

const Media = inject(
    'roomStore',
    'userStore'
)(
    observer(
        ({
            roomStore: {
                setVideoConnectionIsEstablished,
                isInOpponentPreviewMode,
                room: { id: roomId },
            },
            userStore: {
                userCamAccess,
                userMicAccess,
                user: { email },
            },
        }) => {
            const [participants, setParticipants] = useState([]);
            const [localParticipant, setLocalParticipantValue] = useState(null);
            const [isVideoPlaying, setVideoIsPlaying] = useState(false);
            const [isAudioPlaying, setAudioIsPlaying] = useState(false);

            const participantConnected = (participant) => {

                const {tracks} = participant;
                participant.tracks.forEach((publication)=>{
                    if (publication.isSubscribed) {
                        publication.on('disabled', () => {
                            console.log('ON TRACK DISABLED');
                        });
                    }
                    publication.on('subscribed', (trak)=>{
                        publication.on('disabled', () => {
                            console.log('ON TRACK DISABLED');
                        });
                    });

                })
                setVideoConnectionIsEstablished(true);
                setParticipants((prevParticipants) => [...prevParticipants, participant]);
            };

            const participantDisconnected = (participant) => {
                // setVideoConnectionIsEstablished(false); // we have only one participant
                // keep everything up and running, as we hope for participant to reconnect
                console.log('PARTICIPANT DISCONNECTED');
                setParticipants((prevParticipants) => prevParticipants.filter((p) => p !== participant));
            };

            const playVideo = (value, media) => {
                console.log(`PLAY VIDEO value: ${  value  } media: ${  media  }`);
                if (value) {
                    enableVideo({ media });
                } else {
                    disableVideo({ media });
                }
                setVideoIsPlaying(value);
            };

            const playAudio = (value, media) => {
                if (value) {
                    enableAudio({ media });
                } else {
                    disableAudio({ media });
                }
                setAudioIsPlaying(value);
            };

            const setLocalParticipant = (participant) => {
                console.log(`SET LOCAL PARTICIPANT: ${localParticipant}`)
                setLocalParticipantValue(participant);
                if (!userMicAccess && !isAudioPlaying) {
                    playAudio(false, participant);
                }
                if (!userCamAccess && !isVideoPlaying) {
                    playVideo(false, participant);
                }
            };

            useEffect(() => {
                generateTokenAndConnectToRoom({
                    email,
                    roomId,
                    participantConnected,
                    participantDisconnected,
                    setLocalParticipant,
                });
            }, [email, roomId]);

            if (isInOpponentPreviewMode) {
                return <></>;
            }

            return (
                <FullSizeMediaContainer>
                    {participants.length > 0 ? (
                        <OrientationPattern
                            landscape={
                                <LandscapeMedia
                                    playVideo={playVideo}
                                    playAudio={playAudio}
                                    localMedia={localParticipant}
                                    companionMedia={participants[0]}
                                />
                            }
                            portrait={
                                <PortraitMedia
                                    playVideo={playVideo}
                                    playAudio={playAudio}
                                    localMedia={localParticipant}
                                    companionMedia={participants[0]}
                                />
                            }
                        />
                    ) : (
                        <EstablishingConnection />
                    )}
                </FullSizeMediaContainer>
            );
        }
    )
);

export default Media;
