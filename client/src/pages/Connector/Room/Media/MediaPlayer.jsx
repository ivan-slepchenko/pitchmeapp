import { useState, useEffect, useRef } from 'react';
import { trackPublicationsToTracks } from './controllers/helpers';

const MediaPlayer = ({ media }) => {
    const [videoTracks, setVideoTracks] = useState([]);
    const [audioTracks, setAudioTracks] = useState([]);

    const videoRef = useRef();
    const audioRef = useRef();

    useEffect(() => {
        const trackSubscribed = (track) => {
            if (track.kind === 'video') {
                setVideoTracks((_videoTracks) => [..._videoTracks, track]);
            } else {
                setAudioTracks((_audioTracks) => [..._audioTracks, track]);
            }
        };

        const trackUnsubscribed = (track) => {
            if (track.kind === 'video') {
                setVideoTracks((_videoTracks) => _videoTracks.filter((v) => v !== track));
            } else {
                setAudioTracks((_audioTracks) => _audioTracks.filter((a) => a !== track));
            }
        };

        setVideoTracks(trackPublicationsToTracks(media.videoTracks));
        setAudioTracks(trackPublicationsToTracks(media.audioTracks));

        media.on('trackSubscribed', trackSubscribed);
        media.on('trackUnsubscribed', trackUnsubscribed);

        return () => {
            setVideoTracks([]);
            setAudioTracks([]);
            media.removeListener('trackSubscribed', trackSubscribed);
            media.removeListener('trackUnsubscribed', trackUnsubscribed);
        };
    }, [media]);

    useEffect(() => {
        const videoTrack = videoTracks[0];
        if (videoTrack) {
            videoTrack.attach(videoRef.current);
            return () => {
                videoTrack.detach();
            };
        }
    }, [videoTracks]);

    useEffect(() => {
        const audioTrack = audioTracks[0];
        if (audioTrack) {
            audioTrack.attach(videoRef.current);
            return () => {
                audioTrack.detach();
            };
        }
    }, [audioTracks]);

    return (
        <div
            style={{
                width: '100%',
                height: '100%',
            }}
        >
            <video ref={videoRef} autoPlay />
            <audio ref={audioRef} autoPlay muted />
        </div>
    );
};

export default MediaPlayer;
