import { observer } from 'mobx-react';
import VideoToggleButton from './VideoToggleButton';
import AudioToggleButton from './AudioToggleButton';
import MediaPlayer from './MediaPlayer';
import { MiniVideoContainerBackground } from './Styled';

const MiniMediaPlayer = observer(({ media, controls, playVideo, playAudio }) => (
    <>
        <MiniVideoContainerBackground>
             <MediaPlayer media={media} />
        </MiniVideoContainerBackground>
        {controls && (
            <>
                <VideoToggleButton top media={media} playVideo={playVideo} />
                <AudioToggleButton bottom media={media} playAudio={playAudio} />
            </>
        )}
    </>
));

export default MiniMediaPlayer;
