import styled from '@emotion/styled';

export const FullSizeMediaContainer = styled.div`
    video {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        z-index: 0;
        object-fit: cover;
        object-position: 50% 50%;
    }
`;

// TODO: check it in components/Layouts/BluredWrapper
export const ColorTintWrapper = styled.div`
    width: 100%;
    height: 100%;
    background: rgba(18, 0, 171, 0.45);
    position: absolute;
`;

export const MiniVideoContainerBackground = styled.div`
    z-index: 1;
    top: 0px;
    position: absolute;
    background: #444;
    border-radius: 100%;
    overflow: hidden;
    width: 100%;
    height: 100%;
    border-width: 3px;
    border-color: white;
    border-style: solid;
    video {
        position: relative;
        object-fit: cover;
    }
`;
export const LandscapeMiniVideoContainer = styled.div`
    position: relative;
    height: 25vh;
    width: 25vh;
    margin: 1vh;
    video {
        position: relative;
        height: 25vh;
        width: 25vh;
        object-fit: cover;
    }
`;

export const LandscapeMediaContainer = styled.div`
    padding-left: 2vh;
    padding-top: 2vh;
    display: flex;
    flex-direction: column;
    align-elements: flex-start;
    left: 0px;
    top: 0px;
    width: 100px;
`;

export const PortraitTimerContainer = styled.div`
    position: absolute;
    top: 110px;
    left: 2vh;
    height: 14vh;
    width: 14vh;
`;

export const LandscapeTimerContainer = styled.div`
    position: relative;
    min-height: 80px;
    min-width: 80px;
    height: 12vh;
    width: 12vh;
    margin: 1vh;
`;

export const PortraitRightMiniVideoContainer = styled(LandscapeMiniVideoContainer)`
    position: absolute;
    top: 110px;
    right: 2vh;
    height: 14vh;
    width: 14vh;
`;

export const PortraitLeftMiniVideoContainer = styled(LandscapeMiniVideoContainer)`
    position: absolute;
    top: 110px;
    left: 2vh;
    height: 14vh;
    width: 14vh;
`;

export const PortraitCenteredMiniVideoContainer = styled(LandscapeMiniVideoContainer)`
    position: absolute;
    right: 50%;
    top: 110px;
    height: 14vh;
    width: 14vh;
    transform: translateX(50%);
`;
