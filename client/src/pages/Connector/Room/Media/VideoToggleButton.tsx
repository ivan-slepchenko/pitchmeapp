// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { MiniMirrorToggleStyledDiv, NoVideoIcon, VideoIcon } from '../../../Mirror/Styled';

@inject(({ notifyStore, userStore }) => ({ notifyStore, userStore }))
@observer
class VideoToggleButton extends Component {
    render() {
        const {
            top,
            bottom,
            playVideo,
            media,
            userStore: { setUserCamAccess, userCamAccess },
        } = this.props;
        return (
            <MiniMirrorToggleStyledDiv
                top={top}
                bottom={bottom}
                onClick={() => {
                    playVideo(!userCamAccess, media);
                    setUserCamAccess(!userCamAccess, media);
                }}
            >
                {userCamAccess ? <VideoIcon /> : <NoVideoIcon />}
            </MiniMirrorToggleStyledDiv>
        );
    }
}

export default VideoToggleButton;
