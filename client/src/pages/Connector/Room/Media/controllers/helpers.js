import twilio from 'twilio';
import { connect as twilioConnect } from 'twilio-video';
import { TWILIO_ACCOUNT_SID, TWILIO_API_KEY, TWILIO_API_SECRET } from '../../../../../config';
import { CRAFTING_FEEDBACK, LAST_WORD, PITCH, REVIEWING_FEEDBACK } from '../../../../../utils/constants/pitchFlow';
import { IS_DEV } from '../../../../../utils/constants';

export const generateTokenAndConnectToRoom = async ({
    email,
    roomId,
    participantConnected,
    participantDisconnected,
    setLocalParticipant,
}) => {
    console.log('CONNECT TWILLIO');
    const {AccessToken} = twilio.jwt;
    const {VideoGrant} = AccessToken;
    const token = new AccessToken(TWILIO_ACCOUNT_SID, TWILIO_API_KEY, TWILIO_API_SECRET);
    token.identity = email;
    const videoGrant = new VideoGrant();
    token.addGrant(videoGrant);

    return twilioConnect(token.toJwt(), {
        name: roomId,
        audio: !IS_DEV,
        video: { width: 1024, facingMode: 'user' },
    }).then(
        async (room) => {
            console.log(`Successfully joined a Room: ${room}`);
            setLocalParticipant(room.localParticipant);
            room.on('trackDisabled', ()=>{
                console.log('TRACK DISABLED');
            });
            room.on('trackEnabled', ()=>{
                console.log('TRACK ENABLED');
            });
            room.on('participantConnected', participantConnected);
            room.on('participantDisconnected', participantDisconnected);
            room.participants.forEach(participantConnected);
            room.on('disconnected', () => {
                // Detach the local media elements
                room.localParticipant.tracks.forEach((publication) => {
                    const attachedElements = publication.track.detach();
                    attachedElements.forEach((element) => element.remove());
                });
            });
        },
        (error) => {
            console.error(`Unable to connect to Room: ${error.message}`);
        }
    );
};

export const shouldShowMiniMirror = ({ state }) =>
    [CRAFTING_FEEDBACK, REVIEWING_FEEDBACK, LAST_WORD].indexOf(state) === -1;

export const shouldShowTimer = ({ state }) => [PITCH, CRAFTING_FEEDBACK, REVIEWING_FEEDBACK].indexOf(state) !== -1 ;

export const trackPublicationsToTracks = (trackMap) =>
    Array.from(trackMap.values())
        .map((publication) => publication.track)
        .filter((track) => track !== null);

export const disableVideo = ({ media }) => {
    media.videoTracks.forEach(
        publication => publication.track.disable()
    );
};

export const enableVideo = ({ media }) => {
    media.videoTracks.forEach(
        publication => publication.track.enable()
    );
};

export const disableAudio = ({ media }) => {
    media.audioTracks.forEach(
        publication => publication.track.disable()
    );
};

export const enableAudio = ({ media }) => {
    media.audioTracks.forEach(
        publication => publication.track.enable()
    );
};
