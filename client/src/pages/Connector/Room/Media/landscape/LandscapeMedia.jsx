import { inject, observer } from 'mobx-react';
import { LandscapeMediaContainer } from '../Styled';
import { shouldShowMiniMirror, shouldShowTimer } from '../controllers/helpers';
import FullSizeMediaPlayer from '../FullSizeMediaPlayer';
import LandscapeTimer from './LandscapeTimer';
import LandscapeMiniMediaPlayer from './LandscapeMiniMediaPlayer';

const LandscapeMedia = inject(
    'roomStore',
    'userStore'
)(
    observer(
        ({
            localMedia,
            companionMedia,
            playVideo,
            playAudio,
            roomStore: {
                isPitchDeckVisible,
                pitch: { state },
            },
        }) => (
            <LandscapeMediaContainer>
                {shouldShowMiniMirror({ state }) && (
                    <LandscapeMiniMediaPlayer media={localMedia} playVideo={playVideo} playAudio={playAudio} controls />
                )}
                {isPitchDeckVisible ? (
                    <LandscapeMiniMediaPlayer media={companionMedia} />
                ) : (
                    <FullSizeMediaPlayer media={companionMedia} />
                )}
                {shouldShowTimer({ state }) && <LandscapeTimer />}
            </LandscapeMediaContainer>
        )
    )
);

export default LandscapeMedia;
