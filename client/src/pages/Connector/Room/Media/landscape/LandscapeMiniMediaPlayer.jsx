import { LandscapeMiniVideoContainer } from '../Styled';
import MiniMediaPlayer from '../MiniMediaPlayer';

const LandscapeMiniMediaPlayer = (props) => (
    <LandscapeMiniVideoContainer>
        <MiniMediaPlayer {...props} />
    </LandscapeMiniVideoContainer>
);

export default LandscapeMiniMediaPlayer;
