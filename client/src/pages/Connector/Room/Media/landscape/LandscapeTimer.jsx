import Timer from '../../Game/Actions/states/shared/Timer';
import { LandscapeTimerContainer } from '../Styled';

const LandscapeTimer = () => (
    <LandscapeTimerContainer className="p-mt-2">
        <Timer />
    </LandscapeTimerContainer>
);

export default LandscapeTimer;
