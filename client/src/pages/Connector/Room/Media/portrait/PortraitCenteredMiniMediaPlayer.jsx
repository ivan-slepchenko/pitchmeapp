import { PortraitCenteredMiniVideoContainer } from '../Styled';
import MiniMediaPlayer from '../MiniMediaPlayer';

const PortraitCenteredMiniMediaPlayer = (props) => (
    <PortraitCenteredMiniVideoContainer>
        <MiniMediaPlayer {...props} />
    </PortraitCenteredMiniVideoContainer>
);

export default PortraitCenteredMiniMediaPlayer;
