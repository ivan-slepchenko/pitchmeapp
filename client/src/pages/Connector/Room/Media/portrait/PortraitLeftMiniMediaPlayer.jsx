import { PortraitLeftMiniVideoContainer } from '../Styled';
import MiniMediaPlayer from '../MiniMediaPlayer';

const PortraitLeftMiniMediaPlayer = (props) => (
    <PortraitLeftMiniVideoContainer>
        <MiniMediaPlayer {...props} />
    </PortraitLeftMiniVideoContainer>
);

export default PortraitLeftMiniMediaPlayer;
