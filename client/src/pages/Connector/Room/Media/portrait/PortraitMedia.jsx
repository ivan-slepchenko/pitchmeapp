import { inject, observer } from 'mobx-react';
import { shouldShowMiniMirror, shouldShowTimer } from '../controllers/helpers';
import PortraitTimer from './PortraitTimer';
import FullSizeMediaPlayer from '../FullSizeMediaPlayer';
import PortraitLeftMiniMediaPlayer from './PortraitLeftMiniMediaPlayer';
import PortraitCenteredMiniMediaPlayer from './PortraitCenteredMiniMediaPlayer';
import PortraitRightMiniMediaPlayer from './PortraitRightMiniMediaPlayer';

const PortraitMedia = inject(
    'roomStore',
    'userStore'
)(
    observer(
        ({
            localMedia,
            companionMedia,
            playVideo,
            playAudio,
            roomStore: {
                isPitchDeckVisible,
                pitch: { state, isFeedbackOnDeck },
            },
        }) => (
            <>
                {shouldShowTimer({ state }) && <PortraitTimer />}
                {isPitchDeckVisible && isFeedbackOnDeck && <PortraitLeftMiniMediaPlayer media={companionMedia} />}
                {isPitchDeckVisible && !isFeedbackOnDeck && <PortraitCenteredMiniMediaPlayer media={companionMedia} />}
                {!isPitchDeckVisible && <FullSizeMediaPlayer media={companionMedia} />}
                {shouldShowMiniMirror({ state }) && (
                    <PortraitRightMiniMediaPlayer
                        media={localMedia}
                        playVideo={playVideo}
                        playAudio={playAudio}
                        controls
                    />
                )}
            </>
        )
    )
);

export default PortraitMedia;
