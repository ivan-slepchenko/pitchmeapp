import { observer } from 'mobx-react';
import { PortraitRightMiniVideoContainer } from '../Styled';
import MiniMediaPlayer from '../MiniMediaPlayer';

const PortraitRightMiniMediaPlayer = observer(({ media, controls, playVideo, playAudio }) => (
    <PortraitRightMiniVideoContainer>
        <MiniMediaPlayer media={media} controls={controls} playVideo={playVideo} playAudio={playAudio} />
    </PortraitRightMiniVideoContainer>
));

export default PortraitRightMiniMediaPlayer;
