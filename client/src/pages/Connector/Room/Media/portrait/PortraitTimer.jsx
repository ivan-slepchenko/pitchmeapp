import Timer from '../../Game/Actions/states/shared/Timer';
import { PortraitTimerContainer } from '../Styled';

const PortraitTimer = () => (
    <PortraitTimerContainer style={{ zIndex: 1 }}>
        <Timer />
    </PortraitTimerContainer>
);

export default PortraitTimer;
