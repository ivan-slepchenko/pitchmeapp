import { inject, observer } from 'mobx-react';

import Wrapper from './Styled';

function RoomNotifier({
    roomStore: {
        notifier: { value },
    },
}) {
    if (!value) {
        return null;
    }

    return <Wrapper>{value}</Wrapper>;
}

export default inject('roomStore')(observer(RoomNotifier));
