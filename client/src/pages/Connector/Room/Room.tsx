// @ts-nocheck
import { Component } from 'react';
import { connect } from 'react-redux';
import { inject, observer } from 'mobx-react';
// import * as SWRTC from '@andyet/simplewebrtc';

import Presentation from 'components/Presentation';

import styled from '@emotion/styled';
import Media from './Media';
import RoomControlBar from './RoomControlBar';
import PitchSession from './Game/Actions';
import RoomNotifier from './Notifier/Notifier';
import { FullSizeAbsolute } from '../../../Styled';
import OpponentPreview from '../OpponentPreview/OpponentPreview';

const BlackFullSizeContainer = styled(FullSizeAbsolute)`
    background: black;
`;
@connect()
@inject(({ roomStore, userStore }) => ({ roomStore, userStore }))
@observer
class Room extends Component {
    componentWillUnmount() {
        const {
            userStore: { readUserMicAccessFromStorage, readUserCamAccessFromStorage },
        } = this.props;
        // this.leaveWebrtcRoom();
        readUserMicAccessFromStorage();
        readUserCamAccessFromStorage();
    }

    // // TODO:? maybe delete this
    // leaveWebrtcRoom() {
    //     const { dispatch } = this.props;
    //
    //     try {
    //         dispatch(SWRTC.Actions.disconnect());
    //     } catch (error) {
    //         console.error(`Leave webrtc room failed with error: ${error}`);
    //     }
    // }

    render() {
        const {
            roomStore: { isInOpponentPreviewMode },
        } = this.props;
        return (
            <BlackFullSizeContainer>
                {!isInOpponentPreviewMode && <Presentation />}

                <Media />

                {!isInOpponentPreviewMode && <PitchSession />}

                {isInOpponentPreviewMode && <OpponentPreview />}

                <RoomNotifier />

                {!isInOpponentPreviewMode && <RoomControlBar />}
            </BlackFullSizeContainer>
        );
    }
}

export default Room;
