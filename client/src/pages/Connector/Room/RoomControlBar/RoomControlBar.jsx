import React from 'react';
import { inject, observer } from 'mobx-react';

import { gaQuitFullScreen, gaRoomControlBarLeaveRoom, gaSwitchToFullScreen } from 'utils/ga';
import { Button } from 'primereact/button';
import { TopBar, BottomBar } from './Styled';

import { CapitalizedRobotoButton } from '../../../../Styled';
import OrientationPattern from '../../../../components/Layouts/OrientationPattern';
import { CRAFTING_FEEDBACK } from '../../../../utils/constants/pitchFlow';
import { isIOSSafari } from '../../../../utils/detectBrowser';

class RoomRoomControlBar extends React.Component {
    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    render() {
        const {
            roomStore: {
                leaveRoom,
                setPitchState,
                setIsFeedbackOnDeck,
                room: { companion },
                pitch: { timeToRequestTheFeedback, isFeedbackOnDeck },
            },
        } = this.props;

        const switchFullScreen = () => {
            if (!document.fullscreenElement) {
                gaSwitchToFullScreen();
                document.documentElement.requestFullscreen();
            } else {
                gaQuitFullScreen();
                document.exitFullscreen();
            }
        };

        const topBar = (
            <TopBar className="p-d-flex p-jc-center p-dir-row">
                <CapitalizedRobotoButton
                    className="p-button-rounded p-button-danger p-button-xsm stop"
                    label="LEAVE"
                    onClick={() => {
                        gaRoomControlBarLeaveRoom();
                        leaveRoom();
                    }}
                />
                {!isIOSSafari && (
                    <CapitalizedRobotoButton
                        className="p-button-rounded p-button-info p-button-xsm fullscreen"
                        label="FULLSIZE"
                        onClick={switchFullScreen}
                    />
                )}

                <div className="name_title">
                    <div className="title">your companion</div>
                    <div className="name p-mt-2">{companion ? companion.name : 'not in the room'}</div>
                </div>
            </TopBar>
        );
        const requestFeedbackButton = (
            <>
                {timeToRequestTheFeedback && (
                    <Button
                        style={{
                            position: 'absolute',
                            height: '43px',
                            transform: 'translate(0, -50%)',
                            bottom: '-34px',
                        }}
                        className="p-button-rounded p-button-help p-button-lg"
                        label="Request The Feedback"
                        onClick={() => {
                            setPitchState(CRAFTING_FEEDBACK);
                        }}
                    />
                )}
            </>
        );
        const backToFeedbackCraftButton = (
            <>
                {isFeedbackOnDeck && (
                    <Button
                        style={{
                            position: 'absolute',
                            height: '43px',
                            transform: 'translate(0, -50%)',
                            bottom: '-34px',
                        }}
                        className="p-button-rounded p-button-info p-button-lg"
                        label="Back To Ranking"
                        onClick={() => {
                            setIsFeedbackOnDeck(false, true);
                        }}
                    />
                )}
            </>
        );
        const bottomBar = (
            <BottomBar>
                {!timeToRequestTheFeedback && !isFeedbackOnDeck && (
                    <>
                        <div className="title">your companion</div>
                        <div className="name">{companion ? companion.name : 'not in the room'}</div>
                    </>
                )}
                <CapitalizedRobotoButton
                    className="p-button-rounded p-button-danger p-button-xsm stop"
                    label="LEAVE"
                    onClick={() => {
                        gaRoomControlBarLeaveRoom();
                        leaveRoom();
                    }}
                />
                {!isIOSSafari && (
                    <CapitalizedRobotoButton
                        className="p-button-rounded p-button-info p-button-xsm fullscreen"
                        label="FULLSIZE"
                        onClick={switchFullScreen}
                    />
                )}
                {requestFeedbackButton}
                {backToFeedbackCraftButton}
            </BottomBar>
        );
        return <OrientationPattern landscape={bottomBar} portrait={topBar} />;
    }
}

export default inject(({ roomStore, userStore, notifyStore }) => ({ roomStore, userStore, notifyStore }))(
    observer(RoomRoomControlBar)
);
