import styled from '@emotion/styled';

// eslint-disable-next-line
export const TopBar = styled.div`
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    text-align: center;
    min-height: 110px;
    box-sizing: border-box;
    border-style: solid;
    border-width: 18px;
    width: 100%;
    color: white;
    border-image: url(/media/images/misc/header_panel.svg) 18 fill stretch;

    .title {
        font-size: 14px;
    }

    .name {
        font-size: 36px;
        width: 100%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        text-align: center;
        padding-left: 20px;
        padding-right: 20px;
    }

    .name_title {
        width: 100%;
        position: fixed;
        top: 12px;
        pointer-events: none;
    }

    .stop {
        position: fixed;
        left: 10px;
        top: 12px;
    }

    .fullscreen {
        position: fixed;
        right: 10px;
        top: 12px;
    }
`;

export const BottomBar = styled.div`
    position: absolute;
    display: flex;
    justify-content: center;
    flex-direction: row;
    align-items: flex-end;
    left: 0px;
    bottom: 0px;
    width: 100%;
    text-align: center;
    height: 65px;
    box-sizing: border-box;
    border-style: solid;
    border-width: 18px;
    color: white;
    border-image: url(/media/images/misc/header_panel_landscape.svg) 18 fill stretch;

    .title {
        font-size: 14px;
        margin-bottom: -10px;
    }

    .name {
        font-size: 36px;
        margin-bottom: -15px;
        margin-left: 5px;
    }

    .stop {
        position: fixed;
        left: 10px;
        bottom: 12px;
    }

    .fullscreen {
        position: fixed;
        right: 10px;
        bottom: 12px;
    }
`;
