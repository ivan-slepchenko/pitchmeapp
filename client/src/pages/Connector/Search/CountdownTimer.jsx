import { Component } from 'react';
import moment from 'moment';
import { EVENT_TIME_STRING_FORMAT } from '../../../store/EventsStore';

class CountdownTimer extends Component {
    state = {
        days: undefined,
        hours: undefined,
        minutes: undefined,
        seconds: undefined,
    };

    componentDidMount() {
        const updateTime = () => {
            const { eventTimeInString } = this.props;
            const then = moment.utc(eventTimeInString, EVENT_TIME_STRING_FORMAT);
            const now = moment.utc();
            const countdown = moment.utc(then - now);
            const days = parseInt(countdown.format('D'), 10) - 1;
            const hours = countdown.format('HH');
            const minutes = countdown.format('mm');
            const seconds = countdown.format('ss');

            this.setState({ days, hours, minutes, seconds });
        };
        this.interval = setInterval(updateTime, 1000);
        updateTime();
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    render() {
        const { days, hours, minutes, seconds } = this.state;
        return (
            <div className="p-text-center p-pb-2">
                <div className="p-d-flex p-dir-row">
                    {days > 0 && <div className="countdown-item p-mr-1">{days} days</div>}
                    {hours > 0 && <div className="countdown-item p-mr-1">{hours} hours</div>}
                    {days === 0 && <div className="countdown-item p-mr-1">{minutes} minutes</div>}
                    {(days === 0 && hours ===0) && <div className="countdown-item p-mr-1">{seconds} seconds</div>}
                </div>
            </div>
        );
    }
}

export default CountdownTimer;
