// @ts-nocheck
import React, { Component } from 'react';
import { ResizableBox } from 'react-resizable';
import 'react-resizable/css/styles.css';
import { FaRegCopy } from 'react-icons/fa';

import devStore from 'utils/devStore';
import ls from 'utils/localStorage';
import { IS_DEV } from 'utils/constants';
import { copy } from 'utils/helpers';
import { ROUTES } from 'routes';
import { gaSearchBackToMirror } from 'utils/ga';

import Loader from 'components/UI/Loader';

import MirrorVideo from 'components/Mirror';
import { Button } from 'primereact/button';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import { MirrorWrapper, InviteBlock } from './Styled';
import { BottomBar } from '../../../components/UI/Bars';
import { CenteredContainer, FullSizeAbsoluteFlexColumnContainer, WhiteText } from '../../../Styled';
import { ColorTintWrapper } from '../Room/Media/Styled';
import BeBusyWhileWaiting from '../../EvilInvestor/BeBusyWhileWaiting';
import OrientationPattern from '../../../components/Layouts/OrientationPattern';

const MIRROR_SIZE = 'mirror_size';

@withRouter
@inject(({ roomStore, eventsStore, notifyStore, userStore}) => ({ roomStore, eventsStore, notifyStore, userStore }))
@observer
class SearchGame extends Component {
    render() {
        const {
            userStore: { inviteCode, chosenPitch },
            roomStore: { getWaitingTime, usersInRoomData },
            eventsStore: { eventSquaredLogo, eventStatus },
            history,
        } = this.props;

        let pairedUsersInRoomText = '';
        let wantsToMatchInRoomText = '';
        if(usersInRoomData[inviteCode] != null){
            const performersInRoom = usersInRoomData[inviteCode].ROOM?.performer || 0;
            const expertsInRoom = usersInRoomData[inviteCode].ROOM?.expert || 0;
            let performersInSearch = usersInRoomData[inviteCode].SEARCH?.performer || 0;
            let expertsInSearch =  usersInRoomData[inviteCode].SEARCH?.expert || 0;
            if(chosenPitch == null) {
                expertsInSearch -= 1;
            } else {
                performersInSearch -= 1;
            }

            const currentlyPaired = performersInRoom + expertsInRoom;
            pairedUsersInRoomText = `${currentlyPaired} users pitching`;
            wantsToMatchInRoomText = `${performersInSearch} founders, ${expertsInSearch} experts wants to match`;
        }

        const usersInRoomBlock = (
            <WhiteText
                style={{
                    top:'0px',
                    position:'absolute',
                    fontSize: '14px',
                    color: 'yellow',
                    width: '100%',
                    textAlign:'center',
                    paddingTop: '3px'
                }}>
                {wantsToMatchInRoomText}<br/>{pairedUsersInRoomText}
            </WhiteText>
        )

        const streamMirrorSize = ls.get(MIRROR_SIZE) || 320;
        const goToMirrorButton = (
            <Button
                className="p-button-rounded p-button-secondary p-button-rounded-lg p-mt-2"
                icon="pi pi-times"
                onClick={() => {
                    gaSearchBackToMirror(getWaitingTime());
                    history.push({ pathname: ROUTES.MIRROR.path });
                }}
            />
        );

        const portraitMode = (
            <FullSizeAbsoluteFlexColumnContainer>
                {usersInRoomBlock}
                <CenteredContainer>
                    {eventStatus === "ongoing" && (
                        <>
                            <Loader
                                width="130px"
                                height="130px"
                                inner={<img src={eventSquaredLogo} className="p-p-2" alt="event logo" />}
                            />
                            <span className="p-mt-4 p-mb-1">looking for a companion...</span>
                        </>
                    )}
                    <BeBusyWhileWaiting />
                </CenteredContainer>
                <BottomBar>{goToMirrorButton}</BottomBar>
            </FullSizeAbsoluteFlexColumnContainer>
        );
        const landscapeMode = (
            <FullSizeAbsoluteFlexColumnContainer>
                {usersInRoomBlock}
                <CenteredContainer>
                    {eventStatus === "ongoing" && (
                        <>
                            <Loader
                                width="8vh"
                                height="8vh"
                                inner={<img src={eventSquaredLogo} className="p-p-2" alt="event logo" />}
                            />
                            <span className="p-mt-4 p-mb-1">looking for a companion...</span>
                        </>
                    )}
                    <BeBusyWhileWaiting className="p-mb-2" />
                    {goToMirrorButton}
                </CenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );

        return (
            <WhiteText>
                <ColorTintWrapper />

                {devStore.isStreamMode() && (
                    <MirrorWrapper>
                        <ResizableBox
                            className="resizable-box"
                            width={streamMirrorSize}
                            height={streamMirrorSize}
                            lockAspectRatio
                            minConstraints={[120, 120]}
                            maxConstraints={[480, 480]}
                            draggableOpts={{ grid: [32, 32] }}
                            onResize={(_, { size }) => ls.set(MIRROR_SIZE, size.width)}
                        >
                            {/* <MirrorVideo /> */}
                        </ResizableBox>
                    </MirrorWrapper>
                )}
                <OrientationPattern landscape={landscapeMode} portrait={portraitMode} />
            </WhiteText>
        );
    }
}

export default SearchGame;
