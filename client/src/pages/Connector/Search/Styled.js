import styled from '@emotion/styled';

export const CountDownTimerWrapper = styled.div`
    .countdown-wrapper {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-wrap: wrap;
    }

    .countdown-item {
        color: #111;
        font-size: 20px;
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        line-height: 30px;
        margin: 10px;
        padding-top: 10px;
        position: relative;
        width: 30px;
        height: 30px;
    }

    .countdown-item span {
        color: #333;
        font-size: 10px;
        font-weight: 600;
        text-transform: uppercase;
    }
`;

export const CalendarButtonWrapper = styled.div`
    .react-add-to-calendar__wrapper {
        padding: 4px 8px;
        text-align: center;
        background: #007ab9;
        border-radius: 50px;
    }
    .react-add-to-calendar__dropdown {
        position: absolute;
        background: white;
        width: 100%;
        height: 100%;
        left: 0px;
        bottom: 0px;
        z-index: 1000;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        ul {
            li {
                text-align: center;
                padding-top: 4px;
                padding-bottom: 4px;
                padding-right: 20px;
                padding-left: 20px;
                background-color: gray;
                border-radius: 6px;
                margin: 2px;
                a {
                    text-decoration: none;
                    color: white;
                }
            }
            padding-inline-start: unset;
        }
    }
`;

export const MirrorWrapper = styled.div`
    position: fixed;
    top: 125px;
    left: 60px;
    overflow: hidden;

    .resizable-box {
        overflow: hidden;
        border-radius: 100%;
        position: relative;

        .react-resizable-handle {
            border: 2px dashed #fff;
            border-radius: 100%;
            width: 100%;
            height: 100%;
            z-index: 9;
        }
    }
`;

export const InviteBlock = styled.div`
    position: fixed;
    z-index: 999;
    top: 20%;
    left: 50%;
    transform: translate(-50%, 0);
    font-size: 14px;
    text-align: center;
    cursor: pointer;

    svg {
        margin-left: 10px;
        width: 20px;
        height: 20px;
        transform: translateY(4px);
    }
`;
