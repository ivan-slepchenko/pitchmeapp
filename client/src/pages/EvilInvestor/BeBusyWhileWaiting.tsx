// @ts-nocheck
import { Component } from 'react';

import { inject, observer } from 'mobx-react';
import { bind } from 'decko';
import AddToCalendar from 'react-add-to-calendar';
import { Button } from 'primereact/button';
import CountdownTimer from '../Connector/Search/CountdownTimer';
import { questions } from './questions';
import { EvilInvestorThumbnail } from './Styled';
import { Title, BlackBorderedDiv, FormLabel, MultilineFormRow, NoWrapRow, CapitalizedRobotoButton } from '../../Styled';
import { CalendarButtonWrapper } from '../Connector/Search/Styled';
import ls from '../../utils/localStorage';

@inject(({ eventsStore }) => ({ eventsStore }))
@observer
class BeBusyWhileWaiting extends Component {
    state = {
        notAskedQuestions: [],
        question: '',
    };

    componentDidMount() {
        this.nextQuestion();
    }

    getQuestion() {
        const { notAskedQuestions } = this.state;
        let questionsToChoose = notAskedQuestions;
        if(questionsToChoose.length === 0) {
            questionsToChoose = [...questions];
        };
        const chosenQuestionIndex = Math.round(Math.random() * (questionsToChoose.length - 1));
        const question = questionsToChoose[chosenQuestionIndex];
        questionsToChoose.splice(chosenQuestionIndex, 1);
        this.setState({notAskedQuestions: questionsToChoose})

        return question;
    }

    getEventNotStartedYetBlock() {
        const {
            eventsStore: {
                event: { startTime, title },
            },
        } = this.props;
        return (
            <>
                    <div className="p-mt-2 p-mb-2 p-button-info">
                        <NoWrapRow className="p-d-flex p-jc-center" style={{padding: "2px"}}>
                            <FormLabel>the event "{title}"</FormLabel>
                        </NoWrapRow>
                    </div>
                    <NoWrapRow className="p-d-flex p-jc-center p-mb-2">
                        <FormLabel>starts in:</FormLabel>
                    </NoWrapRow>
                    <CountdownTimer eventTimeInString={startTime}/>
            </>
        );
    }

    getEventIsFinishedBlock() {
        const {
            eventsStore: {
                event: { title },
            },
        } = this.props;
        return (
            <>
                <div className="p-mt-2 p-mb-2 p-button-info">
                    <NoWrapRow className="p-d-flex p-jc-center" style={{padding: "2px"}}>
                        <FormLabel>the event "{title}" is finished</FormLabel>
                    </NoWrapRow>
                </div>
            </>
        );
    }

    getEventIsOngoingBlock() {
        const {
            eventsStore: {
                event: { title },
            },
        } = this.props;
        return (
            <>
                <div className="p-mt-2 p-mb-2 p-button-info">
                    <NoWrapRow className="p-d-flex p-jc-center" style={{padding: "2px"}}>
                        <FormLabel>ongoing event: "{title}"</FormLabel>
                    </NoWrapRow>
                </div>
            </>
        );
    }

    @bind
    nextQuestion() {
        this.setState({ question: this.getQuestion() });
        this.forceUpdate();
    }

    render() {
        const { question } = this.state;
        const {
            eventsStore: { event, featureFlags, eventStatus },
        } = this.props;
        let eventBlock = null;
        if (event && featureFlags.EVENT_STATUS_DIALOG.enabled) {
            switch (eventStatus) {
                case 'not_started':
                    eventBlock = this.getEventNotStartedYetBlock();
                    break;
                case 'ongoing':
                    eventBlock = this.getEventIsOngoingBlock();
                    break;
                default:
                    eventBlock = this.getEventIsFinishedBlock();
                    break;
            }
        }

        const evilInvestor = (
            <>
                <NoWrapRow className="p-d-flex p-jc-center p-mb-2">
                    <FormLabel>You can practice answering questions:</FormLabel>
                </NoWrapRow>
                <MultilineFormRow className="p-d-flex p-jc-left p-ai-center p-mb-2">
                    <EvilInvestorThumbnail className="p-mr-2" />
                    <FormLabel>{question}</FormLabel>
                </MultilineFormRow>
                <NoWrapRow className="p-d-flex p-jc-center">
                    <CapitalizedRobotoButton
                        type="submit"
                        label="NEXT"
                        className="p-button-rounded p-button-sm p-button-success"
                        onClick={this.nextQuestion}
                    />
                </NoWrapRow>
            </>
        );
        return (
            <>
                {eventBlock}
                <BlackBorderedDiv static style={{ zIndex: 100 }}>
                    {evilInvestor}
                </BlackBorderedDiv>
            </>
        );
    }
}
export default BeBusyWhileWaiting;
