import { useState } from 'react';
import styled from '@emotion/styled';

import { Button } from 'primereact/button';
import { questions } from './questions';
import { ROUTES } from '../../routes';
import ls from '../../utils/localStorage';
import { LS_KEYS } from '../../utils/constants/lsKeys';
import { EvilInvestorThumbnail } from './Styled';
import {
    AnnieTitle,
    BlackBorderedDiv,
    CenteredContainer,
    FullSizeAbsoluteFlexColumnContainer,
    RoundedImage,
    WhiteText,
} from '../../Styled';

const Wrapper = styled.div`
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    background: url('/media/images/misc/dr-evil.png');
    background-size: cover;
    background-position: center;

    .evil-investor {
        &__main {
            &-text {
                position: fixed;
                top: 100px;
                left: 50%;
                transform: translate(-50%);
                font-size: 22px;
            }

            &-btn {
                position: fixed;
                bottom: 100px;
                left: 50%;
                transform: translate(-50%);
                font-size: 22px;
                padding: 30px;
                min-width: 200px;
            }
        }
    }

    @media (max-width: 480px) {
        .evil-investor {
            &__main {
                &-text {
                    font-size: 18px;
                }

                &-btn {
                    font-size: 18px;
                }
            }
        }
    }
`;

export const PAGE_TITLE = 'evil-investor';

const MAX_QUESTIONS = 5;
const helloText =
    'There we have more than 300 questions collected from real pitch sessions! ' + 'Check if you can handle them!';

export default function EvilInvestor() {
    const [askedQuestionIndexes, handleAskedQuestionIndexes] = useState([]);
    const getQuestion = () => {
        const qIndex = Math.floor(Math.random() * questions.length);

        if (askedQuestionIndexes.includes(qIndex)) {
            return getQuestion();
        }

        handleAskedQuestionIndexes([...askedQuestionIndexes, qIndex]);

        return questions[qIndex];
    };

    function formatDate(dateNow) {
        return new Intl.DateTimeFormat('en-GB', {
            year: 'numeric',
            month: 'long',
            day: '2-digit',
        }).format(dateNow);
    }
    const dateNow = formatDate(new Date());
    const ifINeedToSayHello = ls.get('lastLaunchDate') !== dateNow;
    ls.set('lastLaunchDate', dateNow);

    const [question, handleGetQuestion] = useState(helloText);
    console.log(helloText, question);

    const isAuth = ls.get(LS_KEYS.USERTOKEN);
    const isFreeQuestionsUsed = askedQuestionIndexes.length >= MAX_QUESTIONS;
    const ifJustStarted = askedQuestionIndexes.length === 0;
    const needToSignUp = isAuth ? false : isFreeQuestionsUsed; // не реактивное
    const getBtnText = () => {
        if (needToSignUp) {
            return 'More Questions';
        }

        return (isFreeQuestionsUsed && !isAuth) || ifJustStarted ? 'Ok' : 'I voiced the answer, Next!';
    };

    return (
        <Wrapper>
            <FullSizeAbsoluteFlexColumnContainer>
                <BlackBorderedDiv static className="p-text-center" style={{ maxWidth: '500px' }}>
                    <>
                        <RoundedImage
                            width="50px"
                            height="50px"
                            src="/media/images/logos/1x/logo.png"
                            alt="PitchMe.APP"
                            className="p-mb-2"
                        />
                        {question}
                        <Button
                            className="p-button-rounded p-button-help p-mt-2"
                            label="Give me a next question!"
                            type="button"
                            onClick={() => {
                                if (needToSignUp) {
                                    ls.set(LS_KEYS.EVIL_INVESTOR_SIGN_UP, true);
                                    handleGetQuestion(getQuestion());
                                } else {
                                    handleGetQuestion(getQuestion());
                                }
                            }}
                        />
                        <WhiteText className="p-mt-2">Don't forget to try our main app</WhiteText>
                        <a style={{ color: 'pink' }} href="https://app.pitchme.app">
                            app.pitchme.app
                        </a>
                    </>
                </BlackBorderedDiv>
            </FullSizeAbsoluteFlexColumnContainer>
        </Wrapper>
    );
}
