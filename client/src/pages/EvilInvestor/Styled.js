import styled from '@emotion/styled';
import { BlackBorderedDiv } from '../../Styled';

export const EvilInvestorThumbnail = styled(BlackBorderedDiv)`
    border-radius: 60px;
    width: 60px;
    height: 60px;
    min-width: 60px;
    min-height: 60px;
    background: url(/media/images/misc/dr-evil_sqsm.png);
    background-size: cover;
`;
