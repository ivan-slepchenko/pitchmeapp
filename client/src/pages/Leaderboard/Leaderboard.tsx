import { useEffect, useState } from 'react';
import { inject, observer } from 'mobx-react';
import { ScrollPanel } from 'primereact/scrollpanel';
import { CenteredContainer, FullSizeAbsoluteFlexColumnContainer, RoundedImage, Row, CapitalizedRobotoButton } from '../../Styled';
import {
    CrownBadge,
    IndexColumn,
    LabelButton,
    LeaderboardContainer,
    LeaderboardHeader,
    LeaderboardImage,
    RoundedFlexNoWrapRow,
    ThumbnailColumn,
    ThumbnailContainer,
} from './Styled';
import { ROUTES } from '../../routes';
import leaderboardImage from './leaderboard.jpg';
import { getUserThumbnail } from '../../utils/helpers';
import UserStore from '../../store/UserStore';
import EventsStore from '../../store/EventsStore';
import StatsStore from '../../store/StatsStore';

const RESULTS_PER_SCREEN = 50;

interface LeaderboardProps {
    standalone: boolean,
    userStore: UserStore,
    statsStore: StatsStore,
    eventsStore: EventsStore,
    history: {
        push: any
    }
}

interface LeaderboardEntry {
    userId: any,
    rank: any,
    feedbacks: any,
    name: string
}

const Leaderboard = inject(
    'statsStore',
    'userStore',
    'eventsStore'
)(
    observer(
        ({
             userStore: {
                 user: { id: userId },
                 inviteCode,
                 chosenPitch
             },
             statsStore: { eventLeaderboardData, brandLeaderboardData, fetchLeaderboard },
             eventsStore: { event },
             history,
             standalone
        }:LeaderboardProps) => {
            const [tabIndex, setTabIndex] = useState(0);

            useEffect(() => {
                if (event != null) {
                    fetchLeaderboard();
                }
            }, [event, fetchLeaderboard])

            const entries = [];
            const leaderboardData:Array<LeaderboardEntry> = (tabIndex === 0 && inviteCode != null) ? eventLeaderboardData : brandLeaderboardData;

            const userLeaderboardEntry = leaderboardData.find((entry:LeaderboardEntry) => entry.userId === userId);
            const isLeaderboardUnlocked = chosenPitch == null || (userLeaderboardEntry && userLeaderboardEntry.feedbacks >= 3);

            const maxLoopIndex = Math.min(RESULTS_PER_SCREEN, leaderboardData.length);
            for (let i = 0; i < maxLoopIndex; i++) {
                const entry:LeaderboardEntry = leaderboardData[i];
                entries.push(
                    <RoundedFlexNoWrapRow
                        className="p-mb-1 p-text-center"
                        index={i}
                        highlight={entry.userId === userId}
                        key={i}
                    >
                        <IndexColumn top3={i < 3} className="p-col-1">
                            {i + 1}
                        </IndexColumn>
                        <ThumbnailColumn className="p-col-2 p-d-flex p-jc-center">
                            <ThumbnailContainer>
                                {i === 0 && <CrownBadge />}
                                <RoundedImage width="40px" height="40px" src={getUserThumbnail(entry.userId)} alt="photo" />
                            </ThumbnailContainer>
                        </ThumbnailColumn>
                        <span className="p-col-5 p-ellipsis">{entry.name}</span>
                        <span className="p-col-2">{entry.feedbacks}</span>
                        <span className="p-col-2">{entry.rank}</span>
                    </RoundedFlexNoWrapRow>
                );
            }
            const leaderboardRows = (
                <>
                    <LeaderboardHeader className="p-d-flex p-mb-1 p-text-center">
                        <span className="p-col-1 ">rank</span>
                        <span className="p-col-2" />
                        <span className="p-col-5">name</span>
                        <span className="p-col-2">sessions</span>
                        <span className="p-col-2">score</span>
                    </LeaderboardHeader>
                    <ScrollPanel style={{ flex: '1 1 auto', overflow: 'auto' }}>{entries}</ScrollPanel>
                </>
            );
            const highlightStyle = { color: 'cornflowerblue', textDecoration: 'underline' };
            return (
                <FullSizeAbsoluteFlexColumnContainer>
                    {isLeaderboardUnlocked && leaderboardData.length < 3 && <LeaderboardImage src={leaderboardImage} />}
                    <LeaderboardContainer className="p-p-md-5 p-p-sm-1 p-d-flex p-flex-column p-jc-start">
                        <Row className="p-d-flex p-flex-row p-jc-around" style={{ flex: '0 1 auto' }}>
                            {inviteCode != null && (
                                <LabelButton
                                    style={tabIndex === 0 ? highlightStyle : {}}
                                    onClick={() => {
                                        setTabIndex(0)
                                    }}
                                    type="button"
                                >
                                    <h3>Today</h3>
                                </LabelButton>
                            )}
                            <LabelButton
                                style={tabIndex === 1 ? highlightStyle : {}}
                                onClick={() => {
                                    setTabIndex(1);
                                }}
                                type="button"
                            >
                                <h3>Last Month</h3>
                            </LabelButton>
                        </Row>
                        {isLeaderboardUnlocked ? (
                            leaderboardRows
                        ) : (
                            <CenteredContainer className="p-as-center p-mb-auto p-mt-auto">
                                <div>To see leaderboard</div>
                                <div>please pitch at least 3 times</div>
                            </CenteredContainer>
                        )}
                        {!standalone &&
                            <div className="p-d-flex p-dir-row p-jc-center p-ai-center p-pt-1 p-pb-0">
                                <CapitalizedRobotoButton
                                    label="Back"
                                    className="p-button-rounded p-button-help p-button-sm p-ml-2 p-mr-1"
                                    onClick={() => {
                                        history.push({
                                            pathname: ROUTES.MIRROR.path,
                                        });
                                    }}
                                />
                            </div>
                        }
                    </LeaderboardContainer>
                </FullSizeAbsoluteFlexColumnContainer>
            );
        }
    )
)

export default Leaderboard;
