import styled from '@emotion/styled';
import { FlexNoWrapRow } from '../../Styled';
import { ReactComponent as RankingFilledIcon } from '../../components/UI/SVG/crown_filled_icon.svg';

const colors = ['#eca400', '#E0FF4F', '#2F97C1', '#EEE'];

export const ThumbnailContainer = styled.div`
    width: 40px;
    height: 40px;
`;

export const ThumbnailColumn = styled.div`
    padding: 2px;
`;

type IndexColumnProps = {
    top3:boolean;
}
export const IndexColumn = styled.div`
    font-weight: ${(props:IndexColumnProps) => (props.top3 ? 'bold' : 'normal')};
`;

export const CrownBadge = styled(RankingFilledIcon)`
    width: 20px;
    height: 20px;
    color: yellow;
    position: absolute;
    background: blueviolet;
    padding: 2px;
    border-radius: 100%;
    margin-left: 22px;
    margin-top: -3px;
    transform: rotate(13deg);
`;

export const LeaderboardHeader = styled.div`
    font-size: 14px;
`;

export const LeaderboardContainer = styled.div`
    width: 100%;
    height: 100%;
    padding: 10px;
    max-width: 800px;
    .p-paginator {
        padding: 0px;
    }
`;
export const LeaderboardImage = styled.img`
    left: 50%;
    top: 50%;
    position: absolute;
    transform: translate(-50%, -50%);
    opacity: 0.5;
`;

type RoundedFlexNoWrapRowProps = {
    index: number,
    highlight: boolean
}

export const RoundedFlexNoWrapRow = styled(FlexNoWrapRow)`
    border-radius: 6px;
    background: ${(props:RoundedFlexNoWrapRowProps) => colors[Math.min(props.index, 3)]};
    border: ${(props:RoundedFlexNoWrapRowProps) => (props.highlight ? 'blueviolet' : 'none')};
    border-style: ${(props:RoundedFlexNoWrapRowProps) => (props.highlight ? 'solid' : 'none')};
`;

export const LabelButton = styled.button`
    border: none;
    background: none;
    color: #4f4f4f;
    &:focus {
        outline: none;
    }
`;
