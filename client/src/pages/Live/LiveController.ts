import api from '../../utils/api';

export interface GroupI {
    id: number,
    title: string,
    rightTitle: string,
    stackItems?: boolean,
    height?: number
}

export interface ItemI {
    id: number,
    group: number,
    title: string,
    start_time: number,
    end_time: number,
    canMove: boolean,
    canResize: boolean,
    canChangeGroup: boolean,
    itemProps: any
}

interface InLobbyStatusI {
    email: string,
    status: string,
    datetime: number,
    inviteCode:string,
    isPerformer:boolean
}

export interface TimelineDataI {
    groups: Array<GroupI>,
    records: Array<ItemI>
}

export const getLiveTimeline = async () => api.get(`/live/timeline`).then((response):TimelineDataI => {
    const counters={
        groupId: 0,
        itemId: 0
    };

    const timelineGroupsMap = new Map<string, GroupI>();
    const records:Array<ItemI> = [];

    function getGroupForEntryOrCreateIt(entry: InLobbyStatusI):number {
        return timelineGroupsMap.get(entry.email)?.id || (() => {
            counters.groupId += 1;
            const group = {
                id: counters.groupId,
                title: entry.email,
                rightTitle: entry.email,
            };
            timelineGroupsMap.set(entry.email, group);
            return group.id;
        })();
    }

    function buildTimelineRecord(entry: InLobbyStatusI, groupId: number) {
        if (records.length > 0) {
            const userRecords = records.filter((record)=>record.group === groupId);
            if(userRecords.length > 0) {
                const previousEntry = userRecords[userRecords.length - 1];
                if (previousEntry.end_time === 0 && previousEntry.title !== entry.status) {
                    previousEntry.end_time = entry.datetime;
                }
            }
        }

        switch (entry.status) {
            case 'INACTIVE': {
                // don't do anything extra for this case
                break;
            }
            default: {
                counters.itemId += 1;
                const item = {
                    id: counters.itemId,
                    group: groupId,
                    title: entry.status,
                    start_time: entry.datetime,
                    end_time: 0,
                    canMove: false,
                    canResize: false,
                    canChangeGroup: false,
                    itemProps: {},
                }

                if(entry.status === "SEARCH") {
                    item.itemProps = {
                        style: {
                            background: 'fuchsia',
                            border: 'none'
                        }
                    }
                } else {
                    item.itemProps = {
                        style: {
                            border: 'none'
                        }
                    }
                }

                records.push(item);
                break;
            }
        }
    }

    for (let i = 0; i < response.data.length; i++) {
        const entry:InLobbyStatusI = response.data[i];
        const groupId = getGroupForEntryOrCreateIt(entry);
        buildTimelineRecord(entry, groupId);
    }

    const groups = Array.from(timelineGroupsMap.values());
    return {groups, records}
})
