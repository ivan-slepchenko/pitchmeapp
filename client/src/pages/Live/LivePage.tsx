// @ts-nocheck
import { inject, observer } from 'mobx-react';
import 'react-calendar-timeline/lib/Timeline.css'
import moment from 'moment'
import Timeline from "react-calendar-timeline/lib";
import { useEffect, useState } from 'react';
import { getLiveTimeline, TimelineDataI } from './LiveController';

const LivePage = inject()(
    observer(() => {
        const [timelineData, setTimelineData] = useState<TimelineDataI>({ groups:[], records:[] });

        useEffect(()=>{
            getLiveTimeline().then((data)=>{
                setTimelineData(data);
            });
        },[]);

        return (
            <Timeline
                groups={timelineData.groups}
                items={timelineData.records}
                defaultTimeStart={moment().add(-12, 'hour')}
                defaultTimeEnd={moment().add(+12, 'hour')}
                minZoom={1000}
                maxZoom={20 * 86400 * 1000}
                traditionalZoom
            />
        )
    }),
);

export default LivePage;
