// @ts-nocheck
import { Component } from 'react';
import { Button } from 'primereact/button';
import {
    gaMirrorWantsToJoinGlobalRoom,
    gaMirrorCantJoinRoomNoCameraAccess,
    gaMirrorCantJoinRoomTimeLimitExceed,
} from 'utils/ga';
import { inject, observer } from 'mobx-react';
import { bind } from 'decko';
import { ROUTES } from '../../routes';
import { ReactComponent as RankingIcon } from '../../components/UI/SVG/crown_icon.svg';
import { ReactComponent as StatsIcon } from '../../components/UI/SVG/stats_icon.svg';
import { handleNoCameraAccess } from './handleNoCameraAccess';
import { PaddingFixedButton } from './Styled';

@inject(({ userStore, roomStore, notifyStore, eventsStore }) => ({ userStore, roomStore, notifyStore, eventsStore }))
@observer
class BottomButtons extends Component {
    @bind
    handleClick(e) {
        const {
            notifyStore,
            history,
            userStore: { mediaStatus },
            roomStore: { isRoomLimitExceed, notifyAboutRoomTimeExceed },
        } = this.props;
        // TODO: может быть проблема с прямым доступом в комнату без камеры
        // так как проверка осуществляется только по роуту /mirror
        if (!mediaStatus) {
            e.preventDefault();
            handleNoCameraAccess(notifyStore.show, history.push);
        } else if (isRoomLimitExceed) {
            e.preventDefault();
            notifyAboutRoomTimeExceed();
            gaMirrorCantJoinRoomTimeLimitExceed();
        } else {
            history.push({
                pathname: ROUTES.ROOM.path,
            });
        }
    }

    render() {
        const {
            history,
            location,
            mediaStatus,
            userStore: { userCamAccess },
            eventsStore: {
                featureFlags,
            },
        } = this.props;
        return (
            <>
                <div className="p-d-flex p-jc-center p-mt-2">
                    <div className="p-d-flex p-flex-column p-jc-between p-mr-1">
                        {!featureFlags.NO_DECKS_DEFAULT_TIME.enabled && (
                            <PaddingFixedButton
                                style={{ width: '90px' }}
                                className="p-button-rounded p-button-info p-button-xsm"
                                label="DECKS"
                                onClick={() => {
                                    history.push(ROUTES.PITCH_SETTINGS.path,{ prevPath: location.pathname });
                                }}
                            />
                        )}

                        <Button
                            style={{ width: '90px', fontSize:'11px' }}
                            className="p-button-rounded p-button-info p-button-xsm"
                            label="OPEN MY PROFILE"
                            onClick={() => {
                                history.push(ROUTES.PROFILE.path, { prevPath: location.pathname });
                            }}
                        />
                    </div>

                    <Button
                        style={{fontSize: '15px !important', width: '140px'}}
                        className="p-button-rounded p-button-help"
                        label="Connect With Others"
                        disabled={userCamAccess && !mediaStatus}
                        onClick={(e) => {
                            gaMirrorWantsToJoinGlobalRoom();
                            this.handleClick(e);
                        }}
                    />
                    <div className="p-d-flex p-flex-column p-jc-between p-ml-1">
                        <PaddingFixedButton
                            style={{ width: '90px', fontSize:'11px' }}
                            className="p-button-rounded p-button-info p-button-xsm"
                            label="OPEN THE LEADERBOARD"
                            onClick={() => {
                                history.push(ROUTES.LEADERBOARD.path, { prevPath: ROUTES.MIRROR.path });
                            }}
                        />

                        {featureFlags.STATS.enabled && (
                            <Button
                                style={{ width: '90px' }}
                                className="p-button-rounded p-button-info p-button-xsm"
                                label="STATS"
                                onClick={() => {
                                    history.push(ROUTES.STATS.path, { prevPath: ROUTES.MIRROR.path });
                                }}
                            />
                        )}
                    </div>
                </div>
            </>
        );
    }
}

export default BottomButtons;
