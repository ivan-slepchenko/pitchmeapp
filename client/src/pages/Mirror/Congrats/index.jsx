import Modal from 'components/UI/Modal/';
import { FaCheck, FaFemale, FaMale } from 'react-icons/fa';
import styled from '@emotion/styled';

export const CongratsModalBody = styled.div`
    .congrats {
        &__image {
            width: 265px;
            margin: 20px auto 0;
            padding: 20px;

            img {
                width: 100%;
                height: auto;
            }
        }

        &__title {
            font-size: 18px;
            text-align: center;
        }

        &__list {
            display: flex;
            flex-direction: column;
            align-items: center;

            &-item {
                margin: 5px 0;
                font-size: 16px;
                font-weight: 300;

                &-female {
                    font-size: 18px;
                    fill: #ff2d55 !important;
                }

                &-male {
                    font-size: 18px;
                    fill: #007aff !important;
                }

                svg {
                    fill: #00c9af;
                }
            }
        }

        &__bottom {
            display: flex;
            justify-content: center;
            margin-top: 10px;
        }
    }
`;

export default function CongratsModal({ open, handleClose }) {
    return (
        <Modal open={open} handleClose={handleClose}>
            <CongratsModalBody>
                <div className="congrats__image">
                    <img src="/media/congrats.png" alt="congrats" />
                </div>
                <div className="congrats__title">
                    Level Up!
                    <br />
                    You earned:
                </div>
                <div className="congrats__list">
                    <div className="congrats__list-item">
                        <FaCheck /> New level - STUDENT
                    </div>
                    <div className="congrats__list-item">
                        <FaCheck /> 5 next matches with <FaMale className="congrats__list-item-male" />
                        /
                        <FaFemale className="congrats__list-item-female" /> filter
                    </div>
                </div>
            </CongratsModalBody>
        </Modal>
    );
}
