import { FaMask } from 'react-icons/fa';

import FakeIt from 'components/UI/FakeIt';
import { ExperimentalWrapper, MaskSelect } from './Styled';

function Experimental() {
    return (
        <ExperimentalWrapper>
            <FakeIt gaLabel="Masks">
                <MaskSelect>
                    Use Mask <FaMask />
                </MaskSelect>
            </FakeIt>
        </ExperimentalWrapper>
    );
}

export default Experimental;
