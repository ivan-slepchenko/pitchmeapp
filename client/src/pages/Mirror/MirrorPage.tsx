// @ts-nocheck
import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import MirrorVideo from 'components/Mirror';
import { gaMirrorEnableMic, gaMirrorEnableCamera } from 'utils/ga';
import { Button } from 'primereact/button';
import { NoMediaAccessWrapper, TurnMicButton } from './Styled';
import PitchSelector from '../../components/PitchSelector';
import { CenteredContainer, FullSizeAbsoluteFlexColumnContainer, CapitalizedRobotoButton } from '../../Styled';
import { BottomBar } from '../../components/UI/Bars';
import OrientationPattern from '../../components/Layouts/OrientationPattern';
import BottomButtons from './BottomButtons';
import VideoToggleButton from '../Connector/Room/Media/VideoToggleButton';
import { ROUTES } from '../../routes';

@inject(({ notifyStore, roomStore, userStore }) => ({ notifyStore, roomStore, userStore }))
@observer
class MirrorPage extends Component {
    render() {
        const {
            userStore: {
                userCamAccess,
                userMicAccess,
                mediaStatus,
                setUserCamAccess,
                setUserMicAccess,
                setMediaStatus,
                logout
            },
            history,
            location,
        } = this.props;

        const buttonsBlock = <BottomButtons history={history} location={location} mediaStatus={mediaStatus} />;
        const useYourCamButton = (isSmall) => (
            <Button
                className={{
                    'p-button-rounded': true,
                    'p-mt-1': true,
                    'p-button-warning': true,
                    'p-mb-1': true,
                    'p-button-sm': isSmall,
                }}
                style={{ width: '160px' }}
                label="USE YOUR CAM"
                onClick={() => {
                    setMediaStatus(false);
                    setUserCamAccess(true);
                    setUserMicAccess(true);
                    gaMirrorEnableCamera();
                }}
            />
        );
        const portraitLayout = (
            <FullSizeAbsoluteFlexColumnContainer>
                <PitchSelector history={history} />
                <BottomBar>{buttonsBlock}</BottomBar>
            </FullSizeAbsoluteFlexColumnContainer>
        );
        const landscapeLayout = (
            <FullSizeAbsoluteFlexColumnContainer>
                <CenteredContainer>
                    <PitchSelector history={history} />
                    {buttonsBlock}
                </CenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );

        return (
            <>
                {!userCamAccess && !userMicAccess ? (
                    <FullSizeAbsoluteFlexColumnContainer>
                        <NoMediaAccessWrapper>
                            <div>To connect</div>
                            {useYourCamButton(false)}
                            <div>or</div>
                            <TurnMicButton
                                className="p-button-rounded p-mb-1 p-button-error p-button-text"
                                label="USE YOUR MIC"
                                onClick={() => {
                                    setMediaStatus(false);
                                    setUserMicAccess(true);
                                    gaMirrorEnableMic();
                                }}
                            />
                        </NoMediaAccessWrapper>
                    </FullSizeAbsoluteFlexColumnContainer>
                ) : (
                    <>
                        <VideoToggleButton top media={null} playVideo={()=>{}}/>
                        <CapitalizedRobotoButton
                            className="p-button-rounded p-button-danger p-button-xsm stop"
                            style={{zIndex: 10000, position: "absolute", right: '0px', top: '0px', marginRight: '1%', marginTop: '1%'}}
                            label="LOGOUT"
                            onClick={logout}
                        />
                        <MirrorVideo />
                        <OrientationPattern landscape={landscapeLayout} portrait={portraitLayout} />
                    </>
                )}
            </>
        );
    }
}

export default MirrorPage;
