import styled from '@emotion/styled';

import { RadioButton } from 'primereact/radiobutton';
import { Button } from 'primereact/button';
import { FaVideoSlash, FaVideo, FaMicrophone, FaMicrophoneSlash } from 'react-icons/fa';
import { BlackBorderedDiv } from '../../Styled';

export const TurnMicButton = styled(Button)`
    border: none !important;
    font-weight: bold;
`;

export const PaddingFixedButton = styled(Button)`
    padding-bottom: 3px !important;
`;

export const LightRadioButton = styled(RadioButton)`
    .p-highlight {
        border-color: #a4bf00 !important;
        background: #a4bf00 !important;
        color: #ffffff;
    }
    .p-radiobutton-box {
        border-color: #a4bf00 !important;
        background: #90a704 !important;
    }
`;

export const PitchSelectorFramePitchesBlock = styled(BlackBorderedDiv)`
    width: 100%;
`;

export const ExperimentalWrapper = styled.div`
    position: fixed;
    right: 20px;
    bottom: 120px;
    z-index: 999;
`;

export const AddPitchArrow = styled.div`
    width: 20px;
    height: 20px;
    background: url('/media/images/misc/arrow_white.png');
    background-size: cover;
    background-position: center;
    width: 60px;
    height: 20px;
`;

export const MaskSelect = styled.div`
    background: rgba(0, 0, 0, 0.66);
    border-radius: 12px;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 14px 10px;
    padding-right: 54px;
    text-align: center;
    font-size: 16px;

    svg {
        margin-left: 6px;
        width: 24px;
        height: 24px;
    }
`;

export const NoMediaAccessWrapper = styled.div`
    position: absolute;
    width: 220px;
    left: 50%;
    top: 47%;
    transform: translate(-50%, -50%);
    text-align: center;
    font-size: 16px;

    .turn-on-btn {
        margin: 10px;
        border: 4px solid red;

        svg {
            margin-right: 6px;
            width: 25px;
            height: 25px;
        }
    }
`;

export const MiniMirrorToggleStyledDiv = styled.div`
    height: 25% !important;
    width: 25% !important;
    position: absolute;
    z-index: 10000;
    margin-left: 1%;
    margin-top: 1%;
    background: #555;
    border-radius: 100%;
    width: 4%;
    color: #fff000;
    min-width: 25px;
    min-height: 25px;
    max-width: 35px;
    max-height: 35px;
    cursor: pointer;
    left: 0px;
    top: ${(props) => (props.top ? '0px' : 'unset')};
    bottom: ${(props) => (props.bottom ? '0px' : 'unset')};
    border-color: white;
    border-width: 2px;
    border-style: solid;
`;

export const VideoIcon = styled(FaVideo)`
    width: 60%;
    height: 60%;
    color: #fff000;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`;

export const AudioIcon = styled(FaMicrophone)`
    width: 60%;
    height: 60%;
    color: #fff000;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`;

export const NoAudioIcon = styled(FaMicrophoneSlash)`
    width: 60%;
    height: 60%;
    color: #fff000;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`;

export const NoVideoIcon = styled(FaVideoSlash)`
    width: 60%;
    height: 60%;
    color: #fff000;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`;
