import { ROUTES } from '../../routes';
import { gaMirrorCantJoinRoomNoCameraAccess } from '../../utils/ga';

export function handleNoCameraAccess(showNotification, pushToHistory) {
    showNotification({
        message: (
            <div>
                Please turn on or unblock your camera and mic
                <br />
                <button
                    className="p-rounded-4 p-button-info p-button-sm p-ml-2 p-mr-1"
                    type="button"
                    onClick={() => {
                        // eslint-disable-next-line max-len
                        window.open(
                            'https://help.doxy.me/en/articles/95877-allow-access-to-camera-mic-for-all-browsers-operating-systems',
                            '_blank'
                        );
                    }}
                >
                    PRESS TO READ HOW TO UNLOCK YOUR CAMERA AND MIC
                </button>
            </div>
        ),
        options: {
            variant: 'warn',
        },
        key: 'noCameraAccess',
    });
    pushToHistory({
        pathname: ROUTES.MIRROR.path,
    });
    gaMirrorCantJoinRoomNoCameraAccess();
}
