// @ts-nocheck
import { Component } from 'react';
import { observable, action, set } from 'mobx';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { bind } from 'decko';
import { ROUTES } from 'routes';
import { TextInputField, TextInputArea } from 'components/UI/TextInputField';
import { CharactersLeft, MyProfileTitle } from './Styled';
import {
    RequiredAsterix,
    NextInFormButton,
    Title,
    Form,
    CenteredContainer,
    FullSizeAbsoluteFlexColumnContainer,
    NoWrapRow,
} from '../../Styled';

import { EMAIL_MAX_LENGTH, NAME_MAX_LENGTH } from '../../store/ProfilesStore';

function validateField(name, value) {
    switch (name) {
        case 'name': {
            const invalid = value.length === 0 || value.length > NAME_MAX_LENGTH;

            return {
                invalid,
                field: name,
                message: `required, maximum ${NAME_MAX_LENGTH} symbols`,
            };
        }

        case 'startupName': {
            const invalid = value.length === 0 || value.length > NAME_MAX_LENGTH;

            return {
                invalid,
                field: name,
                message: `required, maximum ${NAME_MAX_LENGTH} symbols`,
            };
        }

        default:
            return {
                invalid: false,
                field: name,
                message: '',
            };
    }
}

@withRouter
@inject(({ userStore, notifyStore, eventsStore }) => ({ userStore, notifyStore, eventsStore }))
@observer
class AboutPage extends Component {
    @observable
    errors = {
        name: '',
        info: '',
    };

    @action
    handleValidateField(name, value) {
        const { invalid, field, message } = validateField(name, value);

        if (invalid) {
            console.log(invalid, field, message);
        }
        set(this.errors, {
            [field]: invalid ? message : '',
        });
    }

    @bind
    handleChangeUserField(e) {
        const { name, value } = e.target;
        const {
            userStore: { changeUserField },
        } = this.props;

        this.handleValidateField(name, value);

        changeUserField(name, value);
    }

    @bind
    async handleSubmitUserProfile(e) {
        const {
            eventsStore: {
                featureFlags: { NO_DECKS_DEFAULT_TIME },
            },
        } = this.props;
        e.preventDefault();
        const { target } = e;

        [...target.elements].forEach(({ name, value }) => {
            this.handleValidateField(name, value);
        });

        const {
            history,
            userStore: { saveUser },
            notifyStore,
            location,
        } = this.props;

        const prevPath = location.state?.prevPath;

        const { errors } = this;

        if (Object.values(errors).some((value) => value)) {
            notifyStore.show({
                message: 'Fill all required fields',
                options: {
                    variant: 'error',
                },
            });
            return;
        }

        const success = await saveUser();

        if (!success) {
            notifyStore.show({
                message: 'Validation error. Please check the input fields!',
                options: {
                    variant: 'error',
                },
            });
            return;
        }

        if (prevPath) {
            history.push(prevPath);
        } else if (NO_DECKS_DEFAULT_TIME.enabled) {
            history.push(ROUTES.MIRROR.path);
        } else {
            history.push(ROUTES.PITCH_SETTINGS.path);
        }
    }

    render() {
        const {
            userStore: {
                user: { name, info, contactEmail, startupName },
            },
            location,
        } = this.props;

        const prevPath = location.state?.prevPath;

        return (
            <FullSizeAbsoluteFlexColumnContainer>
                <CenteredContainer>
                    <Form onSubmit={this.handleSubmitUserProfile}>
                        <MyProfileTitle>My Profile:</MyProfileTitle>
                        <NoWrapRow className="p-mt-0">
                            <TextInputField
                                placeholder="Your Name:"
                                name="name"
                                value={name}
                                maxLength={NAME_MAX_LENGTH}
                                onChange={this.handleChangeUserField}
                                autoComplete="off"
                            />
                            <RequiredAsterix>*</RequiredAsterix>
                        </NoWrapRow>
                        <NoWrapRow className="p-mt-2">
                            <TextInputField
                                placeholder="Startup Name:"
                                name="startupName"
                                value={startupName}
                                maxLength={NAME_MAX_LENGTH}
                                onChange={this.handleChangeUserField}
                                autoComplete="off"
                            />
                            <RequiredAsterix>*</RequiredAsterix>
                        </NoWrapRow>
                        <NoWrapRow className="p-mt-2">
                            <TextInputField
                                placeholder="Public Email:"
                                name="contactEmail"
                                value={contactEmail}
                                maxLength={EMAIL_MAX_LENGTH}
                                onChange={this.handleChangeUserField}
                                autoComplete="off"
                            />
                        </NoWrapRow>
                        <TextInputArea
                            size="small"
                            className="p-mt-2"
                            name="info"
                            placeholder="About You:"
                            autoResize
                            value={info}
                            rows={5}
                            maxLength={130}
                            onChange={this.handleChangeUserField}
                        />
                        <CharactersLeft>{`${130 - info.length} characters left`}</CharactersLeft>
                        <NextInFormButton
                            disabled={name.length === 0 && startupName.length === 0}
                            className="p-button-rounded p-button-help p-button-sm p-mt-2"
                            type="submit"
                            label={prevPath === ROUTES.MIRROR.path ? 'Back' : 'Next'}
                        />
                    </Form>
                </CenteredContainer>
            </FullSizeAbsoluteFlexColumnContainer>
        );
    }
}

export default AboutPage;
