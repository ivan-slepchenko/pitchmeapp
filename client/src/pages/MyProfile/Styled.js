import styled from '@emotion/styled';
import { Title } from '../../Styled';

export const CharactersLeft = styled.span`
    margin-left: auto;
    color: #b70000;
    font-size: 12px;
`;

export const MyProfileTitle = styled(Title)`
    @media (max-height: 400px) {
        display: none
    }
`
