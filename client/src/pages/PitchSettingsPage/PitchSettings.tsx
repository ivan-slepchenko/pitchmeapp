// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ROUTES } from 'routes';
import Loader from 'components/UI/Loader';
import { bind } from 'decko';
import { BorderedDiv, CenteredContainer, FullSizeAbsoluteFlexColumnContainer } from '../../Styled';
import PitchesManager from '../../components/PitchesManager';
import PitchEditor from '../../components/PitchEditor';

@withRouter
@inject(({ userStore, eventsStore, notifyStore, rootStore }) => ({ userStore, eventsStore, notifyStore, rootStore }))
@observer
class PitchSettings extends Component {
    state = {
        isInEditorMode: false,
    };

    componentDidMount() {
        const {
            userStore: {
                user: { pitches },
            },
        } = this.props;
        if (pitches.length === 0) {
            this.createNewPitch();
        }
    }

    @bind()
    createNewPitch() {
        const { userStore } = this.props;
        userStore.createPitchToEdit();
        this.setState({ isInEditorMode: true });
    }

    @bind()
    editPitch(pitchData) {
        const { userStore } = this.props;
        userStore.changePitchToEdit(pitchData);
        this.setState({ isInEditorMode: true });
    }

    @bind()
    pushPitch() {
        const { userStore } = this.props;
        userStore.pushPitch();
        this.setState({ isInEditorMode: false });
    }

    @bind()
    async deletePitch(pitchForDelete) {
        const { userStore } = this.props;
        await userStore.deletePitch(pitchForDelete);
        if (userStore.user.pitches.length === 0) {
            this.setState({ isInEditorMode: true });
        }
    }

    @bind()
    exit() {
        const {
            userStore: {
                user: { pitches },
            },
        } = this.props;
        this.setState({ isInEditorMode: false });
        if (pitches.length === 0) {
            this.goMirror();
        }
    }

    @bind()
    goMirror() {
        const { history } = this.props;
        history.push(ROUTES.MIRROR.path);
    }

    render() {
        const {
            userStore,
            userStore: { inUploadProcess },
            eventsStore: { eventSquaredLogo },
            notifyStore,
            location,
        } = this.props;

        const { isInEditorMode } = this.state;

        if (!userStore.user.id) {
            return (
                <CenteredContainer>
                    <Loader inner={<img src={eventSquaredLogo} alt="Connecting..." />} />
                </CenteredContainer>
            );
        }

        return (
            <FullSizeAbsoluteFlexColumnContainer>
                <BorderedDiv>
                    {isInEditorMode ? (
                        <PitchEditor
                            notifyStore={notifyStore}
                            userStore={userStore}
                            exit={this.exit}
                            pushPitch={this.pushPitch}
                            prevPath={location.state?.prevPath}
                        />
                    ) : (
                        <PitchesManager
                            pitches={userStore.user.pitches}
                            createNewPitch={this.createNewPitch}
                            editPitch={this.editPitch}
                            deletePitch={this.deletePitch}
                            goNextStep={this.goMirror}
                            prevPath={location.state?.prevPath}
                            inUploadProcess={inUploadProcess}
                        />
                    )}
                </BorderedDiv>
            </FullSizeAbsoluteFlexColumnContainer>
        );
    }
}

export default PitchSettings;
