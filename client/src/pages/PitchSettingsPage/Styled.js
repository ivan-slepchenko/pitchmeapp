import styled from '@emotion/styled';

export const Title = styled.h2`
    font-size: large;
    text-align: center;
    margin: 0;
`;
