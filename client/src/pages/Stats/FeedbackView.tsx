// @ts-nocheck
import { Component } from 'react';
import { observer } from 'mobx-react';
import { PillowLabel, YellowRating } from '../Connector/Room/Game/Actions/Styled';
import { FEEDBACK } from '../../config';

@observer
class FeedbackView extends Component {
    render() {
        const { feedback, shadowWholeBlock } = this.props;
        return (
            <>
                {feedback.a && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.a.label}</PillowLabel>
                        <YellowRating readonly value={feedback.a} cancel={false} />
                    </>
                )}
                {feedback.b && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.b.label}</PillowLabel>
                        <YellowRating readonly value={feedback.b} cancel={false} />
                    </>
                )}
                {feedback.c && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.c.label}</PillowLabel>
                        <YellowRating readonly value={feedback.c} cancel={false} />
                    </>
                )}
                {feedback.d && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.d.label}</PillowLabel>
                        <YellowRating readonly value={feedback.d} cancel={false} />
                    </>
                )}
                {feedback.e && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.e.label}</PillowLabel>
                        <YellowRating readonly value={feedback.e} cancel={false} />
                    </>
                )}
                {feedback.f && (
                    <>
                        <PillowLabel shadowWholeBlock={shadowWholeBlock}>{FEEDBACK.f.label}</PillowLabel>
                        <YellowRating readonly value={feedback.f} cancel={false} />
                    </>
                )}
            </>
        );
    }
}

export default FeedbackView;
