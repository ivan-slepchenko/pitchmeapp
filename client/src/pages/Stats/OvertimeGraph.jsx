import { ResponsiveLine } from '@nivo/line';
import { Component } from 'react';

class OvertimeGraph extends Component {
    render() {
        const { data, graphWidth } = this.props;
        return (
            <ResponsiveLine
                data={data}
                width={graphWidth}
                margin={{ top: 5, right: 50, bottom: 10, left: 50 }}
                xScale={{ type: 'point' }}
                yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false }}
                axisTop={null}
                axisRight={null}
                axisBottom={null}
                colors={{ scheme: 'accent' }}
                lineWidth={4}
                isInteractive={false}
                axisLeft={{
                    orient: 'left',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: 'overtime (sec)',
                    legendOffset: -40,
                    legendPosition: 'middle',
                }}
                pointSize={10}
                pointColor={{ theme: 'background' }}
                pointBorderWidth={2}
                pointBorderColor={{ from: 'serieColor' }}
                pointLabelYOffset={-12}
                useMesh
                animate={false}
            />
        );
    }
}

export default OvertimeGraph;
