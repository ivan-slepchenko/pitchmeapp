import { ResponsiveLine } from '@nivo/line';
import { Component } from 'react';

class StarsGraph extends Component {
    render() {
        const { data, graphWidth } = this.props;
        return (
            <ResponsiveLine
                data={data}
                width={graphWidth}
                margin={{ top: 50, right: 50, bottom: 5, left: 50 }}
                xScale={{ type: 'point' }}
                yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: false, reverse: false }}
                axisTop={null}
                axisRight={null}
                axisBottom={null}
                colors={{ scheme: 'accent' }}
                lineWidth={6}
                areaBlendMode="multiply"
                isInteractive={false}
                axisLeft={{
                    orient: 'left',
                    tickSize: 5,
                    tickPadding: 5,
                    tickRotation: 0,
                    legend: 'category stars',
                    legendOffset: -40,
                    legendPosition: 'middle',
                }}
                pointSize={10}
                pointColor={{ theme: 'background' }}
                pointBorderWidth={2}
                pointBorderColor={{ from: 'serieColor' }}
                pointLabelYOffset={-12}
                useMesh
                legends={[
                    {
                        anchor: 'top',
                        direction: 'row',
                        justify: false,
                        translateX: 0,
                        translateY: -40,
                        itemsSpacing: 0,
                        itemDirection: 'left-to-right',
                        itemWidth: 130,
                        itemHeight: 20,
                        itemOpacity: 0.75,
                        symbolSize: 20,
                        symbolShape: 'circle',
                        symbolBorderColor: 'rgba(0, 0, 0, .5)',
                        effects: [
                            {
                                on: 'hover',
                                style: {
                                    itemBackground: 'rgba(0, 0, 0, .03)',
                                    itemOpacity: 1,
                                },
                            },
                        ],
                    },
                ]}
                animate={false}
            />
        );
    }
}

export default StarsGraph;
