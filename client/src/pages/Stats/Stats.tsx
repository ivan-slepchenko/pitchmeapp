// @ts-nocheck
import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { ROUTES } from 'routes';
import {
    CenteredContainer,
    FullSizeAbsoluteFlexColumnContainer,
    FullSizeContainer,
    RoundedImage,
    CapitalizedRobotoButton,
} from '../../Styled';
import { StyledResponsiveLine } from './Styled';
import StarsGraph from './StarsGraph';
import OvertimeGraph from './OvertimeGraph';
import UserPreview from './UserPreview';
import { getUserThumbnail } from '../../utils/helpers';

@withRouter
@inject(({ userStore, eventsStore }) => ({ userStore, eventsStore }))
@observer
class Stats extends Component {
    state = {
        starsData: [],
        overtimeData: [],
        feedbackCrafterData: [],
        userToPreview: null,
    };

    componentDidMount() {
        const {
            userStore: { getUserStats },
        } = this.props;
        getUserStats().then(
            (userStats) => {
                this.setState({
                    starsData: userStats.starsData,
                    overtimeData: userStats.overtimeData,
                    feedbackCrafterData: userStats.feedbackCrafterData,
                });
            },
            (e) => {
                console.error(`[Stats::componentDidMount] failed, ${e}`);
            }
        );
    }

    render() {
        const { history } = this.props;
        const { starsData, overtimeData, feedbackCrafterData, userToPreview } = this.state;

        const graphWidth = Math.max(12 * 80, window.innerWidth);
        const reviewers = [];
        for (let i = 0; i < feedbackCrafterData.length; i++) {
            reviewers.push(
                <RoundedImage
                    key={i}
                    width="40px"
                    height="40px"
                    src={getUserThumbnail(feedbackCrafterData[i]._id)}
                    onClick={() => {
                        this.setState({
                            userToPreview: feedbackCrafterData[i],
                        });
                    }}
                />
            );
        }

        return (
            <FullSizeAbsoluteFlexColumnContainer>
                <FullSizeContainer
                    className="p-d-flex p-dir-col p-jc-center"
                    style={{ overflowX: 'auto', overflowY: 'auto' }}
                >
                    {feedbackCrafterData.length >= 3 && (
                        <>
                            <StyledResponsiveLine
                                style={{
                                    height: 'calc(70% - 40px)',
                                    width: `${graphWidth}px`,
                                }}
                            >
                                <StarsGraph data={starsData} graphWidth={graphWidth} />
                            </StyledResponsiveLine>
                            <div
                                className="p-d-flex p-jc-between"
                                style={{
                                    height: '40px',
                                    width: `${graphWidth}px`,
                                    paddingLeft: '28px',
                                    paddingRight: '28px',
                                    marginTop: '5px',
                                    marginBottom: '5px',
                                }}
                            >
                                {reviewers}
                            </div>
                            <div
                                style={{
                                    height: 'calc(30% - 40px)',
                                    width: `${graphWidth}px`,
                                    minHeight: '120px',
                                }}
                            >
                                <OvertimeGraph data={overtimeData} graphWidth={graphWidth} />
                            </div>
                        </>
                    )}
                    {feedbackCrafterData.length < 3 && (
                        <CenteredContainer className="p-as-center">
                            <div>To see stats panel</div>
                            <div>please pitch at least 3 times</div>
                        </CenteredContainer>
                    )}

                    {!!userToPreview && (
                        <UserPreview
                            userToPreview={userToPreview}
                            handleClose={() => {
                                this.setState({ userToPreview: null });
                            }}
                        />
                    )}
                </FullSizeContainer>
                <div className="p-d-flex p-dir-row p-jc-center p-ai-center p-pt-1 p-pb-1">
                    <CapitalizedRobotoButton
                        label="Back"
                        className="p-button-rounded p-button-help p-button-sm p-ml-2 p-mr-1"
                        onClick={() => {
                            history.push({
                                pathname: ROUTES.MIRROR.path,
                            });
                        }}
                    />
                </div>
            </FullSizeAbsoluteFlexColumnContainer>
        );
    }
}

export default Stats;
