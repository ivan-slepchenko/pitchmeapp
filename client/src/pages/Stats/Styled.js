import styled from '@emotion/styled';

export const StyledResponsiveLine = styled.div`
    path {
        &:nth-of-type(6) {
            stroke-width: 2px;
        }
        &:nth-of-type(5) {
            stroke-width: 6px;
        }
        &:nth-of-type(4) {
            stroke-width: 10px;
        }
        &:nth-of-type(3) {
            stroke-width: 14px;
        }
        &:nth-of-type(2) {
            stroke-width: 18px;
        }
        &:nth-of-type(1) {
            stroke-width: 22px;
        }
    }
`;
