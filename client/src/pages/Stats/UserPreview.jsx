import { Component } from 'react';
import { MdClose } from 'react-icons/md';
import {
    BlackBorderedDiv,
    FlexNoWrapRow,
    FullSizeAbsoluteFlexColumnContainer,
    NoWrapRow,
    RoundedImage,
} from '../../Styled';
import FeedbackView from './FeedbackView';
import { getUserThumbnail } from '../../utils/helpers';
import OrientationPattern from '../../components/Layouts/OrientationPattern';
import { LandscapeRatingBlock } from '../Connector/Room/Game/Actions/Styled';

class UserPreview extends Component {
    render() {
        const { userToPreview, handleClose } = this.props;
        return (
            <FullSizeAbsoluteFlexColumnContainer>
                <BlackBorderedDiv
                    className="p-as-center"
                    style={{
                        maxWidth: '650px',
                        minWidth: '300px',
                        width: 'unset',
                        position: 'relative',
                        backgroundColor: 'rgba(0, 0, 0, 0.8)',
                    }}
                >
                    <MdClose
                        className="modal__close"
                        onClick={handleClose}
                        style={{
                            position: 'absolute',
                            right: '15px',
                            top: '15px',
                        }}
                    />
                    <FlexNoWrapRow className="p-mb-3">
                        <RoundedImage
                            width="40px"
                            height="40px"
                            src={getUserThumbnail(userToPreview._id)}
                            className="p-mr-2"
                        />
                        <div
                            className="p-d-flex p-dir-col"
                            style={{
                                position: 'absolute',
                                left: '60px',
                                right: '30px',
                            }}
                        >
                            <NoWrapRow>Project Name: {userToPreview.startupName}</NoWrapRow>
                            <NoWrapRow>By: {userToPreview.name}</NoWrapRow>
                            <NoWrapRow>Email{userToPreview.email}</NoWrapRow>
                        </div>
                    </FlexNoWrapRow>
                    <OrientationPattern
                        landscape={
                            <LandscapeRatingBlock>
                                <FeedbackView feedback={userToPreview} />
                            </LandscapeRatingBlock>
                        }
                        portrait={<FeedbackView feedback={userToPreview} />}
                    />
                </BlackBorderedDiv>
            </FullSizeAbsoluteFlexColumnContainer>
        );
    }
}

export default UserPreview;
