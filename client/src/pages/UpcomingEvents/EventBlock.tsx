import { EventThumbnail, PassOnlyLabel } from './Styled';
import { EVENT_TYPE_FREE, EVENT_TYPE_PASS, UpcomingEvent } from '../../store/EventsStore';
import { CapitalizedRobotoButton, GreyPanel } from '../../Styled';

interface EventBlockProps {
    upcomingEvent: UpcomingEvent
}

function formatStartTime(startTime: string): string {
    return startTime;
}

export const EventBlock = ({ upcomingEvent }:EventBlockProps) => <>
     <div key={upcomingEvent._id} className="p-d-flex p-dir-row p-width-full">
         <EventThumbnail className="p-rounded-4 p-of-contain" src={upcomingEvent.logo}/>
         <div className="p-flex-column p-ml-3">
             <h5>{formatStartTime(upcomingEvent.startTime)}</h5>
             <h3>{upcomingEvent.title}</h3>
             {upcomingEvent.description}
         </div>
     </div>
     <div className="p-d-flex p-dir-row p-width-full p-mt-2">
         {upcomingEvent.type === EVENT_TYPE_FREE && <CapitalizedRobotoButton
             className="p-button-rounded p-button-sm p-button-success p-ml-auto"
             type="submit"
             label="APPLY"
             // onClick={this.nextQuestion}
         />}
         {upcomingEvent.type === EVENT_TYPE_PASS && <PassOnlyLabel className="p-ml-auto" >
             <b>PASS ONLY</b>
         </PassOnlyLabel>}
     </div>
 </>
