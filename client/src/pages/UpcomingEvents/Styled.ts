import styled from '@emotion/styled';
import { colorOrange } from '../../Styled';

export const EventThumbnail = styled.img`
    width: 110px;
    height: 110px;
`;

export const PassOnlyLabel = styled.div`
    color: ${colorOrange};
`;
