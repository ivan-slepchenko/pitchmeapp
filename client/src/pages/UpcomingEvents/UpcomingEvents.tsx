import { useEffect } from 'react';
import { inject, observer } from 'mobx-react';
import { ScrollPanel } from 'primereact/scrollpanel';
import {
    FullSizeAbsoluteFlexColumnContainer,
    CapitalizedRobotoButton,
    GreyPanel,
    FullSizeAbsolute,
} from '../../Styled';
import { EventThumbnail, PassOnlyLabel } from './Styled'
import EventsStore, { EVENT_TYPE_FREE, EVENT_TYPE_PASS } from '../../store/EventsStore';
import { EventBlock } from './EventBlock';

interface EventsProps {
    standalone: boolean,
    eventsStore: EventsStore,
}

const UpcomingEvents = inject(
    'eventsStore'
)(
    observer(
        ({
             eventsStore: { fetchUpcomingEvents, eventPacks }
        }:EventsProps) => {

            useEffect(() => {
                fetchUpcomingEvents().then();
            }, [])

            return (
                <>
                    <FullSizeAbsolute className="p-d-flex p-flex-column p-align-start p-justify-start">
                        <ScrollPanel  style={{flex: 1, overflow: "scroll"}}>
                            <div className="p-p-3 p-d-flex p-flex-column">
                                {eventPacks.map((upcomingEvent) => <>
                                    <GreyPanel className="p-width-full p-p-3 p-mt-1 p-mb-1">
                                        <EventBlock upcomingEvent={upcomingEvent}/>
                                        {upcomingEvent.child && (
                                            <>
                                                <hr className="p-mt-4 p-mb-4"/>
                                                <EventBlock upcomingEvent={upcomingEvent.child} />
                                            </>
                                        )}
                                    </GreyPanel>
                                </>)}
                                {eventPacks.map((upcomingEvent) => <>
                                    <GreyPanel className="p-width-full p-p-3 p-mt-1 p-mb-1">
                                        <EventBlock upcomingEvent={upcomingEvent}/>
                                        {upcomingEvent.child && (
                                            <>
                                                <hr className="p-mt-4 p-mb-4"/>
                                                <EventBlock upcomingEvent={upcomingEvent.child} />
                                            </>
                                        )}
                                    </GreyPanel>
                                </>)}
                                {eventPacks.map((upcomingEvent) => <>
                                    <GreyPanel className="p-width-full p-p-3 p-mt-1 p-mb-1">
                                        <EventBlock upcomingEvent={upcomingEvent}/>
                                        {upcomingEvent.child && (
                                            <>
                                                <hr className="p-mt-4 p-mb-4"/>
                                                <EventBlock upcomingEvent={upcomingEvent.child} />
                                            </>
                                        )}
                                    </GreyPanel>
                                </>)}
                            </div>
                        </ScrollPanel>
                    </FullSizeAbsolute>
                </>
            );
        }
    )
)

export default UpcomingEvents;
