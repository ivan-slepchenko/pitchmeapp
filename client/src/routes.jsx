import LoginPage from 'pages/Auth/LoginSignup';
import AboutPage from 'pages/MyProfile';
import MirrorPage from 'pages/Mirror';
import Connector from 'pages/Connector';
import NotFoundPage from 'pages/NotFound';
import Agreement from 'pages/Auth/Agreement';
import EvilInvestor from './pages/EvilInvestor/EvilInvestor';
import PitchSettings from './pages/PitchSettingsPage/PitchSettings';
import Leaderboard from './pages/Leaderboard/Leaderboard';
import Stats from './pages/Stats';
import Live from './pages/Live/LivePage';
import UpcomingEvents from './pages/UpcomingEvents/UpcomingEvents';

const ROUTES = {
    ROOT: {
        path: '/',
    },
    NO_AUTH_REDIRECT: {
        path: '/login',
    },
    LOGIN: {
        path: '/login',
    },
    PROFILE: {
        path: '/profile',
    },
    MIRROR: {
        path: '/mirror',
    },
    PITCH_SETTINGS: {
        path: '/pitch-settings',
    },
    EVIL_INVESTOR: {
        path: '/evil-investor',
    },
    LEADERBOARD: {
        path: '/leaderboard',
    },
    LEADERBOARD_STANDALONE: {
        path: '/leaderboard_standalone',
    },
    UPCOMING_EVENTS: {
        path: '/upcoming_events',
    },
    STATS: {
        path: '/stats',
    },
    LIVE: {
        path: '/live',
    },
    ROOM: {
        path: '/room',
    },
    AGREEMENT: {
        path: '/agreement',
    },
};

// TODO: to Object View
const routes = [
    {
        path: ROUTES.ROOT.path,
        exact: true,
        component: (props) => <LoginPage {...props} />,
    },
    {
        path: ROUTES.AGREEMENT.path,
        component: (props) => <Agreement {...props} />,
    },
    {
        path: ROUTES.EVIL_INVESTOR.path,
        component: (props) => <EvilInvestor {...props} />,
    },
    {
        path: ROUTES.LEADERBOARD.path,
        component: (props) => <Leaderboard {...props} />,
    },
    {
        path: ROUTES.LEADERBOARD_STANDALONE.path,
        component: (props) => <Leaderboard standalone {...props} />,
    },
    {
        path: ROUTES.UPCOMING_EVENTS.path,
        component: (props) => <UpcomingEvents {...props} />,
    },
    {
        path: ROUTES.LOGIN.path,
        component: (props) => <LoginPage {...props} />,
    },
    {
        path: ROUTES.LIVE.path,
        component: (props) => <Live {...props} />,
    },
    /**
     * PRIVATE ROUTES:
     */
    {
        isPrivate: true,
        path: ROUTES.MIRROR.path,
        component: (props) => <MirrorPage {...props} />,
    },
    {
        isPrivate: true,
        path: ROUTES.PITCH_SETTINGS.path,
        component: (props) => <PitchSettings {...props} />,
    },
    {
        isPrivate: true,
        path: ROUTES.ROOM.path,
        component: (props) => <Connector {...props} />,
    },
    {
        isPrivate: true,
        path: ROUTES.PROFILE.path,
        component: (props) => <AboutPage {...props} />,
    },
    {
        isPrivate: true,
        path: ROUTES.STATS.path,
        component: (props) => <Stats {...props} />,
    },
    {
        path: '*',
        component: (props) => <NotFoundPage {...props} />,
    },
];

export { ROUTES };

export default routes;
