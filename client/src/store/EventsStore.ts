// @ts-nocheck
import { action, observable } from 'mobx';

import moment from 'moment';
import api from '../utils/api';
import ls from '../utils/localStorage';
import { FEEDBACK } from '../config';
import BrandingURLs from '../utils/branding/BrandingURLs';

export const EVENT_TIME_STRING_FORMAT = 'YYYY-MM-DD:HH:mm';
export const EVENT_TYPE_FREE = 'FREE';
export const EVENT_TYPE_PASS = 'PASS';

export interface UpcomingEvent {
    _id: string,
    brandingCode: string,
    title: string,
    description: string,
    type: string,
    startTime: string,
    endTime: string,
    logo: string,
    hostEventId: string,
    child: UpcomingEvent
}

class EventsStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    upcomingEvents:Array<UpcomingEvent> = [];

    @observable eventPacks:Array<UpcomingEvent> = [];

    @observable event = null;

    @observable nowTime = 0;

    @observable eventLogo = null;

    @observable eventSquaredLogo = null;

    @observable eventQuestions = FEEDBACK;

    @observable featureFlagsFetched = false;

    @observable featureFlags = {
        BOARDING_SLIDES: {
            enabled: true
        },
        PITCH_DECKS: {
            enabled: true
        },
        NO_DECKS_DEFAULT_TIME: {
            enabled: false,
            values: {
                imexpert: "I'm YC expert"
            }
        },
        EVENT_STATUS_DIALOG: {
            enabled: true
        },
        CUSTOM_QUESTIONS: {
            enabled: false
        },
        STATS: {
            enabled: true
        },
        FEEDBACK_TIMER: {
            enabled: false,
            values: {
                time: 150000
            }
        },
        HARD_PITCHING_LIMIT: {
            enabled: false
        }
    };

    get eventStatus() {
        if(this.event == null) {
            return 'undefined';
        }
        const startTimeString = this.event.startTime;
        const endTimeString = this.event.endTime;
        
        if(startTimeString == null || endTimeString == null) {
            return 'ongoing';
        }

        const startUTCDate = moment.utc(startTimeString, EVENT_TIME_STRING_FORMAT).toDate();
        const endUTCDate = moment.utc(endTimeString, EVENT_TIME_STRING_FORMAT).toDate();
        switch (true) {
            case startUTCDate.getTime() > this.nowTime:
                return 'not_started';
            case startUTCDate.getTime() < this.nowTime && endUTCDate.getTime() > this.nowTime:
                return 'ongoing';
            default:
                return 'done';
        }
    }

    init() {
        this.fetchEvent().then(async ({ success }) => {
            if (success) {
                await this.fetchBrandFeatures();
                this.featureFlagsFetched = true;
                if (this.featureFlags.NO_DECKS_DEFAULT_TIME.enabled) {
                    const patchedUserData = this.rootStore.userStore.user;
                    patchedUserData.pitches = [
                        {
                            name: this.featureFlags.NO_DECKS_DEFAULT_TIME.values.name,
                            time: this.featureFlags.NO_DECKS_DEFAULT_TIME.values.time,
                        },
                    ];
                    patchedUserData.chosenPitchIndex = 0;
                    this.rootStore.userStore.setUserData(patchedUserData);
                }
            } else {
                console.log('NO BRANDING');
            }
        });
        setInterval(() => {
            this.nowTime = Date.now();
        }, 10);
    }

    @action.bound
    async fetchBrandFeatures() {
        const { brandingCode } = this.event;
        try {
            return await api
                .get(brandingCode ? `/brand/features?brandingCode=${brandingCode}` : '/brand/features')
                .then((response) => {
                    const { data, success } = response;
                    if (success) {
                        this.featureFlags = data;
                        if (!this.featureFlags.BOARDING_SLIDES.enabled) {
                            ls.set('isOnBoarded', true);
                        }

                        if (this.featureFlags.CUSTOM_QUESTIONS.enabled) {
                            this.eventQuestions = {...this.featureFlags.CUSTOM_QUESTIONS.values};
                        }
                    }
                    return response;
                });
        } catch (error) {
            console.log(`BRAND FEATURES NOT FETCHED BECAUSE OF ERROR ${error}`);
        }
    }

    @action.bound
    async fetchEvent() {
        const {inviteCode} = this.rootStore.userStore;
        try {
            // this request should default some scheduled event, or default event
            const response =  await api.get(inviteCode ? `/events/next?invite_code=${inviteCode}` : '/events/next');
            const { data, success } = response;
            if (success) {
                this.event = data;
                const { brandingCode } = data;
                this.eventSquaredLogo = BrandingURLs.getSquaredLogo(brandingCode);
                this.eventLogo = BrandingURLs.getLogo(brandingCode);
            }
            return response;
        } catch (error) {
            console.log(`NEXT EVENT IS NOT FETCHED BECAUSE OF ERROR ${error}`);
        }
    }

    @action.bound
    async  fetchUpcomingEvents() {
        try {
            const response = await api.get('/events/upcoming');
            const { data, success } = response;
            if (success) {
                this.upcomingEvents = data;
                this.upcomingEvents.forEach((event) => {
                    event.logo = BrandingURLs.getSquaredLogo(event.brandingCode);
                })
                this.packCoupledAndReSort();
            }
        } catch (error) {
            console.log(`NEXT EVENT IS NOT FETCHED BECAUSE OF ERROR ${error}`);
        }
    }

    packCoupledAndReSort(){
        const couplesSetById = {};

        this.upcomingEvents.forEach(event=>{
            couplesSetById[event._id] = event;
        })
        // eslint-disable-next-line guard-for-in
        Object.keys(couplesSetById).forEach((id)=>{
            const event = couplesSetById[id];
            if(event.hostEventId != null){
                couplesSetById[event.hostEventId].child = event;
                delete couplesSetById[event._id];
            }
        });

        const couplesArray = Object.values(couplesSetById);

        couplesArray.sort((a:UpcomingEvent, b:UpcomingEvent) => {
            const aStartTimeInDate = moment.utc(a.startTime, EVENT_TIME_STRING_FORMAT).toDate();
            const bStartTimeInDate = moment.utc(a.startTime, EVENT_TIME_STRING_FORMAT).toDate();
            return aStartTimeInDate.getTime() > bStartTimeInDate.getTime();
        })
        this.eventPacks = couplesArray;
    }
}

export default EventsStore;
