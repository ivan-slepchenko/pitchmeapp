// @ts-nocheck
import { action, observable } from 'mobx';
import uuid from 'uuid/v4';

// Docs: https://iamhosseindhv.com/notistack

class NotifyStore {
    @observable eventsQueue = [];

    @observable existedSubscription = null;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    /**
     * Add a new snack to queue
     * @param {Object} note
     * @param {string} note.message
     * @param {Object} [note.options]
     * @param {('success'|'warning'|'info'|'error')} note.options.variant
     * @param {Function} note.options.action - action buttons
     */
    @action.bound
    show({ key = uuid(), message, options }) {
        if (this.eventsQueue.some((it) => it.key === key)) {
            return;
        }

        this.eventsQueue.push({
            type: 'show',
            key,
            payload: {
                message,
                options,
            },
        });
    }

    @action.bound
    close(key) {
        this.eventsQueue.push({
            type: 'close',
            key,
        });
    }

    @action.bound
    closeAll() {
        this.eventsQueue.push({ type: 'close_all', key: uuid() });
    }

    /**
     *
     * METHODS BELOW ONLY FOR SERVICE!!!
     *
     */

    @action.bound
    utilizeQueue(key) {
        if (key) {
            this.eventsQueue = this.eventsQueue.filter((it) => it.key !== key);
        } else {
            this.eventsQueue = [];
        }
    }

    @action.bound
    setShown(key, message) {
        const event = this.eventsQueue.find((it) => it.key === key);
        if (!event) {
            console.log(`NOTIFICATION NOT FOUND FOR KEY ${key} MESSAGE ${message}`);
        } else {
            event.shown = true;
        }
    }

    get user() {
        return this.rootStore.userStore.user;
    }
}

export default NotifyStore;
