// @ts-nocheck
import { action, observable } from 'mobx';
import ls from 'utils/localStorage';
import { bind } from 'decko';

export const NAME_MAX_LENGTH = 50;
export const EMAIL_MAX_LENGTH = 50;

class ProfilesStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    get isAuth() {
        return this.rootStore.userStore.isAuth;
    }

    get notify() {
        return this.rootStore.notifyStore.show;
    }

    get closeNotifications() {
        return this.rootStore.notifyStore.close;
    }

    @observable challengedUsers = ls.get('challengedUsers') || [];

    @observable favoriteUsers = ls.get('favoriteUsers') || [];

    @action.bound
    toggleChallenged(id) {
        if (this.isAuth) {
            const { challengedUsers } = this;
            if (this.isChallengedUser(id)) {
                challengedUsers.splice(challengedUsers.indexOf(id), 1);
            } else {
                this.notify({
                    message: 'Get challenged back and start the match!',
                    options: {
                        variant: 'info',
                    },
                });
                challengedUsers.push(id);
            }
            ls.set('challengedUsers', challengedUsers);
        } else {
            this.authentificateForAction('challenge');
        }
    }

    @action.bound
    toggleFavorite(id) {
        if (this.isAuth) {
            const { favoriteUsers } = this;
            if (this.isFavoriteUser(id)) {
                favoriteUsers.splice(favoriteUsers.indexOf(id), 1);
            } else {
                this.notify({
                    message: 'Be aware when a user goes online!',
                    options: {
                        variant: 'info',
                    },
                });
                favoriteUsers.push(id);
            }

            ls.set('favoriteUsers', favoriteUsers);
        } else {
            this.authentificateForAction('favorite');
        }
    }

    @bind
    isFavoriteUser(id) {
        return this.isAuth && this.favoriteUsers.includes(id);
    }

    @bind
    isChallengedUser(id) {
        return this.isAuth && this.challengedUsers.includes(id);
    }
}

export default ProfilesStore;
