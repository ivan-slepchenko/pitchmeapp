// @ts-nocheck
import { observable, action, set, computed, autorun } from 'mobx';
import { bind } from 'decko';
import socketIOClient from 'socket.io-client';
import { API_URL } from 'utils/constants';
import ls from 'utils/localStorage';
import {
    gaRoomJoin,
    gaRoomLeave,
    gaRoomDisconnect,
    gaNextPage,
    gaPreviousPage,
    gaPitchTimeOut,
    gaFeedbackTimeOut,
} from 'utils/ga';
import {
    CONNECT_USER,
    ROOM_CONNECTION,
    LEAVE_ROOM,
    MORE_THAN_ONE_ACTIVE_TAB,
    DISCONNECT,
    NEW_CONNECTION, TIMER_TIMEOUT, CHANGE_USERS_COUNT,
} from 'utils/constants/events/common';
import { LS_KEYS } from 'utils/constants/lsKeys';
import { ROUTES } from 'routes';
import { CRAFTING_FEEDBACK, INTRO, PITCH, REVIEWING_FEEDBACK } from '../utils/constants/pitchFlow';
import { EVENTS } from '../utils/constants/events';
import { PRESENTATION } from '../utils/constants/events/presentation';

let feedbackRequestTimeoutId = -1;
const defaultSocket = null;
const defaultTiming = {
    startRoom: 0,
    startSearch: 0,
    startPitch: 0,
    endPitch: 0,
    endRoom: 0,
};
const defaultUsersInRoomData = {};
const defaultRoom = {
    id: '',
    companion: null,
    isVideoConnectionEstablished: false,
};
const defaultReplayPitchIsMyTurn = {
    isPDFPageLoaded: false,
    state: INTRO,
    isMyTurn: true,
    isInReplayMode: true,
    isFeedbackOnDeck: false,
    timeToRequestTheFeedback: false,
    page: 1,
};
const defaultReplayPitchIsNotMyTurn = {
    isPDFPageLoaded: false,
    state: INTRO,
    isMyTurn: false,
    isInReplayMode: true,
    isFeedbackOnDeck: false,
    timeToRequestTheFeedback: false,
    page: 1,
};
const defaultPitch = {
    state: undefined,
    isMyTurn: undefined,
    isInReplayMode: false,
    isFeedbackOnDeck: false,
    timeToRequestTheFeedback: false,
    isPDFPageLoaded: false,
    page: 1,
};
const defaultNotifier = {
    value: null,
};
const defaultFeedback = {
    a: 0,
    b: 0,
    c: 0,
    d: 0,
    e: 0,
    f: 0,
    text: '',
};

// for react-ga purpose
let displayConnectingModeTimeout = null;
let roomWaiterAutorunDisposer = null;

class RoomStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    @observable usersInRoomData = defaultUsersInRoomData;

    @observable timing = defaultTiming;

    @observable socket = defaultSocket;

    @observable room = defaultRoom;

    @observable pitch = defaultPitch;

    @observable feedback = defaultFeedback;

    @observable notifier = defaultNotifier; // TODO: WTF IS THAT?

    @observable isInOpponentPreviewMode = false;

    get $routing() {
        return this.rootStore.routingStore;
    }

    get $user() {
        return this.rootStore.userStore.user;
    }

    get notifyStore() {
        return this.rootStore.notifyStore;
    }

    get eventsStore() {
        return this.rootStore.eventsStore;
    }

    get activePitch() {
        const { chosenPitch: myChosenPitch } = this.rootStore.userStore;
        const {
            companion: { chosenPitch: companionChosenPitch },
        } = this.room;
        return this.pitch.isMyTurn ? myChosenPitch : companionChosenPitch;
    }

    get hasPitch() {
        const { chosenPitch } = this.rootStore.userStore;
        return !!chosenPitch;
    }

    get isPitchDeckExist() {
        const { featureFlags } = this.eventsStore;
        return this.activePitch != null && this.activePitch.fileUUID != null && featureFlags.PITCH_DECKS.enabled;
    }

    get isPitchDeckVisible() {
        const { featureFlags } = this.eventsStore;
        const { isFeedbackOnDeck, state } = this.pitch;
        return (
            this.isPitchDeckExist &&
            (state === PITCH || (state === CRAFTING_FEEDBACK && isFeedbackOnDeck)) &&
            featureFlags.PITCH_DECKS.enabled
        );
    }

    get pitchOvertime() {
        const pitchLength = this.timing.endPitch - this.timing.startPitch;
        return Math.round((pitchLength - this.activePitch.time) / 1000);
    }

    get replayIsNeeded() {
        return !(this.pitch.isInReplayMode || this.room.companion.chosenPitch == null)
    }

    @bind
    nextPage() {
        gaNextPage();

        let newPage = this.pitch.page + 1;

        if (newPage > this.activePitch.pages) {
            newPage = 1;
        }
        set(this.pitch, {
            isPDFPageLoaded: false,
            page: newPage,
        });
        this.sendPresentationEvent(EVENTS.PRESENTATION.CHANGE_PAGE, { page: newPage });
    }

    @bind
    prevPage() {
        gaPreviousPage();

        let newPage = this.pitch.page - 1;

        if (newPage === 0) {
            newPage = this.activePitch.pages;
        }

        set(this.pitch, {
            isPDFPageLoaded: false,
            page: newPage,
        });
        this.sendPresentationEvent(EVENTS.PRESENTATION.CHANGE_PAGE, { page: newPage });
    }

    @action.bound
    setVideoConnectionIsEstablished(value) {
        set(this.room, { isVideoConnectionEstablished: value });
    }

    @bind
    setPDFPageLoaded() {
        set(this.pitch, { isPDFPageLoaded: true });
    }

    @bind
    setIsFeedbackOnDeck(value, isNotifyingNeeded) {
        set(this.pitch, { isFeedbackOnDeck: value });
        if (isNotifyingNeeded) {
            this.emit(EVENTS.PRESENTATION.FEEDBACK_ON_DECK, value);
        }
    }

    startPitch() {
        set(this.timing, { startPitch: Date.now() });
        set(this.pitch, { started: true });
    }

    @bind
    getWaitingTime() {
        return new Date().getTime() - this.timing.startSearch;
    }

    @computed
    get prevRoomTime() {
        const { startRoom, endRoom } = this.timing;

        return endRoom - startRoom;
    }

    displayConnectingModeTimeout = false;

    @action.bound
    waitForRoomAndInitSocket() {
        roomWaiterAutorunDisposer = autorun(() => {
            const inRoom = Boolean(this.room.id);
            if (!inRoom) {
                this.initSocketHandler();
            }
        });
    }

    @action.bound
    destroySocket() {
        if (roomWaiterAutorunDisposer) {
            roomWaiterAutorunDisposer();
            this.disconnect();
        }
    }

    @action.bound
    initSocketHandler() {
        if (this.socket !== defaultSocket) {
            this.socket.disconnect();
        }
        this.socket = socketIOClient(API_URL, { transports: ['websocket'] });

        this.socket.on(NEW_CONNECTION, this.startSearching);
        this.socket.on(DISCONNECT, this.handleDisconnect);
        this.socket.on(ROOM_CONNECTION, this.joinRoom);
        this.socket.on(LEAVE_ROOM, this.leaveRoom);
        this.socket.on(EVENTS.PRESENTATION.LOADED, this.setPDFLoadedOnOpponentSide);
        this.socket.on(EVENTS.PRESENTATION.FEEDBACK_ON_DECK, (payroll) => {
            this.setIsFeedbackOnDeck(payroll, false);
        });

        this.socket.on(MORE_THAN_ONE_ACTIVE_TAB, () => {
            this.socket.disconnect();
            // eslint-disable-next-line no-alert
            alert('You have opened more than one active tab');
            window.location.href = '/';
        });

        this.socket.on(CHANGE_USERS_COUNT, (usersInRoomData)=>{
            this.usersInRoomData = usersInRoomData;
        })

        this.socket.on(PRESENTATION.CHANGE_PAGE, ({ page }) => {
            set(this.pitch, { isPDFPageLoaded: false, page });
        });

        this.socket.on(EVENTS.PITCH.STATE, ({ state, isInReplayMode, feedback }) => {
            set(this.pitch, { state });
            if (state === INTRO && isInReplayMode) {
                set(this.pitch, defaultReplayPitchIsMyTurn);
                set(this.timing, defaultTiming);
                set(this.feedback, defaultFeedback);
            } else if (state === PITCH) {
                this.startPitch();
            }
            if (state === REVIEWING_FEEDBACK) {
                set(this.feedback, feedback);
                set(this.pitch, { isFeedbackOnDeck: false });
            }
            if (state === CRAFTING_FEEDBACK) {
                this.timing.endPitch = Date.now();
            }
        });
    }

    @action.bound
    startSearching() {
        const { userCamAccess, inviteCode } = this.rootStore.userStore;
        set(this.timing, {
            startSearch: Date.now(),
        });
        this.socket.emit(CONNECT_USER, {
            user: this.$user,
            global: this.room.global,
            inviteCode,
            isTester: ls.get(LS_KEYS.TESTER),
            userCamAccess,
        });
    }

    @action.bound
    handleDisconnect() {
        if (this.timing.endRoom > 0) {
            // user already leaved the room
            return;
        }
        gaRoomDisconnect(new Date().getTime() - this.timing.startSearch);
        this.notifyStore.show({
            message: 'Sorry, You were disconnected because of network issues.',
            options: {
                variant: 'warn',
            },
        });
        this.$routing.push(ROUTES.MIRROR.path);
    }

    @action.bound
    joinRoom({ roomId, companion, isMyTurn }) {
        if (companion.chosenPitchIndex === -1 && this.$user.chosenPitchIndex === -1) {
            alert('System Failure, 0 performers.');
        }
        gaRoomJoin(this.getWaitingTime());

        if (this.eventsStore.featureFlags.NO_DECKS_DEFAULT_TIME.enabled) {
            // TODO: THIS SHITTY BLOCK APPEARS TWO TIMES CLEANUP IF WE WILL BE IN TIME WITH TECH
            companion.pitches = [
                {
                    name: this.eventsStore.featureFlags.NO_DECKS_DEFAULT_TIME.values.name,
                    time: this.eventsStore.featureFlags.NO_DECKS_DEFAULT_TIME.values.time,
                },
            ];
            companion.chosenPitchIndex = 0;
        }

        this.startOpponentPreviewMode();

        set(this.timing, {
            startRoom: Date.now(),
        });
        set(this.room, {
            id: roomId,
            companion,
        });
        set(this.pitch, {
            state: INTRO,
            isMyTurn,
        });
    }

    @action.bound
    startOpponentPreviewMode() {
        this.isInOpponentPreviewMode = true;

        if (displayConnectingModeTimeout != null) {
            clearTimeout(displayConnectingModeTimeout);
        }
        displayConnectingModeTimeout = setTimeout(() => {
            displayConnectingModeTimeout = null;
            this.isInOpponentPreviewMode = false;
        }, 4000);
    }

    @action.bound
    switchRoles() {
        this.setPitchState(INTRO, true);
        this.feedback = defaultFeedback;
        this.pitch = defaultReplayPitchIsNotMyTurn;
        set(this.timing, { startPitch: 0, endPitch: 0 });
    }

    @action.bound
    leaveRoom(reason) {
        gaRoomLeave(new Date().getTime() - this.timing.startRoom, reason);

        this.stopFeedbackTimer();
        this.stopHardPitchingTimer();

        set(this.timing, {
            endRoom: Date.now(),
        });

        // close all notification that belongs to the room and show "leave" notification
        this.notifyStore.closeAll();

        switch (reason) {
            case DISCONNECT:
                this.notifyStore.show({
                    message: 'Opponent lost a connection to the room',
                    options: {
                        variant: 'info',
                    },
                });
                break;
            case LEAVE_ROOM:
                this.notifyStore.show({
                    message: 'Opponent left the room',
                    options: {
                        variant: 'info',
                    },
                });
                break;
            case TIMER_TIMEOUT:
                this.notifyStore.show({
                    message: 'Feedback time is over',
                    options: {
                        variant: 'warn',
                    },
                });
                break;
            default:
                console.log('You left the room by your decision');
                break;
        }

        this.rootStore.userStatsStore.increaseChatsPlayed();

        if(this.socket) {
            this.socket.emit(LEAVE_ROOM);
        }
        this.handleNotifier(null);
        this.disconnect();
    }

    @action.bound
    disconnect() {
        clearTimeout(displayConnectingModeTimeout);
        clearTimeout(feedbackRequestTimeoutId);
        set(this.room, defaultRoom);
        set(this.timing, defaultTiming);
        set(this.feedback, defaultFeedback);
        set(this.pitch, defaultPitch);

        if (this.socket) {
            // socket may be destroyed already by leaveRoom, and then it will happen again on Connector.componentWillIUnmount
            this.socket.removeListener(DISCONNECT, this.handleDisconnect);
            this.socket.disconnect();
        }
        this.socket = defaultSocket;
    }

    @action.bound
    companionDid(event, payload) {
        const { game } = payload;
        switch (event) {
            default:
        }
    }

    /**
     * *******************
     * ** PITCH SECTION **
     * *******************
     */

    @action.bound
    sendPresentationEvent(event, payload = null) {
        if (this.socket) {
            switch (event) {
                case EVENTS.PRESENTATION.CHANGE_PAGE: {
                    this.emit(event, payload);
                    set(this.pitch, {
                        page: payload.page,
                    });
                    break;
                }

                default:
                    console.warn('Undefined presentation event:', event);
            }
        }
    }

    feedbackTimer = null;

    hardPitchingTimer = null;

    @action.bound
    startFeedbackTimer() {
        if(this.eventsStore.featureFlags.FEEDBACK_TIMER.enabled){
            this.stopFeedbackTimer();
            if(this.pitch.isMyTurn) {
                this.feedbackTimer = setTimeout(()=>{
                    if(this.replayIsNeeded){
                        this.switchRoles();
                    } else {
                        this.leaveRoom(TIMER_TIMEOUT);
                    }
                    gaFeedbackTimeOut();
                },this.eventsStore.featureFlags.FEEDBACK_TIMER.values.time);
            }
        }
    }

    @action.bound
    stopFeedbackTimer() {
        if(this.eventsStore.featureFlags.FEEDBACK_TIMER.enabled){
            clearTimeout(this.feedbackTimer);
            this.feedbackTimer = null;
        }
    }

    @action.bound
    startHardPitchingTimer() {
        if(this.eventsStore.featureFlags.HARD_PITCHING_LIMIT.enabled){
            this.stopHardPitchingTimer();
            if(this.pitch.isMyTurn) {
                this.hardPitchingTimer = setTimeout(()=>{
                    this.notifyStore.show({
                        message: 'Pitching time is over',
                        options: {
                            variant: 'warn',
                        },
                    });
                    this.setPitchState(CRAFTING_FEEDBACK);
                    this.stopHardPitchingTimer();
                    gaPitchTimeOut();
                },this.activePitch.time);
            }
        }
    }

    @action.bound
    stopHardPitchingTimer() {
        if(this.eventsStore.featureFlags.HARD_PITCHING_LIMIT.enabled){
            clearTimeout(this.hardPitchingTimer);
            this.hardPitchingTimer = null;
        }
    }

    @action.bound
    setPitchState(pitchState, isInReplayMode) {
        if (isInReplayMode) {
            set(this.pitch, { state: pitchState, isInReplayMode });
        } else {
            set(this.pitch, { state: pitchState });
        }
        if (this.pitch.state === CRAFTING_FEEDBACK) {
            this.timing.endPitch = Date.now();
            this.stopHardPitchingTimer();
            this.startFeedbackTimer();
        }

        if (this.pitch.state === PITCH) {
            this.startHardPitchingTimer();
        }

        if (pitchState === REVIEWING_FEEDBACK) {
            set(this.pitch, { isFeedbackOnDeck: false });
            this.emit(EVENTS.PITCH.STATE, {
                state: pitchState,
                feedback: this.feedback,
                overtime: this.pitchOvertime,
                isInReplayMode,
            });
        } else {
            this.emit(EVENTS.PITCH.STATE, { state: pitchState, isInReplayMode });
        }

        if (pitchState === PITCH) {
            this.startPitch();
            feedbackRequestTimeoutId = setTimeout(() => {
                set(this.pitch, { timeToRequestTheFeedback: true });
            }, 15000);
        }
        if (pitchState === CRAFTING_FEEDBACK) {
            set(this.pitch, { timeToRequestTheFeedback: false });
        }
    }

    /**
     * *********************
     * ** SERVICE SECTION **
     * *********************
     */

    @bind
    emit(event, payload) {
        this.socket.emit(event, payload);
    }

    @action.bound
    handleNotifier(value) {
        set(this.notifier, { value });
    }
}

export default RoomStore;
