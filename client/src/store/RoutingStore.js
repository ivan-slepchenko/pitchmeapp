import { createBrowserHistory } from 'history';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';
import { gaPageView } from '../utils/ga';

function initRoutingStore() {
    const routingStore = new RouterStore();
    const browserHistory = createBrowserHistory();
    const history = syncHistoryWithStore(browserHistory, routingStore);

    history.subscribe((location) => {
        const { pathname } = location;
        gaPageView(pathname);
    });

    return routingStore;
}

export { initRoutingStore };
