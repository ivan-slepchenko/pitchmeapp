// @ts-nocheck
import { action, observable } from 'mobx';
import api from '../utils/api';
import EventsStore from './EventsStore';

class StatsStore {
    @observable eventLeaderboardData = [];

    @observable brandLeaderboardData = [];

    rootStore = null;

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    get eventsStore(){
        return this.rootStore.eventsStore;
    }

    @action.bound
    async fetchLeaderboard() {
        const { event } = this.eventsStore;

        const response = await api.post(`/stats/leaderboard`, event);

        this.eventLeaderboardData = Array.from(response.data.eventLeaderboardData);
        this.brandLeaderboardData = Array.from(response.data.brandLeaderboardData);
    }
}

export default StatsStore;
