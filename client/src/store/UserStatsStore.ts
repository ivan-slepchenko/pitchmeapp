// @ts-nocheck
import { observable, action, when } from 'mobx';

import api from 'utils/api';

class UserStatsStore {
    constructor(rootStore) {
        this.rootStore = rootStore;

        when(() => this.isUserAuth, this.fetchStats);
    }

    // TODO: group it to stats obj
    @observable chatsPlayed = 0;

    get notifyStore() {
        return this.rootStore.notifyStore;
    }

    // TODO: if user is not logged We can't use this Store
    get isUserAuth() {
        const { user, isAuth } = this.rootStore.userStore;

        return isAuth && user.id;
    }

    @action.bound
    async fetchStats() {
        const { data } = await api.get('/user/stats');
        if (data) {
            this.chatsPlayed = data.chatsPlayed;
        }
    }

    @action.bound
    async increaseChatsPlayed() {
        if (!this.isUserAuth) {
            return false;
        }

        const result = await api.patch('/user/stats', { field: 'chatsPlayed', operator: 'inc', value: 1 });
        if (result) {
            const { success } = result;
            if (success) {
                this.chatsPlayed += 1;
            }
            return success;
        }

        return false;
    }
}

export default UserStatsStore;
