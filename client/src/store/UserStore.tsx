// @ts-nocheck
import { observable, action, set, when, computed } from 'mobx';

import { ROUTES } from 'routes';
import ls from 'utils/localStorage';
import { LS_KEYS } from 'utils/constants/lsKeys';
import api from 'utils/api';
import { bind } from 'decko';
import { getURLParam } from 'utils/helpers';
import { FEEDBACK } from '../config';
import { GET_PARAMS } from '../utils/constants/getParams';
import { gaRegisterUser } from '../utils/ga';

const IS_ONBOARDED = 'is_onboarded';

const ONE_MIN = 1000 * 60;
export const PITCH_TIMES = [
    { value: ONE_MIN * 0.5, title: '30 sec' },
    { value: ONE_MIN * 1, title: '1 min' },
    { value: ONE_MIN * 3, title: '3 min' },
    { value: ONE_MIN * 5, title: '5 min' },
];

const NEW_PITCH = {
    id: -1,
    name: '',
    time: PITCH_TIMES[2].value,
    file: null,
    originalName: null,
};

class UserStore {
    constructor(rootStore) {
        this.rootStore = rootStore;
        // for technical reasons INVITE_CODE is always required.
        // basically it is because users refreshing browser, and loose invite code
        // so if user enters the site once, with the invite code - it is stored in local storage for later.
        // to override - you can only use another invite code

        this.inviteCode = getURLParam(GET_PARAMS.INVITE_CODE);
        if (this.inviteCode !== null) {
            ls.set(LS_KEYS.INVITE_CODE, this.inviteCode);
        } else {
            this.inviteCode = ls.get(LS_KEYS.INVITE_CODE, this.inviteCode);
        }
    }

    @observable inviteCode = null;

    @observable isAuth = ls.get(LS_KEYS.USERTOKEN);

    @observable user = {
        id: '',
        name: '',
        startupName: '',
        info: '',
        pitches: [],
        email: '',
        contactEmail: '',
        isNotificationObserver: false,
        chosenPitchIndex: 0,
        twilioJWTToken: null,
    };

    @observable inUploadProcess = false;

    @observable pitchToEdit = NEW_PITCH;

    @observable userCamAccess = false;

    @observable userMicAccess = false;

    @observable mediaStatus = false;

    getIsOnboarded(key) {
        const value = ls.get(IS_ONBOARDED + key);
        ls.set(IS_ONBOARDED + key, true);
        return !!value;
    }

    get notifyStore() {
        return this.rootStore.notifyStore;
    }

    get eventsStore() {
        return this.rootStore.eventsStore;
    }

    @computed
    get chosenPitch() {
        const { pitches, chosenPitchIndex } = this.user;

        if (pitches.length === 0) {
            return null;
        }

        return pitches[chosenPitchIndex];
    }

    init() {
        this.checkSocialLogin();

        this.fetchUserDataFromLocalStorage();
        when(() => this.isAuth, this.fetchUserFromBackend);
    }

    @bind
    logout(){
        ls.clear();
        sessionStorage.clear();
        window.location.href = ROUTES.LOGIN.path;
    }

    @bind
    getUserStats() {
        return api.get(`/stats/personal?userId=${this.user.id}`).then((response) => {
            const result = {
                starsData: [
                    {
                        id: FEEDBACK.a.label,
                        color: 'hsl(8, 70%, 50%)',
                        data: [],
                    },
                    {
                        id: FEEDBACK.b.label,
                        color: 'hsl(352, 70%, 50%)',
                        data: [],
                    },
                    {
                        id: FEEDBACK.c.label,
                        color: 'hsl(21, 70%, 50%)',
                        data: [],
                    },
                    {
                        id: FEEDBACK.d.label,
                        color: 'hsl(336, 70%, 50%)',
                        data: [],
                    },
                    {
                        id: FEEDBACK.e.label,
                        color: 'hsl(23, 70%, 50%)',
                        data: [],
                    },
                    {
                        id: FEEDBACK.f.label,
                        color: 'hsl(2, 70%, 50%)',
                        data: [],
                    },
                ],
                overtimeData: [
                    {
                        id: 'overtime',
                        color: 'hsl(8, 70%, 50%)',
                        data: [],
                    },
                ],
                feedbackCrafterData: [],
            };
            if (response.success) {
                for (let index = 0; index < response.data.length; index++) {
                    const entry = response.data[index];
                    result.starsData[0].data.push({ x: index, y: entry.a });
                    result.starsData[1].data.push({ x: index, y: entry.b });
                    result.starsData[2].data.push({ x: index, y: entry.c });
                    result.starsData[3].data.push({ x: index, y: entry.d });
                    result.starsData[4].data.push({ x: index, y: entry.e });
                    result.starsData[5].data.push({ x: index, y: entry.f });
                    result.overtimeData[0].data.push({ x: index, y: entry.overtime });
                    result.feedbackCrafterData.push({
                        ...entry.companionData[0],
                        a: entry.a,
                        b: entry.b,
                        c: entry.c,
                        d: entry.d,
                        e: entry.e,
                        f: entry.f,
                        overtime: entry.overtime,
                    });
                }
            } else {
                console.error('[RoomStore::getUsersStats] response reports failure');
            }
            return result;
        });
    }

    @action.bound
    async fetchUserFromBackend() {
        try {
            const { data: myUserData } = await api.get('/user/me');
            this.pitchToEdit = NEW_PITCH;
            this.setUserData(myUserData);
        } catch (error) {
            ls.set(LS_KEYS.USER_DATA, null);
            ls.set(LS_KEYS.USERTOKEN, null);
            sessionStorage.clear();
            window.location.href = ROUTES.LOGIN.path;
        }
    }

    @action.bound
    changeUserField(field, value) {
        set(this.user, { [field]: value });
        this.saveUser();
    }

    @action.bound
    async saveUser() {
        const { success } = await api.patch('/user/me', this.user);
        return success;
    }

    @action.bound
    async signup(data) {
        const { success, token, message } = await api.post('/auth/sign-up', data);

        if (success) {
            ls.set(LS_KEYS.USERTOKEN, token);
            ls.set(LS_KEYS.USER_SIGN_UP_DATE, Date.now());
            window.location.href = ROUTES.PROFILE.path;
        } else {
            this.notifyStore.show({
                message,
                options: {
                    variant: 'error',
                },
            });
        }
    }

    @action.bound
    async login(data) {
        const { success, token, message } = await api.post('/auth/log-in', data);

        if (success) {
            ls.set(LS_KEYS.USERTOKEN, token);
            window.location.href = ROUTES.PROFILE.path;
        } else {
            this.notifyStore.show({
                message,
                options: {
                    variant: 'error',
                },
            });
        }
    }

    @action
    checkSocialLogin() {
        const FB_POSTFIX = '#_=_';
        let token = getURLParam(LS_KEYS.USERTOKEN);

        if (token) {
            /**
             * fb add postfix so we should delete it
             */
            if (token.includes(FB_POSTFIX)) {
                token = token.replace(FB_POSTFIX, '');
            }

            ls.set(LS_KEYS.USERTOKEN, token);
            this.isAuth = token;

            // clean params
            window.history.replaceState(null, null, window.location.pathname);
        }
    }

    @action.bound
    setUserCamAccess(value) {
        this.userCamAccess = value;
        ls.set('userCamAccess', value);
    }

    @action.bound
    setUserMicAccess(value) {
        this.userMicAccess = value;
        ls.set('userMicAccess', value);
    }

    @action.bound
    setMediaStatus(value) {
        this.mediaStatus = value;
    }

    @action.bound
    readUserMicAccessFromStorage() {
        this.setUserMicAccess(ls.get('userMicAccess'));
    }

    @action.bound
    readUserCamAccessFromStorage() {
        this.setUserCamAccess(ls.get('userCamAccess'));
    }

    /**
     * Set User Data with 2 types:
     * - Real (for Recently logged)
     */
    @action
    fetchUserDataFromLocalStorage() {
        const userData = ls.get(LS_KEYS.USER_DATA);
        if (userData) {
            this.setUserData(userData);
        } else {
            console.warn('no user data in local storage');
        }
    }

    setUserData(userData) {
        ls.set(LS_KEYS.USER_DATA, userData);
        gaRegisterUser(userData);
        set(this.user, userData);
    }

    @action.bound
    async pushPitch() {
        const { file, name, time, _id } = this.pitchToEdit;
        const formData = new FormData();

        this.inUploadProcess = true;

        if (file != null) {
            formData.append('presentation', file);
        }
        formData.append('pitchName', name);
        formData.append('pitchTime', time);

        if (_id != null) {
            formData.append('_id', _id);
        }
        const { success } = await api.post('/user/presentation', formData, (options) => {
            delete options.headers['Content-Type'];
            options.body = formData;
        });
        if (!success) {
            this.notifyStore.show({
                message: (
                    <div>
                        For temporary reasons we can not use your pdf
                        <br />
                        <button
                            className="p-rounded-4 p-button-info p-button-sm p-ml-2 p-mr-1"
                            type="button"
                            onClick={() => {
                                // eslint-disable-next-line max-len
                                window.open('https://www.adobe.com/acrobat/online/compress-pdf.html', '_blank');
                            }}
                        >
                            PRESS TO OPTIMIZE YOUR PDF
                        </button>
                    </div>
                ),
                options: {
                    variant: 'error',
                },
            });
        }
        await this.saveUser();
        await this.fetchUserFromBackend();
        set(this.user, { chosenPitchIndex: this.user.pitches.length - 1 });
        await this.saveUser(); // TODO: I HATE IT
        this.inUploadProcess = false;
    }

    changePitchToEdit(pitchData) {
        this.pitchToEdit = pitchData;
    }

    createPitchToEdit() {
        this.pitchToEdit = NEW_PITCH;
    }

    @action.bound
    async deletePitch(pitchForDelete) {
        await api.delete('/user/presentation', { id: pitchForDelete._id });
        await this.saveUser();
        await this.fetchUserFromBackend();
        set(this.user, { chosenPitchIndex: this.user.pitches.length - 1 });
        await this.saveUser(); // TODO: I HATE IT
    }
}

export default UserStore;
