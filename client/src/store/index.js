import UserStore from './UserStore';
import RoomStore from './RoomStore';
import StatsStore from './StatsStore';
import NotifyStore from './NotifyStore';
import UserStatsStore from './UserStatsStore';
import ProfilesStore from './ProfilesStore';
import EventsStore from './EventsStore';
import { initRoutingStore } from './RoutingStore';

class Store {
    constructor() {
        const routingStore = initRoutingStore();

        this.routingStore = routingStore;
        this.eventsStore = new EventsStore(this);
        this.userStore = new UserStore(this);
        this.roomStore = new RoomStore(this);
        this.userStatsStore = new UserStatsStore(this);
        this.notifyStore = new NotifyStore(this);
        this.profilesStore = new ProfilesStore(this);
        this.statsStore = new StatsStore(this);
        this.userStore.init();
        this.eventsStore.init();
    }
}

export default Store;
