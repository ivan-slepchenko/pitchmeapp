import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import { IS_DEV } from 'utils/constants';

const reduxStore = createStore(composeWithDevTools(applyMiddleware(thunk)));

if (IS_DEV) {
    window.reduxStore = reduxStore;
}

export default reduxStore;
