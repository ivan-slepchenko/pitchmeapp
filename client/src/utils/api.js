import { LS_KEYS } from 'utils/constants/lsKeys';
import { API_URL } from './constants';
import ls from './localStorage';

const apiPath = (path) => `${API_URL}/api${path}`;

async function request(method, path, body, upgradeOptions) {
    const params = [apiPath(path)];

    let options;
    if (['POST', 'PATCH', 'DELETE'].includes(method)) {
        options = {
            method,
            headers: {
                'Content-Type': 'application/json',
                'x-access-token': ls.get(LS_KEYS.USERTOKEN),
            },
            body: JSON.stringify(body),
            credentials: 'omit',
        };
    } else {
        options = {
            headers: {
                'x-access-token': ls.get(LS_KEYS.USERTOKEN),
            },
        };
    }

    if (upgradeOptions) {
        upgradeOptions(options);
    }

    params.push(options);

    try {
        const response = await fetch(...params);
        const { status, ok } = response;
        let json = null;

        if (status === 401) {
            ls.remove(LS_KEYS.USERTOKEN);

            window.location.href = '/login';
        }

        try {
            json = await response.json();
        } catch (jsonError) {
            console.warn('[api] error: there is no json');
        }

        return { status, ok, ...json };
    } catch (error) {
        console.error('[api] error', error);
    }

    return null;
}

/**
 * If success it returns:
 * { status, ok, data, success }
 */
const api = {
    get(path, options) {
        return request('GET', path, options);
    },

    post(path, data, options) {
        return request('POST', path, data, options);
    },

    patch(path, data, options) {
        return request('PATCH', path, data, options);
    },

    delete(path, data, options) {
        return request('DELETE', path, data, options);
    },
};

export default api;
