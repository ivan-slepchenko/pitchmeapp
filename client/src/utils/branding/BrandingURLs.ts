export default class BrandingURLs {

    public static getSquaredLogo(brandingCode:string):string
    {
        return `media/images/branding/${brandingCode}_sq.png`;
    }

    public static getLogo(brandingCode:string):string
    {
        return `media/images/branding/${brandingCode}.png`;
    }
}

