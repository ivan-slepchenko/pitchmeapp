export const NEW_CONNECTION = 'connect';

export const CONNECT_USER = 'connect_user';

export const ROOM_CONNECTION = 'room_connection';

export const LEAVE_ROOM = 'leave_room';

export const CHANGE_USERS_COUNT = 'change_users_count';

export const TIMER_TIMEOUT = 'timer_timeout';

export const DISCONNECT = 'disconnect';

export const MORE_THAN_ONE_ACTIVE_TAB = 'more_than_one_active_tab';

