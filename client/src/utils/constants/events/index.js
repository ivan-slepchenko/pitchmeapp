import { PRESENTATION } from './presentation';
import { PITCH } from './pitch';

export const EVENTS = {
    PRESENTATION,
    PITCH,
};
