export const PRESENTATION = {
    CHANGE_PAGE: 'change_page',
    CHANGE_SIZE: 'change_size',
    FEEDBACK_ON_DECK: 'feedback_on_deck',
};
