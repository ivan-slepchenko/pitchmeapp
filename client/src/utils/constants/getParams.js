export const GET_PARAMS = {
    CLEAR_LS: 'clear_ls', // getParam to clear localStorage
    RESET_PASS_TOKEN: 'token', // getParam to reset password
    TESTER: 'tester', // getParam to set user as a tester
    INVITE_CODE: 'invite_code', // getParam to get into event room
};
