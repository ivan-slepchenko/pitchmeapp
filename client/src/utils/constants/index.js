const { REACT_APP_ENV } = process.env;

const hostEndpoint = `https://${document.location.hostname}:8080`;
const productionEndpoint = 'https://app.pitchme.app:8080';

const ENV_API_URLS_MAP = {
    development: hostEndpoint,
    production: productionEndpoint,
};

export const IS_DEV = REACT_APP_ENV === 'development';

console.log(`CURRENT ENVIRONMENT: ${REACT_APP_ENV}`);

export const API_URL = ENV_API_URLS_MAP[REACT_APP_ENV] || hostEndpoint;
