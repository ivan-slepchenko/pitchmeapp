export const LS_KEYS = {
    USERTOKEN: 'usertoken',
    INVITE_CODE: 'invite_code', // store inviteCode link id for private room
    USER_DATA: 'user_data', // store main fields of userStore.user
    INVISIBLE: 'INVISIBLE',
    TESTER: 'tester',

    LAST_LEFT_ROOM_TIME: 'LAST_LEFT_ROOM_TIME', // last time when user left the room

    AWARD_MODAL: 'AWARD_MODAL', // field to test award user behavior
    USER_SIGN_UP_DATE: 'USER_SIGN_UP_DATE',

    EVIL_INVESTOR_SIGN_UP: 'EVIL_INVESTOR_SIGN_UP',
};
