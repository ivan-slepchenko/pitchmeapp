export const INTRO = 'INTRO';
export const PITCH = 'PITCH';
export const CRAFTING_FEEDBACK = 'CRAFTING_FEEDBACK';
export const REVIEWING_FEEDBACK = 'REVIEWING_FEEDBACK';
export const LAST_WORD = 'LAST_WORD';
