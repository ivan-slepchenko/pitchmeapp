// eslint-disable-next-line
export const zIndex = {
    background: -10,
    backgroundText: -9,
    buttons: -1,
    text: 0,
    foreground: 10,
    foregroundText: 11,
};
