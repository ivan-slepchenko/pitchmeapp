const { userAgent } = navigator;

const isIOS = !!userAgent.match(/iPad/i) || !!userAgent.match(/iPhone/i);

const isWebkit = !!userAgent.match(/WebKit/i);

export const isIOSSafari = isIOS && isWebkit && !userAgent.match(/CriOS/i);

// Internet Explorer 6-11
export const isIE = false || !!document.documentMode || userAgent.indexOf('MSIE') !== -1;

// Edge 20+
export const isEdge = !isIE && !!window.StyleMedia;
