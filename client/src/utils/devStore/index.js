import ls from 'utils/localStorage';

// TODO: move file to LSStore
const WITH_MIRROR = 'with_mirror';
function toggleStreamMode() {
    const withMirror = ls.get(WITH_MIRROR);

    ls.set(WITH_MIRROR, !withMirror);

    return {
        message: `Stream mode: ${!withMirror}`,
        value: !withMirror,
    };
}

function isStreamMode() {
    return ls.get(WITH_MIRROR);
}

const devStore = { toggleStreamMode, isStreamMode };

export default devStore;
