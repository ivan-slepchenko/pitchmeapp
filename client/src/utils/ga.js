const mixpanel = require('mixpanel-browser');

export function gaBoundaryError(error, errorInfo) {
    mixpanel.track('Error boundary catched the error', { error, errorInfo });
}

export function gaMirrorCantJoinRoomNoCameraAccess() {
    mixpanel.track('Cant Join Room - No Camera Access', { category: 'Mirror' });
}
// does not works
export function gaMirrorCantJoinRoomTimeLimitExceed() {
    mixpanel.track('Cant Join Room - Time Limit Exceed', { category: 'Mirror' });
}

export function gaMirrorWantsToJoinGlobalRoom() {
    mixpanel.track('Wants To Join Global Room', { category: 'Mirror' });
}

export function gaMirrorEnableCamera() {
    mixpanel.track('Turn On Camera', { category: 'Mirror' });
}

export function gaMirrorEnableMic() {
    mixpanel.track('Turn On Mic', { category: 'Mirror' });
}

export function gaUserMediaStreamStartFailed(error) {
    mixpanel.track('Error happen while starting media stream', { category: 'Mirror', error });
}

export function gaSearchBackToMirror() {
    mixpanel.track('Back To Mirror', { category: 'Search' });
}

export function gaSwitchToFullScreen() {
    mixpanel.track('Switch Into Full Screen', { category: 'Global' });
}

export function gaQuitFullScreen() {
    mixpanel.track('Quit Full Screen', { category: 'Global' });
}

export function gaRoomControlBarLeaveRoom() {
    mixpanel.track('Leave Room', { category: 'Room' });
}

export function gaPitchTimeOut() {
    mixpanel.track('Pitch Timeout', { category: 'Room' });
}

export function gaFeedbackTimeOut() {
    mixpanel.track('Pitch Timeout', { category: 'Room' });
}

export function gaStartPitch() {
    mixpanel.track('Start Pitch', { category: 'Room' });
}

export function gaKeyUsedToControlPresentation() {
    mixpanel.track('Keyboard Controls Used', { category: 'Presentation' });
}

export function gaNextPage() {
    mixpanel.track('Next Page Requested', { category: 'Presentation' });
}

export function gaPreviousPage() {
    mixpanel.track('Previous Page Requested', { category: 'Presentation' });
}

export function gaPitchUploadClicked() {
    mixpanel.track('Upload Clicked', { category: 'Pitch Editor' });
}

export function gaRoomJoin(waitingTime) {
    mixpanel.track('Join Room', { category: 'Room', waitingTime });
}

export function gaRoomLeave(roomLifeTime, reason) {
    mixpanel.track('Leave', { category: 'Room', reason, roomLifeTime });
}

export function gaRoomDisconnect(socketLifeTime) {
    mixpanel.track('Socket Disconnect By Error', { category: 'Room', socketLifeTime });
}

export function gaVideoConnected(roomLifeTime) {
    mixpanel.track('Video Connected', { category: 'Room', roomLifeTime });
}

export function gaPageView(pathname) {
    mixpanel.track('Page View', { page: pathname });
}

export function gaRegisterUser(user) {
    const { id, name, contactEmail, email } = user;
    mixpanel.identify(email);
    mixpanel.people.set({ $id: id, $name: name, $email: email, $contactEmail: contactEmail });
}
