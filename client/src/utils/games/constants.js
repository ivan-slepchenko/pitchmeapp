const WHO_AM_I = 'who_am_i';
const TRUTH_OR_DARE = 'truth_or_dare';

const GAME_LIST = {
    [WHO_AM_I]: {
        title: 'Who Am I',
        id: WHO_AM_I,
    },
    [TRUTH_OR_DARE]: {
        title: 'Truth Or Dare',
        id: TRUTH_OR_DARE,
    },
};

export { WHO_AM_I, TRUTH_OR_DARE, GAME_LIST };
