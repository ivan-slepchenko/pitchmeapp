import { email } from './constants/regex';
import { API_URL } from './constants';

const isEmail = (value) => email.test(value);

const getURLParam = (param) => {
    const urlObj = new URL(window.location.href);
    return urlObj.searchParams.get(param);
};

/**
 * create custom delay
 * @param {Number} ms - wait milliseconds
 */
const wait = (ms = 2000) => new Promise((res) => setTimeout(res, ms));

/**
 * remove html symbols
 * @param {String} stringWithHTML
 */
const removeHTML = (stringWithHTML) => stringWithHTML.replace(/(<[^>]*>)|(\s)/g, '');

function copy(that, value) {
    const inp = document.createElement('input');
    document.body.appendChild(inp);
    inp.value = value || that.textContent;
    inp.select();
    document.execCommand('copy', false);
    inp.remove();
}

const ONE_MINUTE = 1000 * 60;
function toMinutes(ms) {
    return ms / ONE_MINUTE;
}

function getMinutes(finishMs, startMs) {
    return (finishMs - startMs) / ONE_MINUTE;
}

function getUserThumbnail(id) {
    return `${API_URL}/userstore/profiles/${id}.jpg`;
}

export { wait, copy, getMinutes, toMinutes, getURLParam, isEmail, removeHTML, getUserThumbnail };
