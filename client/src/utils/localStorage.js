/* eslint-disable */

/**
 * Local Storage service
 * TODO: migrate to lib: https://www.npmjs.com/package/local-storage
 */

class LocalStorage {
    constructor() {
        this._ls = window.localStorage;
    }

    get(key) {
        let result = this._ls.getItem(key);

        if (result === null || result === 'undefined') {
            return null;
        } else {
            try {
                result = JSON.parse(result);
            } catch (err) {
                console.warn(err);
            }
        }

        return result;
    }

    set(key, obj) {
        const item = JSON.stringify(obj);

        this._ls.setItem(key, item);
    }

    remove(key) {
        this._ls.removeItem(key);
    }

    clear() {
        this._ls.clear();
    }
}

const ls = new LocalStorage();
window.ls = ls;

export default ls;
