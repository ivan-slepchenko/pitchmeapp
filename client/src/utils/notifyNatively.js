// docs: https://pushjs.org/docs/quick-start
import Push from 'push.js';

function notifyNatively({ title, body }) {
    Push.create(title, {
        body,
        icon: '/logo.png',
        onClick() {
            window.focus();
            this.close();
        },
    });
}

export default notifyNatively;
