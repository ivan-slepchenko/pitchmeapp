import { isEdge, isIE } from './detectBrowser';

/** We don't support these browsers */
export function isNotSupportedBrowser() {
    return isIE || isEdge;
}
