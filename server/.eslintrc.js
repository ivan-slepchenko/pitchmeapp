module.exports = {
    extends: 'airbnb-base',
    plugins: ['security'],
    settings: {
        'import/resolver': 'webpack',
    },
    rules: {
        indent: ['error', 4, { SwitchCase: 1, flatTernaryExpressions: false }],
        'max-len': ['error', { code: 120, ignoreComments: true }],
        'implicit-arrow-linebreak': 0,
        'linebreak-style': 0,
        'no-unused-vars': 1,
        'comma-dangle': 0,
        'import/prefer-default-export': 0,
        'class-methods-use-this': 'off',
        curly: ['error', 'all'], // All blocks must be wrapped in curly braces {}
        'function-paren-newline': 'off',
        'global-require': 'off',
        'import/extensions': 'off',
        'import/no-unresolved': 'off', // For absolute import support
        // Allowing importing from dev deps (for stories and tests)
        'import/no-extraneous-dependencies': 'off',
        // Allowing warning and error console logging
        'no-console': [
            'error',
            {
                allow: ['warn', 'error', 'info', 'log'],
            },
        ],
        // Disallow more than 1 empty lines
        'no-multiple-empty-lines': [
            'error',
            {
                max: 1,
            },
        ],
        // Cannot reassign function parameters but allowing modification
        'no-param-reassign': [
            'error',
            {
                props: false,
            },
        ],
        'no-plusplus': 'off',
        'no-underscore-dangle': 1,
        // Allowing Math.pow rather than forcing `**`
        'no-restricted-properties': [
            'off',
            {
                object: 'Math',
                property: 'pow',
            },
        ],
        'object-curly-newline': 'off',
        'padded-blocks': ['error', 'never'], // Enforce no padding within blocks
        'prefer-destructuring': 'off',
        'consistent-return': 1,
    },
};
