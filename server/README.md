# socket.id: Server Side

## NPM Tasks

-   `set NODE_ENV=development&&npm run dev`  
    Starts the development server with developer local ip to use, watch the source code and restart
    if code changes. Server printing logs into console.  
    COPY YOUR SELF-SIGNED CERTIFICATE FROM FRONT END INTO** _security_ **FOLDER\*\*

-   `npm run build`  
    build a production bundle.

-   `set NODE_ENV=development&&npm run start`  
    run the production server deamonized by pm2. Logs getting into the file, check pm2 commands to
    read them.

-   `npm run stop`  
    stop deamonized production server.

## Server Start Update & Reload:

To start backend, when you did instance restart:

```shell
cd backend
git pull
npm install
npm run build
sudo npm run start
```

To update backend, when you are in spark-me-club-backend folder on a server:

```shell
cd backend
git pull
npm install
npm run build
pm2 reload all
```

### Mongo Database

Mlab hosted db

## Notes

#### Dev ssl for arch/manjaro linux:

-   useful link: https://bbs.archlinux.org/viewtopic.php?id=235724

1. Create an RSA Keypair in ./security folder

-   `openssl req -nodes -new -x509 -keyout dev.key -out dev.crt` (skip all steps except Common Name:
    with input localhost)

2. Trust the root SSL certificate

-   You need to put your pem formatted cert in /etc/ca-certificates/trust-source/anchors/ it needs
    to be ending in .cert
-   `sudo trust extract-compat`
-   `sudo update-ca-trust extract`
-   restart browser

### Environment Variables

-   NODE_ENV = development

add to host files, to test server worker
127.0.0.1       socket.id

www.mvps.net
VPS: 178.157.82.240
L: root
P: Coca_Cola1981


docker exec -it <mycontainer> bash

see logs in container - pm2 logs

https://www.convertapi.com/ - to optimize pdf

MongoDB
https://www.kamatera.com

Mailing Campaign SendPulse
https://sendpulse.com/

google Oauth (ivan.slepchenko@zettech.com)
https://console.cloud.google.com/apis/credentials?project=pitchmeapp-293918
facebook Oauth (ivan's personal account)
https://developers.facebook.com/apps/2088126071318135/fb-login/settings/
linkedin Oauth (ivan's personal account)
https://www.linkedin.com/developers/apps/37360513/settings

pitchme.app CSR certificate:
openssl req -new -newkey rsa:2048 -nodes -out star_pitchme_app.csr -keyout star_pitchme_app.key -subj "/C=LU/ST=/L=Bissen/O=Ivan Sliepchenko/CN=*.pitchme.app"
