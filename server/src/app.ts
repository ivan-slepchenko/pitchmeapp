import express from 'express';
import cors from 'cors';
import expressPinoLogger from 'express-pino-logger';
import helmet from 'helmet';

import initModules from 'modules';
import logger from 'utils/logger';
import { IS_DEV } from 'utils/constants';

const Sentry = require('@sentry/node');

const app = express();

if (!IS_DEV) {
    Sentry.init({ dsn: 'https://e51b8e66620247c799833af1518a6df7@sentry.io/1552759' });
    // The request handler must be the first middleware on the app
    app.use(Sentry.Handlers.requestHandler());
}

// TODO: also need to do: https://expressjs.com/en/advanced/best-practice-security.html
app.disable('x-powered-by');
app.use(helmet());
app.use(cors());
app.use(express.json({limit: '20mb'}));
app.use(express.urlencoded({limit: '20mb', extended: true}));
app.use('/userstore', express.static('/userstore'));

if (!IS_DEV) {
    app.use(expressPinoLogger({ logger, level: 'warn' }));
}

app.use(express.static('public'));

app.get('/api', (req, res) =>
    res.status(201).json({
        message: 'API is working',
        // @ts-ignore
        usersCount: global.usersByEvent || 0,
    })
);

initModules(app);

if (!IS_DEV) {
    app.use(Sentry.Handlers.errorHandler());
}

export default app;
