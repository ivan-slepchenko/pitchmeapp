import mongoose from 'mongoose';
import logger, { inform } from 'utils/logger';

import { DB_PATH } from './protected';

const MONGO_PATH = DB_PATH;

async function initMongoDB(connectedCb) {
    try {
        await mongoose.connect(MONGO_PATH, { useNewUrlParser: true, useUnifiedTopology: true });
        mongoose.set('useFindAndModify', false);
        mongoose.Promise = global.Promise;

        inform(`[MongoDB] ${MONGO_PATH}`);

        if (connectedCb) {
            connectedCb();
        }
    } catch (err) {
        logger.error('[MongoDB]', err);
    }

    return mongoose.connection;
}

export default initMongoDB;
