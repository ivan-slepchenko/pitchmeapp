import fs from 'fs';
import https from 'https';

import { IS_DEV, EXPRESS_PORT, SERVER_HOST } from 'utils/constants';
import { inform } from 'utils/logger';

import app from './app';
import initMongoDB from './db';
import initSockets from './sockets';

const securityPath = `./security/${IS_DEV ? 'dev' : 'prod'}`;
const serverOptions = {
    key: fs.readFileSync(`${securityPath}.key`),
    cert: fs.readFileSync(`${securityPath}.crt`),
    requestCert: false,
    rejectUnauthorized: false,
    passphrase: 'FatBoySlim1981',
};
const server = https.createServer(serverOptions, app).listen(EXPRESS_PORT, () => inform(`[Express] ${SERVER_HOST}`));

initMongoDB();
initSockets(server);
