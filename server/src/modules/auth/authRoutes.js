import { Router } from 'express';
import passport from 'passport';

import {
    handleStrategy,
} from 'modules/auth/controllers';
import { CLIENT_HOST } from 'utils/constants';
import deleteUser from 'modules/auth/controllers/deleteUser';

function initAuthRoutes() {
    const router = Router();

    // google routes
    router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));
    router.get(
        '/google/callback',
        passport.authenticate('google', { failureRedirect: `${CLIENT_HOST}/login` }),
        handleStrategy
    );

    // facebook routes
    router.get('/facebook', passport.authenticate('facebook', { scope: ['email'] }));
    router.get(
        '/facebook/callback',
        passport.authenticate('facebook', { failureRedirect: `${CLIENT_HOST}/login` }),
        handleStrategy
    );

    // linkedin routes
    router.get('/linkedin', passport.authenticate('linkedin', { scope: 'r_liteprofile' }));
    router.get(
        '/linkedin/callback',
        passport.authenticate('linkedin', { failureRedirect: `${CLIENT_HOST}/login`, failureFlash: true }),
        handleStrategy
    );

    router.get('/facebook/burn', deleteUser);

    return router;
}

export default initAuthRoutes;
