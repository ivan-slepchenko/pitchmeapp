import User from 'modules/user/schemas/userSchema';
import { createJWT } from 'utils/helpers/auth';
import { getRandomItem } from 'utils/helpers/common';
import { CLIENT_HOST } from 'utils/constants';

const clientSuccessUrl = (token) => `${CLIENT_HOST}/profile/?usertoken=${token}`;

const clientErrorUrl = `${CLIENT_HOST}/login`;

async function downloadImage(url, profileId) {
    const axios = require('axios');
    const fs = require('fs');

    const writer = fs.createWriteStream(`/userstore/profiles/${profileId}.jpg`);

    const response = await axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
    });
}

function generatePassword() {
    const extra = '#$_!)(';
    const randStr = Math.random().toString(36).slice(2, 10);
    const first = getRandomItem(extra);
    const second = getRandomItem(extra);
    return `${randStr}${first}${second}`;
}

async function handleStrategy(req, res) {
    const {
        id,
        displayName,
        emails,
        provider,
        photos
    } = req.user;

    try {
        // TODO: fb don't have emails sometimes so we make own
        const hasOriginalEmail = emails && emails.length > 0;
        const email = hasOriginalEmail ? emails[0].value : `${provider}_${id}@pitchme.app`;
        let user = await User.findOne({ email });

        if (!user) { // patch pitchme.app user to a pitchme.app
            user = await User.findOneAndUpdate(
                { email: `${provider}_${id}@pitchme.app` },
                {
                    $set: {
                        email: `${provider}_${id}@pitchme.app`
                    },
                }
            );
        }

        let photo = null;
        if (provider === 'facebook') {
            photo = `https://graph.facebook.com/${id}/picture?type=normal`;
        } else if (photos != null && photos.length > 0) { photo = photos[0].value; }
        if (user) {
            await downloadImage(photo, user.id);
            return res.redirect(clientSuccessUrl(createJWT({ userId: user.id })));
        }
        const newUser = await User.create({
            authProvider: provider,
            contactEmail: '',
            startupName: '',
            profileId: id,
            email,
            name: displayName,
            password: generatePassword,
        });
        await downloadImage(photo, newUser.id);
        return res.redirect(clientSuccessUrl(createJWT({ userId: newUser.id })));
    } catch (error) {
        console.log(`HANDLE LINKED IN STRATEGY ERROR ${error}`);
        req.log.error('[handleStrategy]', error);
    }

    return res.redirect(clientErrorUrl);
}

export default handleStrategy;
