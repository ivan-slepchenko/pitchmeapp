import { Router } from 'express';
import { getFeatures } from 'modules/brand/controllers/getFeatures';

function initBrandRoutes() {
    const brandRouter = Router();

    brandRouter.route('/features').get((req, res) => getFeatures(req, res));

    return brandRouter;
}

export default initBrandRoutes;
