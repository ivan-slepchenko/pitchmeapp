import FeatureFlag from '../schemas/featureFlagsSchema';

export async function getFeaturesByBrandingCode(brandingCode) {
    const featureFlagsList = await FeatureFlag.find({ brandingCode: [brandingCode, null] });
    const featureFlags = {};
    for (let index = 0; index < featureFlagsList.length; index++) {
        const featureFlagData = featureFlagsList[index];
        const {
            featureCode,
            enabled: featureEnabled,
            values: featureValuesString,
            brandingCode: featureBrandingCode
        } = featureFlagData;

        let featureValues = {};
        try {
            if (featureValuesString != null) {
                featureValues = JSON.parse(featureValuesString);
            }
        } catch (e) {
            console.log(`JSON FEATURE ${featureCode} PARSING ERROR ERROR: ${e}`);
        }

        if (featureBrandingCode != null || featureFlags[featureCode] == null) {
            featureFlags[featureCode] = {
                enabled: featureEnabled,
                values: featureValues
            };
        }
    }
    return featureFlags;
}

export async function getFeatures(req, res) {
    try {
        const { brandingCode } = req.query;
        const featureFlags = await getFeaturesByBrandingCode(brandingCode);
        return res.status(201).json({
            data: featureFlags,
            success: true,
        });
    } catch (error) {
        return res.status(401).json({ msg: 'Request Failed' });
    }
}
