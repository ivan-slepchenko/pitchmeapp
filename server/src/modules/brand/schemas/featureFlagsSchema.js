import mongoose, { Schema } from 'mongoose';

const featureFlagSchema = new Schema(
    {
        brandingCode: { type: String, required: false },
        featureCode: { type: String, required: true },
        enabled: { type: Boolean, required: true },
        values: { type: String, required: false }
    },
    {
        collection: 'featureFlags',
    }
);

export default mongoose.model('FeatureFlag', featureFlagSchema);
