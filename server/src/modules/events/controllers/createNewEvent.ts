import Event from 'modules/events/schemas/eventsSchema';
export async function createNewEvent(req, res):Promise<Event> {
    try {
        const newEvent = new Event();
        // const now = new Date();
        //
        // let events = await Event.find(
        //     { type: { $ne: PRIVATE_EVENT } }
        // );
        //
        // events = events.filter(
        //     (event) => moment.utc(event.startTime, EVENT_TIME_STRING_FORMAT).toDate().getTime() >= now.getTime()
        // );

        return res.status(201).json({
            data: newEvent,
            success: true,
        });
    } catch (error) {
        return res.status(401).json({ msg: 'Request Failed' });
    }
}
