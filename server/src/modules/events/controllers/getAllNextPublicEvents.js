import Event, { PRIVATE_EVENT } from 'modules/events/schemas/eventsSchema';
import moment from 'moment';

export const EVENT_TIME_STRING_FORMAT = 'YYYY-MM-DD:HH:mm';

export async function getAllNextPublicEvents(req, res) {
    try {
        const now = new Date();

        let events = await Event.find(
            { type: { $ne: PRIVATE_EVENT } }
        );

        events = events.filter(
            (event) => moment.utc(event.startTime, EVENT_TIME_STRING_FORMAT).toDate().getTime() >= now.getTime()
        );

        return res.status(201).json({
            data: events,
            success: true,
        });
    } catch (error) {
        return res.status(401).json({ msg: 'Request Failed' });
    }
}
