import Event from '../schemas/eventsSchema';

const defaultEvent = {
    brandingCode: 'pitchmeapp',
    inviteCode: 'public',
    title: 'pitchme.app every day pitching practice',
    startTime: '2000-00-00:00:00',
    endTime: '2040-00-00:00:00',
};

export async function getNextEventByInviteCode(inviteCode) {
    const event = await Event.findOne({ inviteCode }, {}, { sort: { created_at: -1 } });
    return event || defaultEvent;
}

export async function getNextEvent(req, res) {
    try {
        // eslint-disable-next-line camelcase
        const { invite_code } = req.query;
        const event = await getNextEventByInviteCode(invite_code);
        return res.status(201).json({
            data: event,
            success: true,
        });
    } catch (error) {
        return res.status(401).json({ msg: 'Request Failed' });
    }
}
