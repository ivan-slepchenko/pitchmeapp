import { Router } from 'express';
import { getNextEvent } from 'modules/events/controllers/getNextEvent';
import { getAllNextPublicEvents } from 'modules/events/controllers/getAllNextPublicEvents';
import { createNewEvent } from 'modules/events/controllers/createNewEvent';

function initEventRoutes() {
    const eventRouter = Router();

    eventRouter.post((req, res) => createNewEvent(req, res));
    eventRouter.route('/upcoming').get((req, res) => getAllNextPublicEvents(req, res));
    eventRouter.route('/next').get((req, res) => getNextEvent(req, res));
    eventRouter.route('/all_next_public_events').get((req, res) => getAllNextPublicEvents(req, res));

    return eventRouter;
}

export default initEventRoutes;
