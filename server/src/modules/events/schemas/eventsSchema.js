import mongoose, { Schema } from 'mongoose';

export const FREE_EVENT = 'FREE';
export const PASS_EVENT = 'PASS';
export const PRIVATE_EVENT = 'PRIVATE';
export const PREVALUATION_EVENT = 'PREVALUATION';

const eventSchema = new Schema(
    {
        title: { type: String, required: true },
        description: { type: String, required: true },
        location: { type: String, required: true },
        startTime: { type: String, required: true },
        endTime: { type: String, required: true },
        inviteCode: { type: String, required: false, default: null },
        brandingCode: { type: String, required: false, default: null },
        type: {
            type: String,
            required: false,
            enum: [FREE_EVENT, PASS_EVENT, PRIVATE_EVENT, PREVALUATION_EVENT],
            default: FREE_EVENT
        },
        hostEventId: { type: Schema.Types.ObjectId, ref: 'Event', required: false },
    },
    {
        collection: 'events',
    }
);

export default mongoose.model('Event', eventSchema);
