import mongoose, { Schema } from 'mongoose';

const hostSchema = new Schema(
    {
        name: {
            type: String,
            required: false,
        },
    },
    {
        collection: 'feedbacks',
    }
);

export default mongoose.model('Host', hostSchema);
