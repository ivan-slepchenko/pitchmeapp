import passport from 'passport';

import { googleStrategy, facebookStrategy, linkedinStrategy, checkAuth } from 'utils/auth';
import authRoutes from 'modules/auth/authRoutes';
import userRoutes from 'modules/user/userRoutes';
import statsRoutes from 'modules/stats/statsRoutes';
import eventRoutes from 'modules/events/eventRoutes';
import brandRoutes from 'modules/brand/brandRoutes';
import liveRoutes from 'modules/live/liveRoutes';
import triggerRoutes from 'modules/triggers/triggerRoutes';

function initModules(app) {
    // Apply strategies
    passport.use('google', googleStrategy);
    passport.use('facebook', facebookStrategy);
    passport.use('linkedin', linkedinStrategy);
    passport.serializeUser((user, cb) => cb(null, user));

    app.use(passport.initialize());
    app.use('/api/auth', authRoutes());
    app.use('/api/live', liveRoutes());
    app.use('/api/user', checkAuth, userRoutes());
    app.use('/api/stats', statsRoutes());
    app.use('/api/events', eventRoutes());
    app.use('/api/brand', brandRoutes());
    app.use('/api/trigger', triggerRoutes());
}

export default initModules;
