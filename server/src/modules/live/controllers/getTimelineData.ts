import InLobbyStatus from 'modules/user/schemas/InlobbyStatusSchema';

export default async function getTimelineData(req, res) {
    const now = new Date();
    const startDate = new Date(now.getTime());
    startDate.setDate(now.getDate() - 5);
    const inLobbyStatus = await InLobbyStatus.find();
    return res.status(200).json({
        data: inLobbyStatus,
        success: true,
    });
}
