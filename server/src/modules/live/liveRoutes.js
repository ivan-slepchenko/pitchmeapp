import { Router } from 'express';
import getTimelineData from 'modules/live/controllers/getTimelineData';

function initLiveRoutes() {
    const liveRouter = Router();
    liveRouter.route('/timeline').get(getTimelineData);
    return liveRouter;
}

export default initLiveRoutes;
