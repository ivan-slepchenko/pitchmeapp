import User from 'modules/user/schemas/userSchema';

export default async function getAllUsers(req, res){
    const now = new Date();
    const startDate = new Date(now.getTime());
    startDate.setDate(now.getDate() - 2);
    const users = await User.find();
    let resultsArray = []
    users.forEach((user) => {
        const email = user.email.indexOf('facebook') === -1 || user.email.indexOf('linkedin') === -1 ? user.contactEmail : user.email;
        resultsArray.push({email, name: user.name});
    });
    return res.status(200).json({
        data: resultsArray,
        success: true,
    });
}
