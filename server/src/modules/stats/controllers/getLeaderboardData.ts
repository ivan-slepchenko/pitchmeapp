import logger from 'utils/logger';
import Feedback from 'modules/stats/schemas/feedbackSchema';
import User from 'modules/user/schemas/userSchema';
import { dateFromObj } from 'utils/helpers/common';

//represent key to store all events stats for the brand
export const ALL_EVENTS_CODE = 'all';

interface LeaderboardFilter {
    brandingCode?:string,
    inviteCode?:string
}

interface Stats {
    feedbacks: number,
    rank: number
}

const numeral = require('numeral');
const stats = new Map();
const refreshRequested = new Map();

async function computeLeaderboardData(startDate:Date, filter:LeaderboardFilter) {
    const feedbacks = await Feedback.find({
        $and: [{ _id: { $gt: dateFromObj(startDate) } }, filter]
    });
    const resultsMap = new Map<typeof User, Stats>();
    feedbacks.forEach((feedback) => {
        if (!resultsMap.has(feedback.performer.toString())) {
            resultsMap.set(feedback.performer.toString(), {feedbacks: 1, rank:feedback.getRankWithPenalty()});
        } else {
            const performerResults = resultsMap.get(feedback.performer.toString());
            performerResults.feedbacks = performerResults.feedbacks + 1;
            performerResults.rank = ((performerResults.rank * (performerResults.feedbacks - 1)) + feedback.getRankWithPenalty()) / performerResults.feedbacks;
            resultsMap.set(feedback.performer.toString(), performerResults);
        }
    });
    let resultsArray = []
    for (let [performerId, stats] of resultsMap) {
        const mongoUser = await User.findById(performerId).exec();
        const boostMultiplier = Math.min(stats.feedbacks, 5) / 5;
        resultsArray.push({
            userId: mongoUser._id.toString(),
            name: mongoUser.name,
            feedbacks: stats.feedbacks,
            rank: numeral(boostMultiplier * stats.rank).format('0.00')
        });
    }
    return resultsArray.sort((a, b)=>{
        return a.rank === b.rank ? b.feedbacks - a.feedbacks : b.rank - a.rank
    })
}

export function setRefreshRequested(brandingCode, inviteCode, isRequested)
{
    if (refreshRequested.get(brandingCode) == null) {
        refreshRequested.set(brandingCode, new Map());
    }
    refreshRequested.get(brandingCode).set(inviteCode, isRequested);
}

export default async function getLeaderboardData(req, res) {
    try {
        const { inviteCode, brandingCode } = req.body;

        const now = new Date();

        const brandLeaderboardAlreadyComputed:boolean = !refreshRequested.get(brandingCode) && stats.get(brandingCode);
        if(!brandLeaderboardAlreadyComputed) {
            let startDate = new Date(now.getTime());
            startDate.setMonth(now.getMonth() - 1);
            if(stats.get(brandingCode) == null){
                stats.set(brandingCode, new Map());
            }
            stats.get(brandingCode).set(ALL_EVENTS_CODE, await computeLeaderboardData(startDate, { brandingCode:brandingCode }));
            setRefreshRequested(brandingCode, ALL_EVENTS_CODE, false);
        }

        const eventLeaderboardAlreadyComputed:boolean = !refreshRequested.get(brandingCode) && stats.get(brandingCode) && stats.get(brandingCode)[inviteCode];
        if(!eventLeaderboardAlreadyComputed) {
            let startDate = new Date(now.getTime());
            startDate.setDate(now.getDate() - 1);
            stats.get(brandingCode).set(inviteCode, await computeLeaderboardData(startDate, { brandingCode:brandingCode, inviteCode:inviteCode }));
            setRefreshRequested(brandingCode, inviteCode, false);
        }

        return res.status(200).json({
            data: {
                brandLeaderboardData:stats.get(brandingCode).get(ALL_EVENTS_CODE),
                eventLeaderboardData:stats.get(brandingCode).get(inviteCode)
            },
            success: true,
        });
    } catch (error) {
        logger.error('[getLatestMonthStats]', error);
    }
    return res.status(400).json({ success: false });
}
