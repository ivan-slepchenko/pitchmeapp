import User from 'modules/user/schemas/userSchema';
import { dateFromObj } from 'utils/helpers/common';

export default async function  getNewUsersForLastDay(req, res){
    const now = new Date();
    const startDate = new Date(now.getTime());
    startDate.setDate(now.getDate() - 2);
    const users = await User.find({
        $and: [{ _id: { $gt: dateFromObj(startDate) } }]
    });
    let resultsArray = []
    users.forEach((user) => {
        resultsArray.push({systemEmail: user.email, publicEmail: user.contactEmail, name: user.name});
    });
    return res.status(200).json({
        data: resultsArray,
        success: true,
    });
}
