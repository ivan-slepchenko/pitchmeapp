import logger from 'utils/logger';
import Feedback from 'modules/stats/schemas/feedbackSchema';
import mongoose from 'mongoose';
import User from 'modules/user/schemas/userSchema';

const numeral = require('numeral');
const stats = new Map();
const refreshRequested = new Map();

export default async function getPersonalProgress(req, res) {
    try {
        const { userId } = req.query;
        let resultsArray = [];
        const ObjectId = mongoose.Types.ObjectId;
        let result = await Feedback.aggregate([
            {
                $match: { "performer": ObjectId(userId) }
            },
            {
                $lookup: {
                    from: User.collection.name,
                    localField: 'performer',
                    foreignField: '_id',
                    as: 'performerData'
                },
            },
            {
                $lookup: {
                    from: User.collection.name,
                    localField: 'companion',
                    foreignField: '_id',
                    as: 'companionData'
                },
            },
            {
                $unset: [
                    "performerData.password",
                    "performerData.createdAt",
                    "performerData.chosenPitchIndex",
                    "performerData.isOnboarded",
                    "performerData.resetPasswordDate",
                    "performerData.resetPasswordToken",
                    "performerData.profileId",
                    "companionData.password",
                    "companionData.createdAt",
                    "companionData.chosenPitchIndex",
                    "companionData.isOnboarded",
                    "companionData.resetPasswordDate",
                    "companionData.resetPasswordToken",
                    "companionData.profileId",
                    "_id"
                ]
            }
        ]);

        return res.status(200).json({
            data: result,
            success: true,
        });
    } catch (error) {
        logger.error('[getPersonalProgress]', error);
    }
    return res.status(400).json({ success: false });
}
