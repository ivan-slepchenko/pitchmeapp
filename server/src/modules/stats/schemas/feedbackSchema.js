import mongoose, { Schema } from 'mongoose';

const feedbackSchema = new Schema(
    {
        performer: { type: Schema.Types.ObjectId, ref: 'User', required: true },
        companion: { type: Schema.Types.ObjectId, ref: 'User', required: true },
        a: { type: Number, required: true },
        b: { type: Number, required: true },
        c: { type: Number, required: true },
        d: { type: Number, required: true },
        e: { type: Number, required: true },
        f: { type: Number, required: true },
        text: { type: String, required: false },
        overtime: { type: Number, required: true },
        inviteCode: { type: String, required: false },
        brandingCode: { type: String, required: true }
    },
    {
        collection: 'feedbacks',
    }
);

feedbackSchema.methods.getRank = function getRank() {
    return (this.a + this.b + this.c + this.d + this.e + this.f) / 6;
};

feedbackSchema.methods.getRankWithPenalty = function getRankWithPenalty() {
    let penalty = 1;
    if (this.overtime > 0 && this.overtime <= 30) {
        penalty = 0.7;
    }
    if (this.overtime > 30) {
        penalty = 0.5;
    }
    return this.getRank() * penalty;
};

export default mongoose.model('Feedback', feedbackSchema);
