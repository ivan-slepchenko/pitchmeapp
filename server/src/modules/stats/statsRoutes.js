import { Router } from 'express';

import getLeaderboardData from 'modules/stats/controllers/getLeaderboardData';
import getPersonalProgress from 'modules/stats/controllers/getPersonalProgress';
import getNewUsersForLastDay from 'modules/stats/controllers/getNewUsersForLastDay';
import getAllUsers from 'modules/stats/controllers/getAllUsers';

function initStatsRoutes() {
    const statsRouter = Router();

    statsRouter.route('/leaderboard').post(getLeaderboardData);
    statsRouter.route('/personal').get(getPersonalProgress);
    statsRouter.route('/new_users').get(getNewUsersForLastDay);
    statsRouter.route('/all_users').get(getAllUsers);

    return statsRouter;
}

export default initStatsRoutes;
