import * as ejs from 'ejs';
import * as https from 'https';
import Event from 'modules/events/schemas/eventsSchema';

const fs = require('fs');

const defaultEvent = {
    brandingCode: 'pitchmeapp',
    inviteCode: 'public',
    title: 'pitchme.app every day pitching practice',
    startTime: '2000-00-00:00:00',
    endTime: '2040-00-00:00:00',
};

async function getNextThreePublicEvents() {
    const events = await Event.find({}, {}, { sort: { created_at: -1 } }).limit(20);
    return events;
}

export async function getNextTreeEventsInHtml(req, res) {
    try {
        const events = await getNextThreePublicEvents();

        const template = fs.readFileSync('public/templates/nextThreeEvents.ejs').toString();
        const eventsBlock = await ejs.render(template, { events });
        const eventsPostData = JSON.stringify({
            email: 'ivan.slepchenko@zettech.com',
            phone: '+123456789',
            events_block: eventsBlock
        });

        const options = {
            host: 'events.sendpulse.com',
            path: '/events/id/69e8520a59f04b259478c37a1728776f/7710871',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': eventsPostData.length
            }
        };

        const p = new Promise((resolve, reject) => {
            const triggerEventRequest = https.request(options, (eventResponse) => {
                eventResponse.setEncoding('utf-8');

                let responseString = '';

                eventResponse.on('data', (data) => {
                    responseString += data;
                });

                eventResponse.on('end', () => {
                    resolve(JSON.parse(responseString));
                });
            });

            triggerEventRequest.on('error', (err) => {
                reject(err);
            });

            triggerEventRequest.write(eventsPostData);
            triggerEventRequest.end();
        });

        const triggerEventResponse = await p;

        return res.status(201).json({
            success: true,
            triggerEventResponse
        });
    } catch (error) {
        return res.status(401).json({ msg: `Request Failed error: ${error}` });
    }
}
