import * as https from 'https';
import Event from 'modules/events/schemas/eventsSchema';

const fs = require('fs');

async function getNextThreePublicEvents() {
    const events = await Event.find({}, {}, { sort: { created_at: -1 } }).limit(20);
    return events;
}

export async function sendUserBoardingEmail(req, res) {
    try {
        const events = await getNextThreePublicEvents();
        const eventsPostData = JSON.stringify({
            email: 'ivan.slepchenko@zettech.com',
            phone: '+352661319087',
            events_block: events
        });

        const options = {
            host: 'events.sendpulse.com',
            path: '/events/id/69e8520a59f04b259478c37a1728776f/7710871',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': eventsPostData.length
            }
        };

        const p = new Promise((resolve, reject) => {
            const triggerEventRequest = https.request(options, (eventResponse) => {
                eventResponse.setEncoding('utf-8');

                let responseString = '';

                eventResponse.on('data', (data) => {
                    responseString += data;
                });

                eventResponse.on('end', () => {
                    resolve(JSON.parse(responseString));
                });
            });

            triggerEventRequest.on('error', (err) => {
                reject(err);
            });

            triggerEventRequest.write(eventsPostData);
            triggerEventRequest.end();
        });

        const triggerEventResponse = await p;

        return res.status(201).json({
            success: true,
            triggerEventResponse
        });
    } catch (error) {
        return res.status(401).json({ msg: `Request Failed error: ${error}` });
    }
}
