import { Router } from 'express';
import { getNextTreeEventsInHtml } from 'modules/triggers/controllers/getNextTreeEventsInHtml';
import { sendUserBoardingEmail } from 'modules/triggers/controllers/sendUserBoardingEmail';

function initEventRoutes() {
    const triggerRouter = Router();

    triggerRouter.route('/next_three_events_in_html').get((req, res) => getNextTreeEventsInHtml(req, res));
    triggerRouter.route('/send_user_boarding_email').get((req, res) => sendUserBoardingEmail(req, res));

    return triggerRouter;
}

export default initEventRoutes;
