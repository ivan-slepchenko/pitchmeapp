import { promises as fs } from 'fs';

import Pitch, { getFolderPath } from '../schemas/pitchSchema';

export default async function deletePresentation(req, res) {
    try {
        const { id: _id } = req.body;
        const pitch = await Pitch.findOneAndDelete({ _id });
        if (pitch.fileUUID != null) {
            fs.unlink(`${getFolderPath()}/${pitch.fileUUID}.pdf`);
            for (let i = 1; i <= pitch.pages; i++) {
                fs.unlink(`${getFolderPath()}/${pitch.fileUUID}${i === 1 ? '.jpg' : `-${i}.jpg`}`);
            }
        }
        return res.status(201).json({
            success: true,
        });
    } catch (error) {
        req.log.error('[deletePresentation]', error);
    }

    return res.status(400).json({ success: false });
}
