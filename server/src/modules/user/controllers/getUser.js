import User from '../schemas/userSchema';

async function getUser(req, res) {
    try {
        const user = await User.findById(req.userId).populate({
            path: 'pitches',
        });

        const { name, startupName, email, id, info, chosenPitchIndex, pitches, isOnboarded, contactEmail } = user;

        return res.status(201).json({
            data: {
                id,
                name,
                startupName,
                email,
                info,
                contactEmail,
                pitches,
                chosenPitchIndex,
                isOnboarded
            },
            success: true,
        });
    } catch (error) {
        return res.status(401).json({ msg: 'Authorisation Failed' });
    }
}

export default getUser;
