import User from 'user/schemas/userSchema';

import logger from 'utils/logger';

async function getUserStats(req, res) {
    try {
        const user = await User.findById(req.userId);

        return res.status(200).json({
            data: user.stats,
            success: true,
        });
    } catch (error) {
        logger.error('[getUserStats]', error);
    }

    return res.status(400).json({ success: false });
}

export default getUserStats;
