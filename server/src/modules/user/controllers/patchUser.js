import User from 'user/schemas/userSchema';
import addValidation from 'utils/addValidation';

async function patchUser(req, res) {
    try {
        const { name, startupName, info, chosenPitchIndex, isOnboarded, contactEmail } = req.body;

        console.log(`PATCH USER: ${contactEmail}`);
        await User.findOneAndUpdate(
            { _id: req.userId },
            {
                $set: {
                    name,
                    startupName,
                    info,
                    contactEmail,
                    chosenPitchIndex,
                    isOnboarded
                },
            }
        );

        return res.status(201).json({
            success: true,
        });
    } catch (error) {
        req.log.error('[patchUser]', error);
    }

    return res.status(400).json({ success: false });
}

export default addValidation('patchUser')(patchUser);
