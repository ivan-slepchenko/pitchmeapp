import User from 'user/schemas/userSchema';
import addValidation from 'utils/addValidation';
import logger from 'utils/logger';

async function patchUserStats(req, res) {
    try {
        const { field, operator, value } = req.body;

        await User.findByIdAndUpdate(req.userId, {
            [`${operator}`]: {
                [`stats.${field}`]: value,
            },
        });

        return res.status(201).json({
            success: true,
        });
    } catch (error) {
        logger.error('[patchStats]', error);
    }

    return res.status(400).json({ success: false });
}

export default addValidation('patchUserStats')(patchUserStats);
