import convertapiPackage from 'convertapi';
import User from '../schemas/userSchema';
import Pitch from '../schemas/pitchSchema';

const MAX_PITCHES = 3;
const PPTX_TYPE = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
const PPT_TYPE = 'application/vnd.ms-powerpoint';
const PDF_TYPE = 'application/pdf';
export const pitchDeckAllowedFormats = {};
pitchDeckAllowedFormats[PDF_TYPE] = 'pdf';
pitchDeckAllowedFormats[PPT_TYPE] = 'ppt';
pitchDeckAllowedFormats[PPTX_TYPE] = 'pptx';

const convertapi = convertapiPackage('n3cRdH53lrrgbR7a');

export default async function uploadPresentation(req, res) {
    try {
        const { file, body } = req;
        const user = await User.findById(req.userId).populate('pitches');
        if (user.pitches && user.pitches.length > MAX_PITCHES) {
            return res.status(400).json({ success: false, message: 'Max pitch number limit is achieved' });
        }
        if (file && pitchDeckAllowedFormats[file.mimetype] == null) {
            return res.status(400).json({ success: false, message: 'We support pdf format only' });
        }

        const maxSize30MB = 31457280;
        if (file && file.size >= maxSize30MB) {
            return res.status(400).json({ success: false, message: 'File size is too big' });
        }

        let pitchForSave = null;

        if (body._id) {
            pitchForSave = await Pitch.findOne({ _id: body._id });
            pitchForSave.name = body.pitchName;
            pitchForSave.time = parseInt(body.pitchTime, 10);
        } else {
            pitchForSave = new Pitch({
                name: body.pitchName,
                time: parseInt(body.pitchTime, 10),
                user: user._id,
            });
            user.pitches.push(pitchForSave);
        }
        if (file) {
            const fileExtension = pitchDeckAllowedFormats[file.mimetype];
            pitchForSave.originalName = file.originalname;
            pitchForSave.fileUUID = file.filename.substring(0, file.filename.length - (fileExtension.length + 1));
            const result = await convertapi.convert('jpg', {
                File: `/userstore/decks/${file.filename}`,
                ImageResolutionH: '300',
                ImageResolutionV: '300',
                ImageQuality: '65'
            }, fileExtension);
            pitchForSave.pages = result.response.Files.length;
            result.saveFiles('/userstore/decks');
        }
        await pitchForSave.save();
        await user.save();
        return res.status(201).json({ success: true });
    } catch (error) {
        console.error('[uploadPresentation]', error);
    }

    return res.status(400).json({ success: false });
}
