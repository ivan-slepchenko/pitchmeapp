import { body } from 'express-validator';
import addErrorResponse from 'utils/addErrorResponse';

export const NAME_MAX_LENGTH = 50;

function validate(controllerFn) {
    // to work you need to define addValidation.js
    const { validationName } = controllerFn;

    let validation;
    switch (validationName) {
        case 'patchUser': {
            validation = [
                body('name')
                    .isLength({ min: 1, max: NAME_MAX_LENGTH })
                    .trim()
                    .escape(),
            ];
            break;
        }

        case 'patchUserStats': {
            validation = [body('field'), body('operator').matches(/^set|inc$/i), body('value')];
            break;
        }

        default:
            throw new Error(
                `Undefined controller for validation with name: ${validationName}
                If it's undefined you should add validationName in controller fn`
            );
    }

    // we can add custom validation error response for every controller
    // but at now it's the same
    return [...validation, addErrorResponse(controllerFn)];
}

export default validate;
