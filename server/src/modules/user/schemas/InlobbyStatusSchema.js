import mongoose, { Schema } from 'mongoose';

const inLobbyStatusSchema = new Schema(
    {
        email: {
            type: String,
            required: true,
        },
        status: {
            type: String,
            required: true,
        },
        datetime: {
            type: Number,
            required: true
        },
        isPerformer: {
            type: Boolean,
            required: true
        },
        inviteCode: {
            type: String,
            required: false
        }
    },
    {
        collection: 'inLobbyStatus',
    }
);

export default mongoose.model('InLobbyStatus', inLobbyStatusSchema);
