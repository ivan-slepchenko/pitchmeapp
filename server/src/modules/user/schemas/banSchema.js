import mongoose, { Schema } from 'mongoose';

const banSchema = new Schema(
    {
        ip: {
            type: String,
            unique: true,
            required: true,
        },
    },
    {
        collection: 'bans',
    }
);

export default mongoose.model('Ban', banSchema);
