import mongoose, { Schema } from 'mongoose';

const pitchSchema = new Schema(
    {
        name: {
            type: String,
            required: true,
        },
        time: {
            type: Number,
            required: true,
        },
        fileUUID: {
            type: String,
            required: false,
        },
        originalName: {
            type: String,
            required: false,
        },
        pages: {
            type: Number,
            required: false,
        },
        user: { type: Schema.Types.ObjectId, ref: 'User' },
    },
    {
        collection: 'pitches',
    }
);

export const getFolderPath = () => '/userstore/decks';

export default mongoose.model('Pitch', pitchSchema);
