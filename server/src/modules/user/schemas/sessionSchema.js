import mongoose, { Schema } from 'mongoose';

const sessionSchema = new Schema(
    {
        user1: { type: Schema.Types.ObjectId, ref: 'User' },
        user2: { type: Schema.Types.ObjectId, ref: 'User' },
    },
    {
        collection: 'session',
    }
);

export default mongoose.model('Session', sessionSchema);
