import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';

import userStatsSchema from '../userStatsSchema';

const userSchema = new Schema(
    {
        profileId: {
            type: String,
            unique: true,
            required: false,
        },
        email: {
            type: String,
            unique: true,
            required: true,
        },
        contactEmail: {
            type: String,
            unique: false,
            required: false,
        },
        password: {
            type: String,
        },
        resetPasswordToken: {
            type: String,
            default: '',
        },
        resetPasswordDate: {
            type: Date,
            default: '',
        },
        createdAt: {
            type: Date,
            default: Date.now,
        },
        name: {
            type: String,
            default: '',
        },
        startupName: {
            type: String,
            default: '',
        },
        info: {
            type: String,
            default: '',
        },
        stats: {
            type: userStatsSchema,
            // default: userStatsSchema
        },
        chosenPitchIndex: {
            type: Number,
            default: -1,
        },
        pitches: [{ type: Schema.Types.ObjectId, ref: 'Pitch' }],
        isOnboarded: {
            type: Boolean,
            default: false,
        }
    },
    {
        collection: 'users',
    }
);

userSchema.methods.comparePassword = async function comparePassword(password) {
    const isEqual = await bcrypt.compare(password, this.password);

    return isEqual;
};

/**
 * The pre-save hook method.
 */
userSchema.pre('save', function saveHook(next) {
    const user = this;

    // proceed further only if the password is modified or the user is new
    if (!user.isModified('password')) {
        return next();
    }

    return bcrypt.genSalt(10, (saltError, salt) => {
        if (saltError) {
            return next(saltError);
        }

        return bcrypt.hash(user.password, salt, (hashError, hash) => {
            if (hashError) {
                return next(hashError);
            }

            // replace a password string with hash value
            user.password = hash;

            return next();
        });
    });
});

export default mongoose.model('User', userSchema);
