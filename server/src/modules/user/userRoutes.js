import { Router } from 'express';
import multer from 'multer';
import { v4 as uuid } from 'uuid';

import { pitchDeckAllowedFormats } from 'modules/user/controllers/uploadPresentation';
import {
    getUser,
    patchUser,
    validate,
    getUserStats,
    patchUserStats,
    uploadPresentation,
    deletePresentation,
} from './controllers';
import { getFolderPath } from './schemas/pitchSchema';

const storage = multer.diskStorage({
    destination(req, file, cb) {
        cb(null, getFolderPath());
    },
    filename(req, file, cb) {
        cb(null, `${uuid()}.${pitchDeckAllowedFormats[file.mimetype]}`);
    },
});
const upload = multer({
    storage,
    fileFilter(req, file, cb) {
        if (pitchDeckAllowedFormats[file.mimetype] != null) {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Allowed only .pdf'));
        }
    },
});

function initUserRoutes() {
    const userRouter = Router();

    userRouter
        .route('/me')
        .get(getUser)
        .patch(validate(patchUser));

    userRouter
        .route('/presentation')
        .post(upload.single('presentation'), uploadPresentation)
        .delete(deletePresentation);

    userRouter
        .route('/stats')
        .get(getUserStats)
        .patch(validate(patchUserStats));

    return userRouter;
}

export default initUserRoutes;
