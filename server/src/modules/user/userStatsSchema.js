import { Schema } from 'mongoose';

const userStatsSchema = new Schema({
    chatsPlayed: {
        type: Number,
        default: 0,
    },
});

export default userStatsSchema;
