import Feedback from 'modules/stats/schemas/feedbackSchema';
import Session from 'modules/user/schemas/sessionSchema';
import Ban from 'modules/user/schemas/banSchema';
import logger from 'utils/logger';
import User from 'modules/user/schemas/userSchema';
import userFactory, { IUser, IUserConstr, EUserStatus } from './userFactory';
import { EVENTS } from './events';
import { dateFromObj } from 'utils/helpers/common';
import { IS_DEV } from 'utils/constants';
import { setRefreshRequested } from 'modules/stats/controllers/getLeaderboardData';
type IUsers = Map<string, IUser>;

const REVIEWING_FEEDBACK = 'REVIEWING_FEEDBACK';

function UserStore(io) {
    const userConstructor = userFactory(io);

    class Store {
        users: IUsers = new Map();

        constructor() {
            setInterval(() => this.useConnector(), 2500);
        }

        async getNotUsedUsers(currentUser: IUser, currentMongoDBUser: typeof User) {
            const now = new Date();
            // 24 hours ago
            const yesterday = new Date(now.getTime());
            yesterday.setDate(now.getDate() - 1);
            const mongoSessions = await Session.find(
                { $and:[{_id: { $gt: dateFromObj(yesterday) }}, { $or:[{user1: currentMongoDBUser._id}, {user2: currentMongoDBUser._id}] }] }
            );
            const userSessions: Array<typeof Session> = [];
            mongoSessions.forEach((session) => {
                userSessions.push(session);
            });
            console.log('MONGO SESSIONS: ' + userSessions.length);
            const usedCompanionsIds = userSessions.map(session => {
                const sessionUser1Id = session.user1._id.toString();
                const sessionUser2Id = session.user2._id.toString();
                const currentMongoUserId = currentMongoDBUser._id.toString();
                return sessionUser1Id === currentMongoUserId ? sessionUser2Id : sessionUser1Id;
            });
            const usersArray = Array.from(this.users.values());

            const result = usersArray.filter(user => {
                let foundIdIndex = usedCompanionsIds.indexOf(user.id);
                return foundIdIndex === -1 && user.id != currentUser.id;
            });
            return result;
        }

        updateSocketId(originalSocketId: string, newSocketId: string){
            const user = this.users.get(originalSocketId);
            user.socketId = newSocketId;
            this.remove(originalSocketId);
            this.add(newSocketId, user);
        }

        async add(socketId: string, userData: IUserConstr) {
            const { email } = userData;
            const user = new userConstructor(userData);
            this.log(`connected : ${email}`);
            this.users.set(socketId, user);
            this.emitUsersCount();
        }

        remove(socketId: string) {
            if (this.users.get(socketId)) {
                this.users.get(socketId).registerLobbyExit();
                this.users.delete(socketId);
                this.emitUsersCount();
            }
        }

        async handleSocketEvent({ event, socketId, payload }: { event: string; socketId: string; payload?: any }) {
            const user = this.users.get(socketId);

            this.log(`[handle event] ${event} socketId: ${socketId}`);

            this.users.forEach((value) => {
                this.log('CURRENT USERS: ' + value.email + ' : ' + value.socketId);
            });

            if (event !== EVENTS.COMMON.CONNECT_USER && event !== EVENTS.COMMON.DISCONNECT && !user) {
                this.log(`event not processed because user is not found for socketId ${event}`);
                return;
            }

            switch (event) {
                case EVENTS.COMMON.BAD_USER: {
                    user.leaveRoom(EVENTS.COMMON.LEAVE_ROOM);
                    this.remove(user.opponentSocketId);
                    this.remove(socketId);

                    try {
                        Ban.create({
                            ip: user.opponentIp,
                        });
                    } catch (banCreateError) {
                        console.log('banCreateError', banCreateError);
                    }
                    break;
                }

                case EVENTS.COMMON.LEAVE_ROOM: {
                    console.log(`on socket LEAVE_ROOM event appears for socket: ${socketId}`);
                    user
                        ? console.log('user is found for the event: ' + user.email)
                        : console.log('user is not found for the event');

                    user.leaveRoom(EVENTS.COMMON.LEAVE_ROOM);
                    this.remove(user.opponentSocketId);
                    this.remove(socketId);
                    break;
                }

                case EVENTS.COMMON.DISCONNECT: {
                    console.log(`on socket DICSCONNECT event appears for socket: ${socketId}`);
                    user
                        ? console.log('user is found for the event: ' + user.email)
                        : console.log('user is not found for the event');
                    if (user) {
                        user.leaveRoom(EVENTS.COMMON.DISCONNECT);
                        this.remove(user.opponentSocketId);
                        this.remove(socketId);
                    }
                    break;
                }

                case EVENTS.PITCH.STATE: {
                    if (payload.state === REVIEWING_FEEDBACK) {
                        const performer = this.users.get(user.opponentSocketId);
                        const performerMongoUser = await User.findById(performer.id).exec();7
                        const companionMongoUser = await User.findById(user.id).exec();
                        await this.saveFeedbackEntry(user, performerMongoUser, companionMongoUser, payload).then(() => {
                            console.log('FEEDBACK SUCCESSFULLY SAVED');
                        }, (error) => {
                            console.log(`FEEDBACK SAVE ERROR ${error}`);
                        });
                        setRefreshRequested(user.event.brandingCode, user.event.inviteCode, true);
                    }
                    user.sendEventToOpponent(event, payload);
                }

                default: {
                    user.sendEventToOpponent(event, payload);
                }
            }
        }

        private async saveFeedbackEntry(user: IUser, mongoPerformer: typeof User, mongoCompanion: typeof User, payload) {
            const feedback = new Feedback({
                performer: mongoPerformer._id,
                companion: mongoCompanion._id,
                overtime: payload.overtime,
                inviteCode: user.event.inviteCode,
                brandingCode: user.event.brandingCode,
                ...payload.feedback
            });
            try {
                await feedback.save();
                console.log('Feedback saved!');
            } catch (e){
                console.log('WTF: '+ e);
            }
        }

        findByUserId(id: string): IUser {
            return Array.from(this.users.values()).find((user: IUser) => user.id === id);
        }

        getSearchSocketIds(userStack: string[]): string[] {
            return userStack.filter((socketId) => this.users.get(socketId).status === EUserStatus.SEARCH);
        }

        async useConnector() {
            try {
                const keys = Array.from(this.users.keys());
                for (let i=0; i<keys.length; i++) {
                    const user1SocketId = keys[i];
                    const user1 = this.users.get(user1SocketId);
                    const user1MongoPresentation = await User.findOne({ email: user1.email });

                    console.log(`USE CONNECTOR FOR SOCKET ID: ${user1SocketId}`);
                    console.log(`USE CONNECTOR USER1: ${user1.email} status: ${user1.status}`);
                    if (user1.status === EUserStatus.SEARCH) {

                        const freshUsers = await this.getNotUsedUsers(user1, user1MongoPresentation);

                        console.log('FRESH USERS LENGTH: ' + freshUsers.length);

                        const user2 = freshUsers.find((candidate) => {
                            console.log('user1.inviteCode: '+user1.inviteCode);
                            console.log('candidate.inviteCode',candidate.inviteCode);
                            console.log('candidate.status',candidate.status);
                            console.log('user1.featureFlags.NO_DECKS_DEFAULT_TIME.enabled',user1.featureFlags.NO_DECKS_DEFAULT_TIME.enabled);
                            console.log('user1.chosenPitch',user1.chosenPitch);
                            // Conditions when we shouldn't connect users:
                            return !(
                                (user1.inviteCode !== candidate.inviteCode) ||
                                (candidate.status !== EUserStatus.SEARCH) ||
                                (user1.isTester && candidate.isTester) ||
                                (
                                    !user1.featureFlags.NO_DECKS_DEFAULT_TIME.enabled &&
                                    (user1.chosenPitch == null && candidate.chosenPitch == null)
                                )
                            );
                        });

                        console.log('OPPONENT FOUND: ' + user2);
                        if (user2) {
                            console.log(`USE CONNECTOR USER2 ${user2.email} status ${user2.status}`);
                            console.log(`currentUser ${user1.email} + companion ${user2.email}`);
                            await this.connectPair(user1, user2);
                        }
                    }
                }
            } catch (err) {
                console.log('[useConnector]', err);
            }
        }

        async connectPair(user1: IUser, user2: IUser) {
            const roomId = `${user1.email}vs${user2.email}`;

            let user1Turn = Math.random() >= 0.5;
            if(user1.chosenPitch == null){
                user1Turn = false; //TRUE!!
            }
            if(user2.chosenPitch == null) {
                user1Turn = true; //NOT TRUE!!
            }

            user1.roomConnect({ roomId, companion: user2, isMyTurn: user1Turn });
            user2.roomConnect({ roomId, companion: user1, isMyTurn: !user1Turn});
            console.log(`USER1 ${user1.email} status ${user1.status}`);
            console.log(`USER2 ${user2.email} status ${user2.status}`);
            //TODO: temporary solution, we have to use only mongodb User type in the whole app
            const mongoUser1 = await User.findOne({email:user1.email});
            const mongoUser2 = await User.findOne({email:user2.email});
            if(!IS_DEV && !user1.isTester && !user2.isTester){
                const session = new Session({
                    user1:mongoUser1._id,
                    user2:mongoUser2._id
                });
                await session.save();
            }
        }

        emit(event: string, data: any) {
            io.sockets.emit(event, data);
        }

        emitUsersCount() {
            const count = this.users.size;
            const usersArray = Array.from(this.users.values());
            const usersByEvent = {};
            for(let index = 0; index < this.users.size; index++){
                const user = usersArray[index];
                if(!usersByEvent[user.inviteCode]) {
                    usersByEvent[user.inviteCode] = {};
                }
                let eventUsers = usersByEvent[user.inviteCode];
                if(eventUsers[user.status] == null){
                    eventUsers[user.status] = {};
                }
                const eventUsersByStatus = eventUsers[user.status];
                const userType = user.chosenPitch == null ? 'expert' : 'performer';
                if(eventUsersByStatus[userType] == null){
                    eventUsersByStatus[userType] = 0;
                }
                eventUsersByStatus[userType] = eventUsersByStatus[userType] + 1;
            }

            // @ts-ignore
            global.usersByEvent = usersByEvent;
            this.emit(EVENTS.COMMON.CHANGE_USERS_COUNT, usersByEvent);
        }

        log(...msg) {
            logger.warn('\x1b[33m%s\x1b[0m', `[Users] ${[...msg].join(' ')}`);
        }
    }

    const usersStore = new Store();

    return usersStore;
}

export default UserStore;
