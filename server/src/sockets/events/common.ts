export const COMMON = {
    NEW_CONNECTION: 'connection',
    RECONNECT: 'reconnect',
    CONNECT_USER: 'connect_user',
    ROOM_CONNECTION: 'room_connection',
    LEAVE_ROOM: 'leave_room',
    CHANGE_USERS_COUNT: 'change_users_count',
    DISCONNECT: 'disconnect',
    MORE_THAN_ONE_ACTIVE_TAB: 'more_than_one_active_tab',
    TOGGLE_INVISIBILITY: 'TOGGLE_INVISIBILITY',
    BAD_USER: 'bad_user',
};
