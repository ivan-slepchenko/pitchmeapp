import { PRESENTATION } from './presentation';
import { COMMON } from './common';
import { PITCH } from './pitch';

export const EVENTS = {
    COMMON,
    PRESENTATION,
    PITCH
};
