import socketIO from 'socket.io';

import { SERVER_HOST } from 'utils/constants';
import { inform } from 'utils/logger';
import socketsFactory from './socketsFactory';

function initSockets(server): void {

    const io = new socketIO(server, {
        pingInterval: 3000,
        pingTimeout: 5000,
    });

    const Sockets = socketsFactory(io);
    new Sockets();

    inform(`[Socket.io] ${SERVER_HOST}/socket.io`);
}

export default initSockets;
