import logger from 'utils/logger';
import UsersStore from './UsersStore';
import { EVENTS } from './events';
import { getNextEventByInviteCode } from 'modules/events/controllers/getNextEvent';
import { getFeaturesByBrandingCode } from 'modules/brand/controllers/getFeatures';

function socketsFactory(io) {
    const usersStore = UsersStore(io);

    return class Sockets {
        constructor() {
            io.on(EVENTS.COMMON.NEW_CONNECTION, (socket) => {
                const ip = JSON.stringify(socket.request.headers['x-forwarded-for']);
                let originalSocketId = socket.id;
                let connected = true;
                console.log('NEW CONNECTION: ' + originalSocketId);

                socket.on(EVENTS.COMMON.RECONNECT, async (payload) => {
                    console.log('RECONNECT: ' + originalSocketId + ' TO: ' + socket.id)
                    usersStore.updateSocketId(originalSocketId, socket.id);
                    originalSocketId = socket.id;
                });

                socket.on(EVENTS.COMMON.CONNECT_USER, async (payload) => {
                    try {
                        // sometimes this may happen, even after event.disconnect.
                        // TODO: may be removed, the reason was in the other code chunk
                        if (!connected) return;

                        const { user, category, lang, global, inviteCode, isTester, searchFilters, isInvisible, userCamAccess } = payload;
                        const { id, profileId, name, startupName, info, email, age, gender, pitches, chosenPitchIndex, contactEmail } = user;

                        const userConnected = usersStore.findByUserId(id);
                        if (userConnected) {
                            socket.emit(EVENTS.COMMON.MORE_THAN_ONE_ACTIVE_TAB);
                            socket.disconnect(true);
                            return;
                        }

                        const event = await getNextEventByInviteCode(inviteCode);
                        console.log('EVENT IS FOUND FOR THE INVITE CODE: ' + inviteCode + " event: " + event);
                        const featureFlags = await getFeaturesByBrandingCode(event ? event.brandingCode : "pitchmeapp")

                        usersStore.add(socket.id, {
                            id,
                            profileId,
                            contactEmail,
                            startupName,
                            ip,
                            name,
                            info,
                            email,
                            age,
                            gender,
                            chosenPitch: pitches[chosenPitchIndex],
                            socketId: socket.id,
                            category,
                            lang,
                            global,
                            inviteCode,
                            isTester,
                            searchFilters,
                            isInvisible,
                            userCamAccess,
                            event,
                            featureFlags
                        });
                        console.log('USER ADDED: ' + socket.id)
                    } catch (connectUserError) {
                        logger.error('connect user error');
                        console.log(connectUserError);
                    }
                });

                socket.on(EVENTS.COMMON.LEAVE_ROOM, () =>
                    usersStore.handleSocketEvent({ event: EVENTS.COMMON.LEAVE_ROOM, socketId:socket.id })
                );

                socket.on(EVENTS.COMMON.DISCONNECT, () => {
                    socket.disconnect(true);
                    connected = false;
                    usersStore.handleSocketEvent({ event: EVENTS.COMMON.DISCONNECT, socketId:socket.id });
                });

                socket.on(EVENTS.COMMON.BAD_USER, () => {
                    usersStore.handleSocketEvent({
                        event: EVENTS.COMMON.BAD_USER,
                        socketId: socket.id
                    });
                });

                socket.on(EVENTS.COMMON.TOGGLE_INVISIBILITY, (payload) =>
                    usersStore.handleSocketEvent({
                        event: EVENTS.COMMON.TOGGLE_INVISIBILITY,
                        socketId:socket.id,
                        payload,
                    })
                );

                socket.on(EVENTS.PITCH.STATE, (payload: { state: string }) =>
                    usersStore.handleSocketEvent({ event: EVENTS.PITCH.STATE, socketId:socket.id, payload })
                );
                socket.on(EVENTS.PRESENTATION.CHANGE_PAGE, (payload: { page: number }) =>
                    usersStore.handleSocketEvent({ event: EVENTS.PRESENTATION.CHANGE_PAGE, socketId:socket.id, payload })
                );
                socket.on(EVENTS.PRESENTATION.CHANGE_SIZE, (payload: {}) =>
                    usersStore.handleSocketEvent({ event: EVENTS.PRESENTATION.CHANGE_SIZE, socketId:socket.id, payload })
                );
                socket.on(EVENTS.PRESENTATION.LOADED, (payload: {}) => {
                    usersStore.handleSocketEvent({ event: EVENTS.PRESENTATION.LOADED, socketId:socket.id, payload });
                });
                socket.on(EVENTS.PRESENTATION.FEEDBACK_ON_DECK, (payload) => {
                    usersStore.handleSocketEvent({ event: EVENTS.PRESENTATION.FEEDBACK_ON_DECK, socketId:socket.id, payload });
                });
            });
        }
    };
}
export default socketsFactory
