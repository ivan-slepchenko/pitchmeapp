import { EVENTS } from './events';
import InLobbyStatus from 'modules/user/schemas/InlobbyStatusSchema';

export enum EUserStatus {
    SEARCH = 'SEARCH',
    ROOM = 'ROOM',
    INACTIVE = 'INACTIVE',
}

export enum EUserGender {
    MALE = 'male',
    FEMALE = 'female',
    BOTH = 'both',
}

export interface IUser extends IUserConstr{
    opponentSocketId: string;
    opponentIp: string;

    userTurn: boolean;
    status: EUserStatus;
    roomId: string;


    pitchFilename?: string;

    roomConnect: (payload: any) => void;
    leaveRoom: (reason: string) => void;
    emitBoth: (event: string, payload: any) => void;
    toggleVideo: (payload: any) => void;
    sendEventToOpponent: (event: string, payload?: any) => void;
    registerLobbyExit: () => void;
}

export interface IUserConstr {
    id: string;
    profileId: string;
    ip: string;
    email: string;
    contactEmail: string;
    name: string;
    startupName: string;
    info: string;
    age?: number;
    gender?: EUserGender;
    isInvisible: boolean;
    socketId: string;
    category: string;
    chosenPitch?: any;
    lang: string;
    global: boolean;
    inviteCode: string;
    isTester?: boolean;
    searchFilters?: any;
    userCamAccess: boolean;
    event: any;
    featureFlags: any;
}

// TODO: think about io to User class
function userFactory(io) {
    class User implements IUser {
        profileId: string;
        id: string;
        ip: string;
        email: string;
        contactEmail: string;
        name: string;
        startupName: string;
        age?: number;
        gender: EUserGender;
        isInvisible: boolean;
        info:string;
        socketId: string;
        opponentSocketId: string;
        opponentIp: string;
        lang: string;
        status: EUserStatus;
        roomId: string;
        category: string;
        chosenPitch?: any;
        userTurn: boolean;
        global: boolean;
        inviteCode: string;
        isTester?: boolean;
        searchFilters?: any;
        userCamAccess: boolean;
        event: any;
        featureFlags: any;

        constructor(userData: IUserConstr) {
            this.roomId = '';
            this.gender = userData.gender || EUserGender.BOTH;

            this.id = userData.id;
            this.ip = userData.ip;
            this.email = userData.email;
            this.contactEmail = userData.contactEmail;
            this.name = userData.name;
            this.startupName = userData.startupName;
            this.info=userData.info;

            this.isInvisible = userData.isInvisible;
            this.age = userData.age;
            this.socketId = userData.socketId;
            this.category = userData.category;
            this.chosenPitch = userData.chosenPitch;
            this.lang = userData.lang;
            this.userCamAccess = userData.userCamAccess;

            // private room
            this.global = userData.global;
            this.inviteCode = userData.inviteCode; // inviteCode link

            this.isTester = userData.isTester; // tester role
            this.searchFilters = userData.searchFilters; // filter to search companion
            this.featureFlags = userData.featureFlags;
            this.event = userData.event;

            this.setStatusToSearch();
        }

        setStatusToSearch() {
            this.status = EUserStatus.SEARCH;
            const inLobbyStatus = new InLobbyStatus({
                email: this.email,
                status: EUserStatus.SEARCH,
                datetime: new Date().getTime(),
                inviteCode: this.inviteCode,
                isPerformer: this.chosenPitch != null
            })
            inLobbyStatus.save();
        }

        setStatusToRoom() {
            this.status = EUserStatus.ROOM;
            const inLobbyStatus = new InLobbyStatus({
                email: this.email,
                status: EUserStatus.ROOM,
                datetime: new Date().getTime(),
                inviteCode: this.inviteCode,
                isPerformer: this.chosenPitch != null
            })
            inLobbyStatus.save();
        }

        registerLobbyExit() {
            const inLobbyStatus = new InLobbyStatus({
                email: this.email,
                status: EUserStatus.INACTIVE,
                datetime: new Date().getTime(),
                inviteCode: this.inviteCode,
                isPerformer: this.chosenPitch != null
            })
            inLobbyStatus.save();
        }

        roomConnect({ roomId, companion, isMyTurn }: { roomId: string; companion: IUser, isMyTurn: boolean }) {
            this.setStatusToRoom();
            this.roomId = roomId;
            this.opponentSocketId = companion.socketId;
            this.opponentIp = companion.ip;
            this.userTurn = undefined;

            io.to(this.socketId).emit(EVENTS.COMMON.ROOM_CONNECTION, {
                companion,
                roomId,
                isMyTurn
            });
        }

        toggleVideo(payload) {
            io.to(this.opponentSocketId).emit(EVENTS.COMMON.TOGGLE_INVISIBILITY, payload);
        }

        leaveRoom(reason) {
            if (this.opponentSocketId) {
                io.to(this.opponentSocketId).emit(EVENTS.COMMON.LEAVE_ROOM, reason);
            }

            if (this.roomId) {
                /**
                 * Change status after delay
                 * to avoid issue when user go to search and connect immediately
                 */
                setTimeout(() => {
                    this.setStatusToSearch();
                }, 2500);
            }

            // default props
            this.roomId = '';
            this.opponentSocketId = '';
        }

        emitBoth(event: string, payload?: any, payload2?: any) {
            io.to(this.socketId).emit(event, payload);
            io.to(this.opponentSocketId).emit(event, payload2 || payload);
        }

        sendEventToOpponent(event: string, payload?: any) {
            io.to(this.opponentSocketId).emit(event, payload);
        }
    }

    return User;
}

export default userFactory;
