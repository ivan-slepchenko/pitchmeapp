import { validationResult } from 'express-validator';

function addErrorResponse(controllerFn) {
    return (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                success: false,
                message: 'Fields validation error',
            });
        }

        return controllerFn(req, res, next);
    };
}

export default addErrorResponse;
