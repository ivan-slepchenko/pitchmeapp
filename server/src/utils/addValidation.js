function addValidation(ruleName) {
    return (validatedFn) => {
        validatedFn.validationName = ruleName;

        return validatedFn;
    };
}

export default addValidation;
