import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../../protected';

const invalidResponse = {
    success: false,
    message: 'No valid token provided.',
};

function checkAuth(req, res, next) {
    const { headers, body, query } = req;
    const token = headers['x-access-token'] || body.token || query.token;

    if (token) {
        // async fn

        jwt.verify(token, JWT_SECRET, (error, decoded) => {
            if (error) {
                console.error('[checkAuth]', error);

                return res.status(401).send(invalidResponse);
            }

            const { userId } = decoded;
            // set user id into req
            req.userId = userId;
            return next();
        });
    } else {
        return res.status(403).send(invalidResponse);
    }
}

export default checkAuth;
