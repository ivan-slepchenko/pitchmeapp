import { Strategy as FacebookStrategy } from 'passport-facebook';

import { SERVER_HOST } from 'utils/constants';

/**
 * https://developers.facebook.com/apps/
 * To test on localhost you need to add: https://localhost:8080/api/auth/facebook/callback
 * to fb dev panel
 * localhost cb is deleted by security reason
 */
const facebookStrategy = new FacebookStrategy(
    {
        clientID: '2088126071318135',
        clientSecret: 'a105db17ead0f71d9c11362f7a003e38',
        callbackURL: `${SERVER_HOST}/api/auth/facebook/callback`,
        profileFields: ['displayName', 'email'],
    },
    (_accessToken, _refreshToken, profile, done) => {
        done(null, profile);
    }
);

export default facebookStrategy;
