import { Strategy as GoogleStrategy } from 'passport-google-oauth20';

import { SERVER_HOST } from 'utils/constants';

// ivan.slepchenko@zettech.com
// https://console.developers.google.com - pitchme
// TODO: move to value to string
const googleStrategy = new GoogleStrategy(
    {
        clientID: '594165987483-7hk6jqutmsh2d8cepj1qiihbm20nl143.apps.googleusercontent.com',
        clientSecret: 'NI7h6Y08GRdG-pPTwwZGXy7B',
        callbackURL: `${SERVER_HOST}/api/auth/google/callback`,
    },
    (_accessToken, _refreshToken, profile, done) => done(null, profile)
);

export default googleStrategy;
