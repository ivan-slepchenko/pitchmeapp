import { Strategy as LinkedinStrategy } from 'passport-linkedin-oauth2';

import { SERVER_HOST } from 'utils/constants';

const linkedinStrategy = new LinkedinStrategy(
    {
        clientID: '77z31ja28h7auf',
        clientSecret: 'YN4x1blqoSDT0JA5',
        callbackURL: `${SERVER_HOST}/api/auth/linkedin/callback`,
        scope: ['r_liteprofile']
    },
    (token, tokenSecret, profile, done) => {
        done(null, profile);
    }
);

export default linkedinStrategy;
