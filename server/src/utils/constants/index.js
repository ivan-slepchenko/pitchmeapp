require('dotenv').config();

export const IS_DEV = process.env.NODE_ENV === 'development';
export const EXPRESS_PORT = 8080;

export const CLIENT_HOST = IS_DEV ? 'https://localhost:3000' : 'https://app.pitchme.app';
export const SERVER_HOST = IS_DEV ? `https://localhost:${EXPRESS_PORT}` : `https://app.pitchme.app:${EXPRESS_PORT}`;

