import { getRandomInt } from './helpers/common';

/**
 *
 *  it finds pair user index for the first user
 *  returns the Index
 *  or Null
 *  */
export default function getValidPairIndex(users) {
    const usersCount = users.length;

    if (usersCount > 1) {
        return getRandomInt([1, usersCount]);

        // logic with Opponent gender and ages
        // const user1 = users[0];
        // const {
        //     gender: user1Gender,
        //     age: user1Age,
        //     opponentGender: user1OpponentGender,
        //     opponentAgeFrom: user1OpponentAgeFrom,
        //     opponentAgeTo: user1OpponentAgeTo
        // } = user1;

        // // search pair for user1:
        // for (let userIndex = 1; userIndex < usersCount; userIndex += 1) {
        //     const user2 = users[userIndex];
        //     const {
        //         gender: user2Gender,
        //         age: user2Age,
        //         opponentAgeFrom: user2OpponentAgeFrom,
        //         opponentAgeTo: user2OpponentAgeTo,
        //         opponentGender: user2OpponentGender
        //     } = user2;

        //     const validGender =
        //         (user1OpponentGender === 'both' ||
        //             user1OpponentGender === user2Gender) &&
        //         (user2OpponentGender === 'both' ||
        //             user2OpponentGender === user1Gender);

        //     const ageInRange =
        //         user2Age >= user1OpponentAgeFrom &&
        //         user2Age <= user1OpponentAgeTo &&
        //         (user1Age >= user2OpponentAgeFrom &&
        //             user1Age <= user2OpponentAgeTo);

        //     const validPair = validGender && ageInRange;

        //     if (validPair) {
        //         return userIndex;
        //     }
        // }
    }

    return null;
}
