import jwt from 'jsonwebtoken';
import { JWT_SECRET } from '../../protected';

function decodeJWT(req) {
    const token = req.body.token || req.query.token || req.headers['x-access-token'];
    const payload = jwt.decode(token, JWT_SECRET, { complete: true });

    return payload;
}

function createJWT(payload = {}) {
    const token = jwt.sign(payload, JWT_SECRET, {
        expiresIn: '28d',
    });

    return token;
}

export { decodeJWT, createJWT };
