import mongoose from 'mongoose';

function getRandomInt([min, max]: [number, number]): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Get random item from Array/String
 */
function getRandomItem(pool: any[] | String): any {
    return pool[Math.floor(Math.random() * pool.length)];
}

export function dateFromObj( strDate ) {
    return mongoose.Types.ObjectId(
        Math.floor((new Date( strDate )).getTime()/1000).toString(16) + "0000000000000000"
    )
}

const wait = (ms: number): Promise<void> => new Promise((res) => setTimeout(res, ms));

export { getRandomInt, wait, getRandomItem };
