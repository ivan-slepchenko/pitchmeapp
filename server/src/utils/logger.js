import pino from 'pino';
import chalk from 'chalk';
import { IS_DEV } from 'utils/constants';

const logger = pino({
    name: 'server-app',
    level: IS_DEV ? 'debug' : 'info',
});

const inform = (string) => console.info(chalk.cyan(string));

export { inform };

export default logger;
