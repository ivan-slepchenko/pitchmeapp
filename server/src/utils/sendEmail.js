import nodemailer from 'nodemailer';
import { google } from 'googleapis';

import logger, { inform } from 'utils/logger';

import { MAIN_EMAIL, EMAIL_REFRESH_TOKEN, EMAIL_CLIENT_ID, EMAIL_CLIENT_SECRET } from '../protected';

const OAuth2 = google.auth.OAuth2;
const refreshToken = EMAIL_REFRESH_TOKEN;
const oauth2Client = new OAuth2(
    EMAIL_CLIENT_ID,
    EMAIL_CLIENT_SECRET,
    'https://developers.google.com/oauthplayground' // Redirect URL
);
let transporter = null;

oauth2Client.setCredentials({
    refresh_token: refreshToken,
});

oauth2Client.getAccessToken().then((accessToken) => {
    transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            type: 'OAuth2',
            user: MAIN_EMAIL,
            clientId: EMAIL_CLIENT_ID,
            clientSecret: EMAIL_CLIENT_SECRET,
            refreshToken,
            accessToken,
        },
    });
}, (reason) => { console.error(`EMAIL TRANSPORT IS NOT SET, due to: ${reason}`); });

async function sendEmail(to, subject, html) {
    const mailOptions = {
        from: `pitchme.app <${MAIN_EMAIL}>`,
        to,
        subject,
        html,
    };

    await transporter.sendMail(mailOptions, (err, inf) => {
        if (err) {
            logger.error('[sendEmail]', err);
        } else {
            // eslint-disable-next-line
            inform('Email was sent successfully', inf);
        }
    });

    transporter.close();
}

export default sendEmail;
