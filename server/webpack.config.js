const path = require('path');
const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = function runConfig(env, argv) {
    const isDev = argv.mode === 'development';

    const commonConfig = {
        target: 'node',
        externals: [nodeExternals()],

        entry: ['babel-polyfill', './src/index.js'],

        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'build'),
        },

        resolve: {
            extensions: ['.ts', '.js'],
            alias: {
                utils: path.resolve(__dirname, 'src/utils/'),
                modules: path.resolve(__dirname, 'src/modules/'),
                user: path.resolve(__dirname, 'src/modules/user/'),
                socket: path.resolve(__dirname, 'src/socket/'),
            },
        },
        module: {
            rules: [
                {
                    test: /\.ts$/,
                    loader: 'ts-loader',
                },
            ],
        },

        plugins: [
            new CleanWebpackPlugin(['build'], {
                root: path.resolve(__dirname, ''),
                verbose: true,
            }),
            new webpack.HotModuleReplacementPlugin(),
        ],
    };

    if (isDev) {
        commonConfig.devtool = 'source-map';
    }

    return commonConfig;
};
